# Cinema-EPAM
<div style="text-align: center;"><strong>Project Description</strong></div>
The task of the final project is to develop a web application that supports the functionality according to the task variant.


<div style="text-align: center;"><strong>Variants</strong></div>

**Cinema.** The system is an Internet showcase of a cinema with one hall. The system has a schedule of films for the week from 9:00 to 22:00 (start of the last film) hours.An unregistered user can see the schedule, free seats in the hall and has the opportunity to register.The user should be able to sort the schedule entries by movie title, number of available seats, session date / time (default), and filter the schedule by the movies available for viewing.The registered user must be able to purchase a ticket for the selected session.The administrator can schedule a new movie, cancel the movie, view the attendance of the hall.

## Database
- **Name:** MySQL
- **Versions:** 15.1 Distrib 10.4.24-MariaDB
- **Schema:**
![](src/sql/schema.png)