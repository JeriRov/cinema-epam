package epam.project.finalproject.services.impl;

import epam.project.finalproject.dao.SessionDAO;
import epam.project.finalproject.dao.factories.DAOFactory;
import epam.project.finalproject.dao.factories.DAOFactoryDeliver;
import epam.project.finalproject.entities.Session;
import epam.project.finalproject.exceptions.DAOException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.SessionService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.time.LocalDate;
import java.util.*;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class SessionServiceImplTest {
    @Spy
    private static SessionService sessionService = new SessionServiceImpl();
    @Mock
    private static SessionDAO sessionDao;
    @Mock
    private static MockedStatic<DAOFactoryDeliver> factoryDeliver;
    private List<Session> sessionList;
    @Mock
    private Map<String, String> filterSortMap;
    @Mock
    private Session session;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        DAOFactoryDeliver daoFactoryDeliver = mock(DAOFactoryDeliver.class);
        DAOFactory daoFactory = mock(DAOFactory.class);
        sessionList = new ArrayList<>();

        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactoryDeliver);
        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactory);
        when(daoFactoryDeliver.getFactory()).thenReturn(daoFactory);
        when(daoFactory.getSessionDao()).thenReturn(sessionDao);
    }

    @AfterEach
    void clean() {
        factoryDeliver.close();
    }

    @Test
    void getById() throws DAOException, ServiceException {
        Session session = mock(Session.class);
        when(sessionDao.findById(1)).thenReturn(session);
        final Session byId = sessionService.getById(1);
        Assertions.assertEquals(session, byId);
    }

    @Test
    void getByIdServiceException() throws DAOException {
        final int id = 1;
        when(sessionDao.findById(id)).thenThrow(DAOException.class);

        Assertions.assertThrows(ServiceException.class, () -> sessionService.getById(id));
    }

    @Test
    void deleteSession() throws ServiceException, DAOException {
        Session session = mock(Session.class);
        sessionService.delete(session.getId());
        verify(sessionDao).delete(session.getId());
    }

    @Test
    void deleteSessionServiceException() throws DAOException {
        Session session = mock(Session.class);
        when(sessionDao.delete(anyInt())).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> sessionService.delete(session.getId()));
    }

    @Test
    void countTotalPages() throws DAOException, ServiceException {
        final int countTotalRow = 4;
        final int size = 2;
        when(sessionDao.countTotalRow()).thenReturn(countTotalRow);
        final int countTotalPages = sessionService.countTotalPages(size);
        Assertions.assertEquals(2, countTotalPages);
    }

    @Test
    void countTotalPagesServiceException() throws DAOException {
        final int size = 2;
        when(sessionDao.countTotalRow()).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> sessionService.countTotalPages(size));
    }

    @Test
    void addSession() throws DAOException, ServiceException {
        Session session = mock(Session.class);
        sessionService.create(session);
        verify(sessionDao).insert(session);
    }

    @Test
    void addSessionServiceException() throws DAOException {
        Session session = mock(Session.class);
        when(sessionDao.insert(any(Session.class))).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> sessionService.create(session));
    }

    @Test
    void getAll() throws ServiceException, DAOException {
        sessionList.add(session);
        when(sessionDao.findAll(anyInt(), anyInt())).thenReturn(sessionList);
        final List<Session> all = sessionService.getAll(anyInt(), anyInt());
        Assertions.assertNotNull(all);
        Assertions.assertFalse(all.isEmpty());
    }

    @Test
    void getAllPageMoreThanOne() throws ServiceException, DAOException {
        final int page = 3;
        final int size = 2;
        sessionList.add(session);
        sessionList.add(session);
        when(sessionDao.findAll(page, size)).thenReturn(sessionList);
        final List<Session> all = sessionService.getAll(page, size);
        Assertions.assertNotNull(all);
        Assertions.assertEquals(2, sessionList.size());
    }

    @Test
    void getAllServiceException() throws DAOException {
        when(sessionDao.findAll(anyInt(), anyInt())).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> sessionService.getAll(anyInt(), anyInt()));
    }

    @Test
    void getFilteredAndSortedSessionList() throws DAOException, ServiceException {
        final int start = 1;
        final int size = 1;
        sessionList.add(session);
        when(sessionDao.findFilteredAndSortedSessionList(filterSortMap, start, size)).thenReturn(sessionList);
        final List<Session> list = sessionService.getFilteredAndSorted(filterSortMap, start, size);
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    void getFilteredAndSortedSessionListPageBiggerOne() throws DAOException, ServiceException {
        final int page = 2;
        final int size = 1;
        sessionList.add(session);
        when(sessionDao.findFilteredAndSortedSessionList(filterSortMap, page, size)).thenReturn(sessionList);
        final List<Session> list = sessionService.getFilteredAndSorted(filterSortMap, page, size);
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    void getFilteredAndSortedSessionListServiceException() throws DAOException {
        final int page = 1;
        final int size = 1;
        when(sessionDao.findFilteredAndSortedSessionList(filterSortMap, page, size)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> sessionService.getFilteredAndSorted(filterSortMap, page, size));
    }

    @Test
    void getFilterSortMapFromParams() {
        Map<String, String[]> paramMap = new HashMap<>();
        paramMap.put(SESSION_SORT_BY_PARAM_NAME, new String[1]);
        paramMap.put(SESSION_SORT_METHOD_PARAM_NAME, new String[1]);
        paramMap.put(SESSION_FILTER_SHOW_PARAM_NAME, new String[1]);
        final Map<String, String> map = sessionService.getFilterSortMapFromParams(paramMap);
        Assertions.assertFalse(map.isEmpty());
    }

    @Test
    void sessionValid() throws DAOException, ServiceException {
        when(sessionDao.findAllByDatetimeAndDuration(session)).thenReturn(sessionList);
        final Collection<String> list = sessionService.sessionValid(session);
        Assertions.assertNotNull(list);
        Assertions.assertTrue(list.isEmpty());
    }

    @Test
    void sessionValidErrorList() throws DAOException, ServiceException {
        sessionList.add(session);
        when(sessionDao.findAllByDatetimeAndDuration(session)).thenReturn(sessionList);
        final Collection<String> list = sessionService.sessionValid(session);
        Assertions.assertNotNull(list);
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    void sessionValidServiceException() throws DAOException, ServiceException {
        when(sessionDao.findAllByDatetimeAndDuration(session)).thenThrow(DAOException.class);
        final Collection<String> list = sessionService.sessionValid(session);
        Assertions.assertNotNull(list);
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    void getSessionFieldsValidErrorList() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(FILM_ID_PARAM, "1");
        paramMap.put(SESSION_TIME_PARAM, "09:00");
        paramMap.put(SESSION_DATE_PARAM, String.valueOf(LocalDate.now()));
        paramMap.put(SESSION_PRICE_PARAM, "100");

        List<String> errorList = sessionService.getSessionFieldsValidErrorList(paramMap);

        Assertions.assertNotNull(errorList);
        Assertions.assertTrue(errorList.isEmpty());
    }

    @Test
    void getSessionFieldsValidErrorListFilmIdNull() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(FILM_ID_PARAM, null);
        paramMap.put(SESSION_TIME_PARAM, "09:00");
        paramMap.put(SESSION_DATE_PARAM, String.valueOf(LocalDate.now()));
        paramMap.put(SESSION_PRICE_PARAM, "100");

        List<String> errorList = sessionService.getSessionFieldsValidErrorList(paramMap);

        Assertions.assertNotNull(errorList);
        Assertions.assertTrue(errorList.contains(VALID_FILM_EMPTY));
    }

    @Test
    void getSessionFieldsValidErrorListServiceException()  {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(FILM_ID_PARAM, null);
        paramMap.put(SESSION_TIME_PARAM, "09:00");
        paramMap.put(SESSION_DATE_PARAM, String.valueOf(LocalDate.now()));
        paramMap.put(SESSION_PRICE_PARAM, "100");

        List<String> errorList = sessionService.getSessionFieldsValidErrorList(paramMap);

        Assertions.assertNotNull(errorList);
        Assertions.assertTrue(errorList.contains(VALID_FILM_EMPTY));
    }

}