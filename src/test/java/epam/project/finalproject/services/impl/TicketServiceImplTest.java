package epam.project.finalproject.services.impl;

import epam.project.finalproject.dao.TicketDAO;
import epam.project.finalproject.dao.factories.DAOFactory;
import epam.project.finalproject.dao.factories.DAOFactoryDeliver;
import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.entities.Session;
import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.DAOException;
import epam.project.finalproject.exceptions.EmptyListException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.NotificationService;
import epam.project.finalproject.services.SeatService;
import epam.project.finalproject.services.TicketService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.*;

class TicketServiceImplTest {
    private TicketService ticketService;
    @Mock
    private static TicketDAO ticketDao;
    @Mock
    private static MockedStatic<DAOFactoryDeliver> factoryDeliver;
    @Mock
    private SeatService seatService;
    private List<Ticket> ticketList;
    @Mock
    private Ticket ticket;
    @Mock
    private NotificationService notificationService;


    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        ticketList = new ArrayList<>();
        ticketService = Mockito.spy(new TicketServiceImpl(seatService, notificationService));

        DAOFactoryDeliver daoFactoryDeliver = mock(DAOFactoryDeliver.class);
        DAOFactory daoFactory = mock(DAOFactory.class);

        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactoryDeliver);
        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactory);
        when(daoFactoryDeliver.getFactory()).thenReturn(daoFactory);
        when(daoFactory.getTicketDao()).thenReturn(ticketDao);
    }

    @AfterEach
    void clean() {
        factoryDeliver.close();
    }

    @Test
    void sendTicketNotification() throws ServiceException {
        User user1 = new User();
        user1.setNotification(true);
        Session session = mock(Session.class);
        when(ticket.getSession()).thenReturn(session);
        when(session.getDate()).thenReturn(LocalDate.now());
        ticketList.add(ticket);

        ticketService.sendTicketNotification(ticketList, user1, Locale.getDefault());
        verify(notificationService, times(1)).createNotification(any(JobDetail.class), any(Trigger.class));
    }

    @Test
    void sendTicketNotificationUserNotificationFalse() throws ServiceException {
        User user1 = new User();
        user1.setNotification(false);
        Ticket ticket = mock(Ticket.class);
        Session session = mock(Session.class);
        when(ticket.getSession()).thenReturn(session);
        when(session.getDate()).thenReturn(LocalDate.now());
        ticketList.add(ticket);

        ticketService.sendTicketNotification(ticketList, user1, Locale.getDefault());
        verify(notificationService, never()).createNotification(any(JobDetail.class), any(Trigger.class));
    }

    @Test
    void saveTicketList() throws ServiceException {
        final Ticket ticket = mock(Ticket.class);
        List<Ticket> ticketList = List.of(ticket, ticket);
        Seat seat = mock(Seat.class);
        Session session = mock(Session.class);

        when(ticket.getSeat()).thenReturn(seat);
        when(ticket.getSession()).thenReturn(session);
        when(seatService.isSeatFreeBySessionId(anyInt(), anyInt())).thenReturn(true);
        ticketService.saveAll(ticketList);
        verify(ticketService, times(ticketList.size())).save(any(Ticket.class));
    }

    @Test
    void saveTicketListThrowEmptyListEmptyListException() {
        Assertions.assertThrows(EmptyListException.class, () -> ticketService.saveAll(null));
    }

    @Test
    void saveTicketListThrowEmptyListServiceList() throws DAOException {
        when(ticketDao.insert(any(Ticket.class))).thenThrow(DAOException.class);
        ticketList.add(ticket);
        Assertions.assertThrows(ServiceException.class, () -> ticketService.saveAll(ticketList));
    }


    @Test
    void saveTicket() throws DAOException, ServiceException {
        Ticket ticket = mock(Ticket.class);
        Seat seat = mock(Seat.class);
        Session session = mock(Session.class);

        when(ticket.getSeat()).thenReturn(seat);
        when(ticket.getSession()).thenReturn(session);
        when(seatService.isSeatFreeBySessionId(seat.getId(), session.getId())).thenReturn(true);

        ticketService.save(ticket);
        verify(ticketDao).insert(ticket);
    }

    @Test
    void saveTicketThrowServiceException() throws ServiceException {
        Ticket ticket = mock(Ticket.class);
        Seat seat = mock(Seat.class);
        Session session = mock(Session.class);

        when(ticket.getSeat()).thenReturn(seat);
        when(ticket.getSession()).thenReturn(session);
        when(seatService.isSeatFreeBySessionId(seat.getId(), session.getId())).thenReturn(false);
        Assertions.assertThrows(ServiceException.class, () -> ticketService.save(ticket));

        verifyNoInteractions(ticketDao);
    }

    @Test
    void formTicketList() {
        Session session = mock(Session.class);
        Seat seat = mock(Seat.class);
        List<Seat> seatList = List.of(seat, seat);
        User user = mock(User.class);
        final List<Ticket> ticketList = ticketService.formTicketList(session, seatList, user);
        Assertions.assertFalse(ticketList.isEmpty());
    }

    @Test
    void formTicketListThrowEmptyListException() {
        Session session = mock(Session.class);
        List<Seat> seatList = List.of();
        User user = mock(User.class);
        Assertions.assertThrows(EmptyListException.class, () -> ticketService.formTicketList(session, seatList, user));
    }


    @Test
    void countTotalCostOfTicketList() {
        Ticket ticket = mock(Ticket.class);
        List<Ticket> ticketList = List.of(ticket, ticket);
        when(ticket.getTicketPrice()).thenReturn(BigDecimal.TEN);
        final BigDecimal actual = ticketService.countTotalCostOfTicketList(ticketList);
        Assertions.assertEquals(BigDecimal.valueOf(20), actual);
    }

    @Test
    void countTotalCostOfTicketListThrowEmptyListException() {
        List<Ticket> ticketList = List.of();
        Assertions.assertThrows(EmptyListException.class, () -> ticketService.countTotalCostOfTicketList(ticketList));

    }

    @Test
    void getAllByUserId() throws DAOException, ServiceException {
        ticketList.add(ticket);
        when(ticketDao.findAllByUserId(anyInt(), anyInt(), anyInt())).thenReturn(ticketList);
        final List<Ticket> all = ticketService.getAllByUserId(0, 0, 0);
        Assertions.assertFalse(all.isEmpty());
    }

    @Test
    void getAllByUserIdServiceException() throws DAOException {
        when(ticketDao.findAllByUserId(anyInt(), anyInt(), anyInt())).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> ticketService.getAllByUserId(anyInt(), anyInt(), anyInt()));
    }

    @Test
    void getAllByUserIdPageBiggerOne() throws DAOException, ServiceException {
        ticketList.add(ticket);
        when(ticketDao.findAllByUserId(1, 2, 1)).thenReturn(ticketList);
        final List<Ticket> all = ticketService.getAllByUserId(1, 2, 1);
        Assertions.assertFalse(all.isEmpty());
    }

    @Test
    void countTotalPagesByUserId() throws DAOException, ServiceException {
        when(ticketDao.countTotalRowByUserId(1)).thenReturn(4);
        final int countTotalPages = ticketService.countTotalPagesByUserId(1, 2);
        Assertions.assertEquals(2, countTotalPages);
    }

    @Test
    void countTotalPagesByUserIdServiceException() throws DAOException {
        when(ticketDao.countTotalRowByUserId(1)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> ticketService.countTotalPagesByUserId(1, 2));
    }

    @Test
    void getById() throws DAOException, ServiceException {
        Ticket ticket = mock(Ticket.class);
        when(ticketDao.findById(1)).thenReturn(ticket);
        final Ticket ticketServiceById = ticketService.getById(1);
        Assertions.assertEquals(ticket, ticketServiceById);
    }

    @Test
    void getByIdServiceException() throws DAOException {
        when(ticketDao.findById(1)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> ticketService.getById(1));
    }
}