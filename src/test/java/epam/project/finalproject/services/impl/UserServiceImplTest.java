package epam.project.finalproject.services.impl;

import epam.project.finalproject.dao.UserDAO;
import epam.project.finalproject.dao.factories.DAOFactory;
import epam.project.finalproject.dao.factories.DAOFactoryDeliver;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.AuthException;
import epam.project.finalproject.exceptions.DAOException;
import epam.project.finalproject.exceptions.RegisterException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.UserService;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

class UserServiceImplTest {
    @Spy
    private static UserService userService = new UserServiceImpl();
    @Mock
    private static UserDAO userDao;
    @Mock
    private static MockedStatic<DAOFactoryDeliver> factoryDeliver;
    private Map<String, String> saltAndPass;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        DAOFactoryDeliver daoFactoryDeliver = mock(DAOFactoryDeliver.class);
        DAOFactory daoFactory = mock(DAOFactory.class);
        saltAndPass = new HashMap<>();

        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactoryDeliver);
        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactory);
        when(daoFactoryDeliver.getFactory()).thenReturn(daoFactory);
        when(daoFactory.getUserDao()).thenReturn(userDao);
    }

    @AfterEach
    void clean() {
        factoryDeliver.close();
    }

    @Test
    void authenticateUser() throws AuthException, DAOException {
        String login = "example@gmail.com";
        String salt = "salt";
        String password = "pass";
        when(userDao.getSaltAndPassByLogin(login)).thenReturn(saltAndPass);
        saltAndPass.put("salt", salt);
        saltAndPass.put("password", "askkI/9VxpN23TFWHWX9QjhwQRckmwLv94BgIaVK4xE=");
        Assertions.assertDoesNotThrow(() -> userService.authenticateUser(login, password));
    }

    @Test
    void authenticateUserServiceException() throws AuthException, DAOException {
        String login = "example@gmail.com";
        String password = "pass";
        when(userDao.getSaltAndPassByLogin(login)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> userService.authenticateUser(login, password));
    }

    @Test
    void authenticateUserFindSuchUserException() throws AuthException, DAOException {
        String login = "example@gmail.com";
        String password = "pass";
        when(userDao.getSaltAndPassByLogin(login)).thenThrow(AuthException.class);
        Assertions.assertThrows(ServiceException.class, () -> userService.authenticateUser(login, password));
    }


    @Test
    void getUserByLogin() throws ServiceException, AuthException, DAOException {
        User user = mock(User.class);
        when(userDao.getUserByLogin(anyString())).thenReturn(user);
        final User userByLogin = userService.getUserByLogin(anyString());
        Assertions.assertEquals(user, userByLogin);
    }

    @Test
    void getUserByLoginServiceException() throws AuthException, DAOException {
        when(userDao.getUserByLogin(anyString())).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> userService.getUserByLogin(anyString()));
    }

    @Test
    void getById() throws DAOException, ServiceException {
        User user = mock(User.class);
        when(userDao.findById(1)).thenReturn(user);
        final User us = userService.getById(1);
        assertEquals(user, us);
    }

    @Test
    void getByIdServiceException() throws DAOException {
        when(userDao.findById(1)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> userService.getById(1));
    }

    @Test
    void getMaxId() throws DAOException, ServiceException {
        when(userDao.getMaxId()).thenReturn(2);
        final int maxId = userService.getMaxId();
        assertEquals(2, maxId);
    }

    @Test
    void getMaxIdServiceException() throws DAOException {
        when(userDao.getMaxId()).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> userService.getMaxId());
    }

    @Test
    void create() throws ServiceException, DAOException {
        User user = mock(User.class);
        userService.create(user);
        verify(userDao).insert(user);
    }

    @Test
    void createServiceException() throws DAOException {
        User user = mock(User.class);
        doThrow(DAOException.class).when(userDao).insert(any(User.class));
        Assertions.assertThrows(ServiceException.class, () -> userService.create(user));
    }

    @Test
    void createRegisterException() throws DAOException {
        User user = mock(User.class);
        doThrow(RegisterException.class).when(userDao).insert(any(User.class));
        Assertions.assertThrows(ServiceException.class, () -> userService.create(user));
    }

    @Test
    void save() throws ServiceException, DAOException {
        User user = mock(User.class);
        userService.save(user);
        verify(userDao).update(user);
    }

    @Test
    void saveServiceException() throws DAOException {
        User user = mock(User.class);
        doThrow(DAOException.class).when(userDao).update(any(User.class));
        Assertions.assertThrows(ServiceException.class, () -> userService.save(user));
    }

    @Test
    void findByVerificationCode() throws DAOException, AuthException, ServiceException {
        User user = new User();
        user.setVerificationCode("verify");
        when(userDao.findByVerificationCode(anyString())).thenReturn(user);

        Assertions.assertEquals(user, userService.findByVerificationCode("verify"));
    }

    @Test
    void findByVerificationCodeServiceException() throws DAOException, AuthException {
        User user = new User();
        user.setVerificationCode("verify");
        when(userDao.findByVerificationCode(anyString())).thenThrow(DAOException.class);

        Assertions.assertThrows(ServiceException.class, () -> userService.findByVerificationCode(""));
    }

    @Test
    void findByVerificationCodeNull() {
        Assertions.assertThrows(ServiceException.class, () -> userService.findByVerificationCode(null));
    }


    @Test
    void validateUserEmail() {
        Assertions.assertEquals("", userService.validateUserEmail("email@gmail.com"));
    }

    @Test
    void validateUserEmailInvalid() {
        Assertions.assertEquals(VALID_EMAIL_INVALID, userService.validateUserEmail("email"));
    }

    @Test
    void getUserValidErrorList() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(F_NAME_PARAM, "Test");
        paramMap.put(L_NAME_PARAM, "Test");
        paramMap.put(EMAIL_PARAM, "test@gmail.com");
        paramMap.put(PASS_PARAM, "password");
        paramMap.put(PASS_CONFIRM_PARAM, "password");
        paramMap.put(PHONE_PARAM, null);
        paramMap.put(NOTIFICATION_PARAM, null);
        paramMap.put(VERIFICATION_CODE_PARAM, null);
        paramMap.put(ENABLED_PARAM, null);

        List<String> errorList = userService.getUserValidErrorList(paramMap);

        Assertions.assertEquals(Collections.emptyList(), errorList);

    }

    @Test
    void getUserValidErrorListInvalid() {
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put(F_NAME_PARAM, "Test");
        paramMap.put(L_NAME_PARAM, "Test");
        paramMap.put(EMAIL_PARAM, "test");
        paramMap.put(PASS_PARAM, "passwod");
        paramMap.put(PASS_CONFIRM_PARAM, "password");
        paramMap.put(PHONE_PARAM, null);
        paramMap.put(NOTIFICATION_PARAM, null);
        paramMap.put(VERIFICATION_CODE_PARAM, null);
        paramMap.put(ENABLED_PARAM, null);

        List<String> errorList = userService.getUserValidErrorList(paramMap);

        Assertions.assertEquals(List.of(VALID_EMAIL_INVALID, VALID_PASS_CONFIRM_NOT_EQUAL), errorList);
    }
}