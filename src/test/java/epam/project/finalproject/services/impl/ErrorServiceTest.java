package epam.project.finalproject.services.impl;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static org.mockito.Mockito.*;

class ErrorServiceTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void handleExceptionWithMessageResponseIsNotCommitted() throws ServletException, IOException {
        final String message = "message";
        final String className = ErrorServiceTest.class.getName();
        final Exception exception = new Exception(message);
        when(response.isCommitted()).thenReturn(false);

        ErrorService.handleException(request, response, message, className, exception);

        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }

    @Test
    void handleExceptionWithMessageResponseIsCommitted() {
        final String message = "message";
        final String className = ErrorServiceTest.class.getName();
        final Exception exception = new Exception(message);
        when(response.isCommitted()).thenReturn(true);

        ErrorService.handleException(request, response, message, className, exception);

        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void handleExceptionWithoutMessage() throws ServletException, IOException {
        final String message = "message";
        final String className = ErrorServiceTest.class.getName();
        final Exception exception = new Exception(message);
        when(response.isCommitted()).thenReturn(false);

        ErrorService.handleException(request, response, className, exception);

        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}