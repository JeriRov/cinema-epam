package epam.project.finalproject.services.impl;

import epam.project.finalproject.exceptions.VerifyException;
import epam.project.finalproject.services.CaptchaService;
import epam.project.finalproject.utilities.PropertiesManager;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.json.spi.JsonProvider;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.Properties;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static org.mockito.Mockito.*;

class CaptchaServiceImplTest {

    @Spy
    private CaptchaService captchaService = new CaptchaServiceImpl();
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void captchaValidation() throws IOException {
        when(request.getParameter("g-recaptcha-response")).thenReturn("response");

        MockedStatic<PropertiesManager> propertiesManagerMockedStatic = mockStatic(PropertiesManager.class);
        Properties properties = mock(Properties.class);
        propertiesManagerMockedStatic.when(() -> PropertiesManager.getProperties(anyString())).thenReturn(properties);

        HttpsURLConnection connection = mock(HttpsURLConnection.class);

        MockedConstruction<URL> urlMockedConstruction = mockConstruction(URL.class,
                (mock, context) -> when(mock.openConnection()).thenReturn(connection));


        OutputStream outputStream = mock(OutputStream.class);
        InputStream inputStream = mock(InputStream.class);

        when(connection.getOutputStream()).thenReturn(outputStream);
        when(connection.getInputStream()).thenReturn(inputStream);

        MockedConstruction<BufferedReader> bufferedReaderMockedConstruction = mockConstruction(BufferedReader.class,
                (mock, context) -> when(mock.readLine()).thenReturn(RandomString.make()).thenReturn(null));

        MockedStatic<JsonProvider> jsonMockedStatic = mockStatic(JsonProvider.class);


        JsonReader jsonReader = mock(JsonReader.class);


        JsonObject jsonObject = mock(JsonObject.class);
        when(jsonReader.readObject()).thenReturn(jsonObject);
        when(jsonObject.getBoolean(anyString())).thenReturn(false);
        JsonProvider jsonProvider = mock(JsonProvider.class);

        when(jsonProvider.createReader(any(StringReader.class))).thenReturn(jsonReader);
        jsonMockedStatic.when(JsonProvider::provider).thenReturn(jsonProvider);


        when(jsonObject.getBoolean(anyString())).thenReturn(true);

        captchaService.captchaValidation(request, response);

        propertiesManagerMockedStatic.close();
        urlMockedConstruction.close();
        jsonMockedStatic.close();
        bufferedReaderMockedConstruction.close();

        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void captchaValidationFail() {
        when(request.getParameter("g-recaptcha-response")).thenReturn("response");

        MockedStatic<PropertiesManager> propertiesManagerMockedStatic = mockStatic(PropertiesManager.class);
        Properties properties = mock(Properties.class);
        when(properties.getProperty(anyString())).thenReturn("");

        propertiesManagerMockedStatic.when(() -> PropertiesManager.getProperties(anyString())).thenReturn(properties);

        captchaService.captchaValidation(request, response);

        propertiesManagerMockedStatic.close();

        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void captchaValidationThrowVerifyException() {
        when(request.getParameter("g-recaptcha-response")).thenReturn(null);

        Assertions.assertThrows(VerifyException.class, () -> captchaService.captchaValidation(request, response));
    }

    @Test
    void captchaValidationIOException() {
        when(request.getParameter("g-recaptcha-response")).thenReturn("response");

        MockedStatic<PropertiesManager> propertiesManagerMockedStatic = mockStatic(PropertiesManager.class);
        Properties properties = mock(Properties.class);
        when(properties.get(anyString())).thenReturn(null);

        propertiesManagerMockedStatic.when(() -> PropertiesManager.getProperties(anyString())).thenReturn(properties);

        captchaService.captchaValidation(request, response);

        propertiesManagerMockedStatic.close();
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
    }
}