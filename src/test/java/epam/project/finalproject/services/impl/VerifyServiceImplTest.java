package epam.project.finalproject.services.impl;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.mail.messages.VerifyMailMessage;
import epam.project.finalproject.services.MailService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.VerifyService;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Locale;

import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND_VIEW_VERIFICATION;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class VerifyServiceImplTest {
    @Spy
    private VerifyService verifyService;
    @Mock
    private UserService userService;
    @Mock
    private HttpServletRequest request;
    @Mock
    private MailService mailService;
    private User user;
    private final String code = "someCode";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        verifyService = new VerifyServiceImpl(userService, mailService);
        user = new User();
    }

    @Test
    void constructorTest() {
        Assertions.assertDoesNotThrow(() -> {
            new VerifyServiceImpl();
        });
        Assertions.assertDoesNotThrow(() -> {
            new VerifyServiceImpl(userService, mailService);
        });
    }

    @Test
    void testVerify() throws ServiceException {
        when(userService.findByVerificationCode(anyString())).thenReturn(user);
        Assertions.assertTrue(verifyService.verify("code"));
    }

    @Test
    void verifyFalse() throws ServiceException {
        when(userService.findByVerificationCode(anyString())).thenReturn(null);
        Assertions.assertFalse(verifyService.verify("code"));

        when(userService.findByVerificationCode(anyString())).thenReturn(user);
        user.setEnabled(true);
        Assertions.assertFalse(verifyService.verify("code"));
    }

    @Test
    void verifyNull() {
        Assertions.assertThrows(NullPointerException.class, () -> verifyService.verify(null));
    }

    @Test
    void saveVerificationCodeToUser() throws ServiceException {
        when(userService.save(user)).thenReturn(user);
        verifyService.saveVerificationCodeToUser(user, code);
        Assertions.assertEquals(code, user.getVerificationCode());
    }

    @Test
    void sendMail() throws ServiceException {
        final String verifyURL = verifyService.getVerifyURL(request, code);
        Locale locale = Locale.getDefault();
        verifyService.sendMail(user, verifyURL, locale);
        VerifyMailMessage message = new VerifyMailMessage(user, verifyURL, locale);
        verify(mailService, times(1)).sendEmail(user.getEmail(), message.subject().toString(), message.body().toString());
    }

    @Test
    void getVerifyUrl() {
        when(request.getRequestURL()).thenReturn(new StringBuffer("url"));
        String urlCode = "url?command=" + COMMAND_VIEW_VERIFICATION + "&code=" + code;
        assertEquals(urlCode, verifyService.getVerifyURL(request, code));
    }
}