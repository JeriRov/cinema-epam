package epam.project.finalproject.services.impl;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.mail.MailSessionFactory;
import epam.project.finalproject.services.MailService;
import jakarta.mail.Message;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Properties;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class MailServiceImplTest {
    @Spy
    private static MailService mailService = new MailServiceImpl();
    @Mock
    private static MockedStatic<MailSessionFactory> mailSessionFactoryMockedStatic;
    @Mock
    private static MockedStatic<Transport> transportMockedStatic;
    @Mock
    private Session mailSession;
    @Mock
    private Properties properties;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void clean() {
        mailSessionFactoryMockedStatic.close();
        transportMockedStatic.close();
    }

    @Test
    void sendEmail() throws ServiceException {
        mailSessionFactoryMockedStatic.when(() -> MailSessionFactory.createSession(any(Properties.class))).thenReturn(mailSession);
        when(mailSession.getProperties()).thenReturn(properties);
        when(properties.get(anyString())).thenReturn("some");

        mailService.sendEmail(RandomString.make(), "", "");

        mailSessionFactoryMockedStatic.verify(() -> MailSessionFactory.createSession(any(Properties.class)), times(1));
        transportMockedStatic.verify(() -> Transport.send(any(Message.class)), times(1));
    }

    @Test
    void sendEmailServiceException() {
        Assertions.assertThrows(ServiceException.class, () -> mailService.sendEmail(RandomString.make(), "", ""));

        mailSessionFactoryMockedStatic.when(() -> MailSessionFactory.createSession(any(Properties.class))).thenReturn(mailSession);
        when(mailSession.getProperties()).thenReturn(properties);
        when(properties.get(anyString())).thenReturn("some");

        Assertions.assertThrows(ServiceException.class, () -> mailService.sendEmail("", "", ""));
    }
}