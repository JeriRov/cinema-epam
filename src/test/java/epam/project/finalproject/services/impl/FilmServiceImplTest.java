package epam.project.finalproject.services.impl;

import epam.project.finalproject.dao.FilmDAO;
import epam.project.finalproject.dao.factories.DAOFactory;
import epam.project.finalproject.dao.factories.DAOFactoryDeliver;
import epam.project.finalproject.entities.Film;
import epam.project.finalproject.exceptions.DAOException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.FilmService;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class FilmServiceImplTest {
    @Spy
    private static FilmService filmService = new FilmServiceImpl();
    @Mock
    private static FilmDAO filmDao;
    @Mock
    private static MockedStatic<DAOFactoryDeliver> factoryDeliver;
    @Mock
    List<Film> filmList;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        DAOFactoryDeliver daoFactoryDeliver = mock(DAOFactoryDeliver.class);
        DAOFactory daoFactory = mock(DAOFactory.class);

        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactoryDeliver);
        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactory);
        when(daoFactoryDeliver.getFactory()).thenReturn(daoFactory);
        when(daoFactory.getFilmDAO()).thenReturn(filmDao);
    }

    @AfterEach
    void clean() {
        factoryDeliver.close();
    }

    @Test
    void getAll() throws ServiceException, DAOException {
        when(filmDao.findAll()).thenReturn(filmList);
        final List<Film> all = filmService.getAll();
        Assertions.assertFalse(all.isEmpty());
    }

    @Test
    void getAllServiceException() throws DAOException {
        when(filmDao.findAll()).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> filmService.getAll());
    }


    @Test
    void getAllPaginated() throws DAOException, ServiceException {

        when(filmDao.findAll(anyInt(), anyInt())).thenReturn(filmList);
        final List<Film> all = filmService.getAll(anyInt(), anyInt());

        Assertions.assertNotNull(all);
        Assertions.assertFalse(all.isEmpty());
    }

    @Test
    void getAllPaginatedServiceException() throws DAOException {
        when(filmDao.findAll(anyInt(), anyInt())).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> filmService.getAll(anyInt(), anyInt()));
    }

    @Test
    void getAllPaginatedPageBiggerOne() throws DAOException, ServiceException {
        final int start = 2;
        final int size = 1;
        when(filmDao.findAll(start, size)).thenReturn(filmList);
        final List<Film> all = filmService.getAll(start, size);
        Assertions.assertFalse(all.isEmpty());
    }

    @Test
    void countTotalPages() throws DAOException, ServiceException {
        final int countTotalRow = 4;
        final int size = 2;
        final int expected = 2;
        when(filmDao.countTotalRow()).thenReturn(countTotalRow);
        final int countTotalPages = filmService.countTotalPages(size);
        Assertions.assertEquals(expected, countTotalPages);
    }

    @Test
    void countTotalPagesServiceException() throws DAOException {
        final int size = 2;
        when(filmDao.countTotalRow()).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> filmService.countTotalPages(size));
    }

    @Test
    void createFilm() throws DAOException, ServiceException {
        Film film = mock(Film.class);
        filmService.create(film);
        verify(filmDao).insert(film);
    }

    @Test
    void createFilmServiceException() throws DAOException {
        Film film = mock(Film.class);
        when(filmDao.insert(film)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> filmService.create(film));
    }

    @Test
    void deleteFilmById() throws DAOException, ServiceException {
        Film film = mock(Film.class);
        filmService.delete(film.getId());
        verify(filmDao).delete(film.getId());
    }

    @Test
    void deleteFilmByIdServiceException() throws DAOException {
        final int filmId = 1;
        when(filmDao.delete(filmId)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> filmService.delete(filmId));
    }

    @Test
    void getFilmById() throws ServiceException, DAOException {
        Film film = mock(Film.class);
        when(filmDao.findById(1)).thenReturn(film);
        final Film filmById = filmService.getById(1);
        Assertions.assertEquals(film, filmById);
    }

    @Test
    void getFilmByIdServiceException() throws DAOException {
        final int id = 1;
        when(filmDao.findById(id)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> filmService.getById(id));
    }

    @Test
    void getFilmValidErrorList() {
        Map<String, String> filmParamMap = new HashMap<>();
        filmParamMap.put(FILM_NAME_PARAM, RandomString.make(10));
        filmParamMap.put(FILM_DESCR_PARAM, RandomString.make(10));
        filmParamMap.put(POSTER_URL_PARAM, RandomString.make(10));
        filmParamMap.put(FILM_DURATION_PARAM, "100");
        String[] genreIds = {"1", "2"};

        Assertions.assertEquals(Collections.emptyList(), filmService.getFilmValidErrorList(filmParamMap, genreIds));
    }

    @Test
    void getFilmValidErrorListGenresIdsEmpty() {
        Map<String, String> filmParamMap = new HashMap<>();
        filmParamMap.put(FILM_NAME_PARAM, RandomString.make(10));
        filmParamMap.put(FILM_DESCR_PARAM, RandomString.make(10));
        filmParamMap.put(POSTER_URL_PARAM, RandomString.make(10));
        filmParamMap.put(FILM_DURATION_PARAM, "100");
        String[] genreIds = new String[0];

        Assertions.assertEquals(List.of(VALID_GENRE_LIST_EMPTY), filmService.getFilmValidErrorList(filmParamMap, genreIds));
    }
}