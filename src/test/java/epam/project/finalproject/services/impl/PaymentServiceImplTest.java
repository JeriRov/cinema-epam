package epam.project.finalproject.services.impl;

import com.mchange.util.AssertException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.PaymentService;
import epam.project.finalproject.utilities.PropertiesManager;
import jakarta.json.spi.JsonProvider;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PaymentServiceImplTest {
    private PaymentService paymentService;

    @BeforeEach
    void setUp() {
        paymentService = new PaymentServiceImpl();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void makePayment() throws ServiceException, IOException {
        Map<String, String> card = new HashMap<>();
        MockedStatic<PropertiesManager> propertiesManagerMockedStatic = mockStatic(PropertiesManager.class);
        Properties properties = mock(Properties.class);
        propertiesManagerMockedStatic.when(() -> PropertiesManager.getProperties(anyString())).thenReturn(properties);

        HttpsURLConnection connection = mock(HttpsURLConnection.class);

        MockedConstruction<URL> urlMockedConstruction = mockConstruction(URL.class,
                (mock, context) -> when(mock.openConnection()).thenReturn(connection));

        OutputStream outputStream = mock(OutputStream.class);
        InputStream inputStream = mock(InputStream.class);

        when(connection.getOutputStream()).thenReturn(outputStream);
        when(connection.getInputStream()).thenReturn(inputStream);
        when(connection.getResponseCode()).thenReturn(HttpsURLConnection.HTTP_OK);

        paymentService.makePayment(card, BigDecimal.TEN);

        verify(connection, times(1)).setRequestMethod("POST");
        verify(connection, times(1)).disconnect();
        propertiesManagerMockedStatic.close();
        urlMockedConstruction.close();
    }

    @Test
    void makePaymentThrowServiceException() throws IOException {
        Map<String, String> card = new HashMap<>();
        MockedStatic<PropertiesManager> propertiesManagerMockedStatic = mockStatic(PropertiesManager.class);
        Properties properties = mock(Properties.class);
        propertiesManagerMockedStatic.when(() -> PropertiesManager.getProperties(anyString())).thenReturn(properties);

        HttpsURLConnection connection = mock(HttpsURLConnection.class);

        MockedConstruction<URL> urlMockedConstruction = mockConstruction(URL.class,
                (mock, context) -> when(mock.openConnection()).thenReturn(connection));

        OutputStream outputStream = mock(OutputStream.class);
        InputStream inputStream = mock(InputStream.class);

        when(connection.getOutputStream()).thenReturn(outputStream);
        when(connection.getInputStream()).thenReturn(inputStream);

        Assertions.assertThrows(ServiceException.class, () -> paymentService.makePayment(card, BigDecimal.TEN));

        propertiesManagerMockedStatic.close();
        urlMockedConstruction.close();
    }

    @Test
    void validateCard() {
        Map<String, String> card = new HashMap<>();
        card.put(CREDIT_CARD_NUMBER, "1111 1111 1111 1111");
        card.put(CREDIT_CARD_DATE, "11/11");
        card.put(CREDIT_CARD_CVV, "111");

        Assertions.assertDoesNotThrow(() -> paymentService.validateCard(card));
    }

    @Test
    void validateCardInvalid() {
        Map<String, String> card = new HashMap<>();
        Assertions.assertThrows(ServiceException.class, () -> paymentService.validateCard(card));
    }
}