package epam.project.finalproject.services.impl;

import epam.project.finalproject.services.GeneralService;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;

import static epam.project.finalproject.utilities.constants.OtherConstants.MAX_SESSION_TIME;
import static epam.project.finalproject.utilities.constants.OtherConstants.MIN_SESSION_TIME;
import static org.mockito.Mockito.*;

class GeneralServiceImplTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private ServletContext servletContext;
    @Mock
    private HttpSession session;

    private GeneralService generalService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        generalService = new GeneralServiceImpl();
    }

    @Test
    void initParams() {
        when(request.getServletContext()).thenReturn(servletContext);
        when(request.getSession(true)).thenReturn(session);

        generalService.initParams(request);

        verify(servletContext, times(1)).setAttribute("minSessionTime", MIN_SESSION_TIME);
        verify(servletContext, times(1)).setAttribute("maxSessionTime", MAX_SESSION_TIME);
        verify(session).setAttribute("nowDate", LocalDate.now());
    }
}