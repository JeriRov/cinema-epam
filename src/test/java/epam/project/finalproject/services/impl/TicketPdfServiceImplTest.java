package epam.project.finalproject.services.impl;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import epam.project.finalproject.entities.Film;
import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.entities.Session;
import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.exceptions.PdfException;
import epam.project.finalproject.exceptions.WritePdfToResponseException;
import epam.project.finalproject.services.TicketPdfService;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

class TicketPdfServiceImplTest {
    @Spy
    private TicketPdfService ticketPdfService = new TicketPdfServiceImpl();

    @Test
    void TicketPdfServiceImplPdfException() {
        MockedStatic<BaseFont> baseFont = mockStatic(BaseFont.class);
        baseFont.when(() -> BaseFont.createFont(anyString(), anyString(), anyBoolean())).thenThrow(DocumentException.class);
        Assertions.assertThrows(PdfException.class, () -> ticketPdfService = new TicketPdfServiceImpl());
        baseFont.close();
    }

    @Test
    void formPDFTicket() {
        Ticket ticket = mock(Ticket.class);
        Session session = mock(Session.class);
        Film film = mock(Film.class);
        Seat seat = mock(Seat.class);
        long duration = film.getDurationInMinutes();
        Locale locale = Locale.getDefault();
        BigDecimal price = BigDecimal.TEN;

        when(ticket.getSession()).thenReturn(session);
        when(session.getFilm()).thenReturn(film);
        when(ticket.getSeat()).thenReturn(seat);
        when(ticket.getTicketPrice()).thenReturn(price);
        when(session.getDate()).thenReturn(LocalDate.now());
        when(session.getTime()).thenReturn(LocalTime.now());
        when(ticket.getSession().getFilm().getDurationInMinutes()).thenReturn(duration);
        final ByteArrayOutputStream stream = ticketPdfService.formPDFTicket(ticket, locale);
        Assertions.assertNotNull(stream);
    }

    @Test
    void formPDFTicketPdfException() {
        Ticket ticket = mock(Ticket.class);
        Session session = mock(Session.class);
        Film film = mock(Film.class);
        Seat seat = mock(Seat.class);
        long duration = film.getDurationInMinutes();
        Locale locale = Locale.getDefault();
        BigDecimal price = BigDecimal.TEN;

        MockedStatic<PdfWriter> pdfWriterMockedStatic = mockStatic(PdfWriter.class);
        pdfWriterMockedStatic.when(() -> PdfWriter.getInstance(any(Document.class), any(OutputStream.class))).thenThrow(DocumentException.class);

        when(ticket.getSession()).thenReturn(session);
        when(session.getFilm()).thenReturn(film);
        when(ticket.getSeat()).thenReturn(seat);
        when(ticket.getTicketPrice()).thenReturn(price);
        when(session.getDate()).thenReturn(LocalDate.now());
        when(session.getTime()).thenReturn(LocalTime.now());
        when(ticket.getSession().getFilm().getDurationInMinutes()).thenReturn(duration);
        Assertions.assertThrows(PdfException.class, () -> ticketPdfService.formPDFTicket(ticket, locale));
    }

    @Test
    void formPDFTicketPdfExceptionNullPointerException() {
        Ticket ticket = mock(Ticket.class);
        Locale locale = Locale.getDefault();
        Assertions.assertThrows(NullPointerException.class, () -> ticketPdfService.formPDFTicket(ticket, locale));
    }


    @Test
    void writePdfToResponse() throws IOException {
        ByteArrayOutputStream stream = mock(ByteArrayOutputStream.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        ServletOutputStream servletOutputStream = mock(ServletOutputStream.class);
        when(response.getOutputStream()).thenReturn(servletOutputStream);
        doNothing().when(stream).writeTo(servletOutputStream);
        ticketPdfService.writePdfToResponse(stream, response);
        verify(stream).writeTo(servletOutputStream);
        verify(stream).flush();
        verify(stream).close();
    }

    @Test
    void writePdfToResponseIOException() throws IOException {
        ByteArrayOutputStream stream = mock(ByteArrayOutputStream.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        ServletOutputStream servletOutputStream = mock(ServletOutputStream.class);
        when(response.getOutputStream()).thenReturn(servletOutputStream);
        doThrow(IOException.class).when(stream).writeTo(servletOutputStream);

        Assertions.assertThrows(WritePdfToResponseException.class, () -> ticketPdfService.writePdfToResponse(stream, response));

        when(response.getOutputStream()).thenThrow(IOException.class);

        Assertions.assertThrows(WritePdfToResponseException.class, () -> ticketPdfService.writePdfToResponse(stream, response));

    }

    @Test
    void writePdfToResponsePdfException() throws IOException {
        ByteArrayOutputStream stream = mock(ByteArrayOutputStream.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        when(response.getOutputStream()).thenThrow(IllegalStateException.class);

        Assertions.assertThrows(PdfException.class, () -> ticketPdfService.writePdfToResponse(stream, response));
    }
}