package epam.project.finalproject.services.impl;

import epam.project.finalproject.dao.SeatDAO;
import epam.project.finalproject.dao.factories.DAOFactory;
import epam.project.finalproject.dao.factories.DAOFactoryDeliver;
import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.exceptions.DAOException;
import epam.project.finalproject.exceptions.EmptyArrayException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.SeatService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

class SeatServiceImplTest {
    @Spy
    private static SeatService seatService = new SeatServiceImpl();
    @Mock
    private static SeatDAO seatDao;
    @Mock
    private static MockedStatic<DAOFactoryDeliver> factoryDeliver;

    @Mock
    List<Seat> seatList;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        DAOFactoryDeliver daoFactoryDeliver = mock(DAOFactoryDeliver.class);
        DAOFactory daoFactory = mock(DAOFactory.class);

        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactoryDeliver);
        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactory);
        when(daoFactoryDeliver.getFactory()).thenReturn(daoFactory);
        when(daoFactory.getSeatDao()).thenReturn(seatDao);
    }


    @AfterEach
    void clean() {
        factoryDeliver.close();
    }

    @Test
    void getSeatListByIdArray() throws DAOException, ServiceException {
        String[] seatIds = {"2"};
        Seat seat = mock(Seat.class);
        when(seatDao.findById(anyInt())).thenReturn(seat);
        final List<Seat> list = seatService.getSeatListByIdArray(seatIds);
        assertFalse(list.isEmpty());
    }

    @Test
    void getSeatListByIdArrayServiceException() throws DAOException {
        String[] seatIds = {"2"};
        when(seatDao.findById(anyInt())).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> seatService.getSeatListByIdArray(seatIds));
    }

    @Test
    void getSeatListByIdArrayIsNull() throws DAOException, ServiceException {
        try {
            seatService.getSeatListByIdArray(null);
            fail("EmptyArrayException should be thrown");
        } catch (EmptyArrayException ignored) {
        }
        verify(seatDao, never()).findById(anyInt());
    }

    @Test
    void getFreeSeatsBySessionId() throws DAOException, ServiceException {
        int sessionId = 1;
        when(seatDao.findAllFreeSeatBySessionId(sessionId)).thenReturn(seatList);
        final List<Seat> list = seatService.getFreeSeatsBySessionId(sessionId);
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    void getFreeSeatsBySessionIdServiceException() throws DAOException {
        int sessionId = 1;
        when(seatDao.findAllFreeSeatBySessionId(sessionId)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> seatService.getFreeSeatsBySessionId(sessionId));
    }


    @Test
    void getAll() throws DAOException, ServiceException {
        when(seatDao.findAll()).thenReturn(seatList);
        final List<Seat> all = seatService.getAll();
        Assertions.assertFalse(all.isEmpty());
    }

    @Test
    void getAllServiceException() throws DAOException {
        when(seatDao.findAll()).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> seatService.getAll());
    }

    @Test
    void isSeatFreeBySessionIdIsTrue() throws DAOException, ServiceException {
        int sessionId = 1;
        int seatId = 1;
        when(seatDao.isSeatFree(seatId, sessionId)).thenReturn(true);
        final boolean seatFreeBySessionId = seatService.isSeatFreeBySessionId(seatId, sessionId);
        Assertions.assertTrue(seatFreeBySessionId);
    }

    @Test
    void isSeatFreeBySessionIdIsFalse() throws DAOException, ServiceException {
        int sessionId = 1;
        int seatId = 1;
        when(seatDao.isSeatFree(seatId, sessionId)).thenReturn(false);
        final boolean seatFreeBySessionId = seatService.isSeatFreeBySessionId(seatId, sessionId);
        Assertions.assertFalse(seatFreeBySessionId);
    }

    @Test
    void isSeatFreeBySessionServiceException() throws DAOException {
        int sessionId = 1;
        int seatId = 1;
        when(seatDao.isSeatFree(seatId, sessionId)).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> seatService.isSeatFreeBySessionId(seatId, sessionId));
    }
}
