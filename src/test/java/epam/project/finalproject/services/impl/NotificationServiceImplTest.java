package epam.project.finalproject.services.impl;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.NotificationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

import static org.mockito.Mockito.*;

class NotificationServiceImplTest {
    private NotificationService notificationService;
    @Mock
    private StdSchedulerFactory schedulerFactory;
    @Mock
    private Scheduler scheduler;
    @Mock
    private Trigger trigger;
    @Mock
    private JobDetail jobDetail;

    @BeforeEach
    void setup() throws SchedulerException {
        MockitoAnnotations.openMocks(this);
        when(schedulerFactory.getScheduler()).thenReturn(scheduler);

        notificationService = new NotificationServiceImpl(schedulerFactory);
    }

    @Test
    void createNotification() throws SchedulerException {
        Assertions.assertDoesNotThrow(() -> notificationService.createNotification(jobDetail, trigger));
        verify(scheduler, times(1)).start();
        verify(scheduler, times(1)).scheduleJob(jobDetail, trigger);
    }

    @Test
    void createNotificationKeyAlreadyExist() throws ServiceException, SchedulerException {
        notificationService.createNotification(jobDetail, trigger);

        when(scheduler.checkExists(jobDetail.getKey())).thenReturn(true);
        notificationService.createNotification(jobDetail, trigger);
        verify(scheduler, times(1)).start();
        verify(scheduler, times(1)).scheduleJob(jobDetail, trigger);
    }

    @Test
    void createNotificationKeyServiceException() throws SchedulerException {
        doThrow(SchedulerException.class).when(scheduler).start();
        Assertions.assertThrows(ServiceException.class, () -> notificationService.createNotification(jobDetail, trigger));
    }
}
