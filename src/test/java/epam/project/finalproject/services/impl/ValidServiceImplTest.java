package epam.project.finalproject.services.impl;

import epam.project.finalproject.services.ValidService;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.*;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mockStatic;

class ValidServiceImplTest {
    @Spy
    private ValidService validService = new ValidServiceImpl();
    private List<String> errorList;
    private String field;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        errorList = new ArrayList<>();
        field = "";
    }

    @Test
    void validLettersField() {
        field = "field";
        validService.validLettersField(errorList, field, MIN_F_NAME_LENGTH, MAX_F_NAME_LENGTH, VALID_F_NAME_EMPTY,
                VALID_F_NAME_INVALID, VALID_F_NAME_LENGTH);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validLettersFieldNullOrBlank() {
        field = "";
        validService.validLettersField(errorList, field, MIN_F_NAME_LENGTH, MAX_F_NAME_LENGTH, VALID_F_NAME_EMPTY,
                VALID_F_NAME_INVALID, VALID_F_NAME_LENGTH);
        Assertions.assertEquals(List.of(VALID_F_NAME_EMPTY), errorList);

        errorList = new ArrayList<>();

        validService.validLettersField(errorList, null, MIN_F_NAME_LENGTH, MAX_F_NAME_LENGTH, VALID_F_NAME_EMPTY,
                VALID_F_NAME_INVALID, VALID_F_NAME_LENGTH);
        Assertions.assertEquals(List.of(VALID_F_NAME_EMPTY), errorList);
    }

    @Test
    void validLettersFieldPattern() {
        field = "Test1";
        validService.validLettersField(errorList, field, MIN_F_NAME_LENGTH, MAX_F_NAME_LENGTH, VALID_F_NAME_EMPTY,
                VALID_F_NAME_INVALID, VALID_F_NAME_LENGTH);
        Assertions.assertEquals(List.of(VALID_F_NAME_INVALID), errorList);
    }

    @Test
    void validLettersFieldLength() {
        String generatedString = randomDigitsString();
        validService.validLettersField(errorList, generatedString, MIN_F_NAME_LENGTH, MAX_F_NAME_LENGTH, VALID_F_NAME_EMPTY,
                VALID_F_NAME_INVALID, VALID_F_NAME_LENGTH);
        Assertions.assertEquals(List.of(VALID_F_NAME_LENGTH), errorList);
    }


    @Test
    void validDateField() {
        LocalDate date = LocalDate.now();
        validService.validDateField(errorList, date.toString(), LocalDate.now(), LocalDate.now().plusYears(1), VALID_DATE_EMPTY,
                VALID_DATE_INVALID, VALID_DATE_RANGE);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validDateFieldNullOrBlank() {
        validService.validDateField(errorList, null, LocalDate.now(), LocalDate.now().plusYears(1), VALID_DATE_EMPTY,
                VALID_DATE_INVALID, VALID_DATE_RANGE);
        Assertions.assertEquals(List.of(VALID_DATE_EMPTY), errorList);

        errorList = new ArrayList<>();

        validService.validDateField(errorList, "", LocalDate.now(), LocalDate.now().plusYears(1), VALID_DATE_EMPTY,
                VALID_DATE_INVALID, VALID_DATE_RANGE);
        Assertions.assertEquals(List.of(VALID_DATE_EMPTY), errorList);
    }

    @Test
    void validDateFieldPattern() {
        field = LocalDate.now() + "a";
        validService.validDateField(errorList, field, LocalDate.now(), LocalDate.now().plusYears(1), VALID_DATE_EMPTY,
                VALID_DATE_INVALID, VALID_DATE_RANGE);

        Assertions.assertEquals(List.of(VALID_DATE_INVALID), errorList);
    }

    @Test
    void validDateFieldRange() {
        field = String.valueOf(LocalDate.now().plusYears(2));
        validService.validDateField(errorList, field, LocalDate.now(), LocalDate.now().plusYears(1), VALID_DATE_EMPTY,
                VALID_DATE_INVALID, VALID_DATE_RANGE);

        Assertions.assertEquals(List.of(VALID_DATE_RANGE), errorList);
    }

    @Test
    void validDateFieldDateTimeParseException() {
        LocalDate startRange = LocalDate.now();
        LocalDate endRange = LocalDate.now().plusYears(1);


        field = String.valueOf(LocalDate.now());
        MockedStatic<LocalDate> localDate = mockStatic(LocalDate.class);
        localDate.when(() -> LocalDate.parse(anyString())).thenThrow(DateTimeParseException.class);
        validService.validDateField(errorList, field, startRange, endRange, VALID_DATE_EMPTY,
                VALID_DATE_INVALID, VALID_DATE_RANGE);

        Assertions.assertEquals(List.of(VALID_DATE_INVALID), errorList);
        localDate.close();
    }


    @Test
    void validTimeField() {
        String time = String.valueOf(MIN_SESSION_TIME);
        validService.validTimeField(errorList, time, MIN_SESSION_TIME, MAX_SESSION_TIME, VALID_TIME_EMPTY, VALID_TIME_INVALID,
                VALID_TIME_RANGE);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validTimeFieldNullOrBlank() {
        validService.validTimeField(errorList, null, MIN_SESSION_TIME, MAX_SESSION_TIME, VALID_TIME_EMPTY, VALID_TIME_INVALID,
                VALID_TIME_RANGE);
        Assertions.assertEquals(List.of(VALID_TIME_EMPTY), errorList);

        errorList = new ArrayList<>();

        validService.validTimeField(errorList, "", MIN_SESSION_TIME, MAX_SESSION_TIME, VALID_TIME_EMPTY, VALID_TIME_INVALID,
                VALID_TIME_RANGE);
        Assertions.assertEquals(List.of(VALID_TIME_EMPTY), errorList);
    }

    @Test
    void validTimeFieldPattern() {
        field = MIN_SESSION_TIME + "a";
        validService.validTimeField(errorList, field, MIN_SESSION_TIME, MAX_SESSION_TIME, VALID_TIME_EMPTY, VALID_TIME_INVALID,
                VALID_TIME_RANGE);

        Assertions.assertEquals(List.of(VALID_TIME_INVALID), errorList);
    }

    @Test
    void validTimeFieldRange() {
        field = String.valueOf(MAX_SESSION_TIME.plusHours(1));
        validService.validTimeField(errorList, field, MIN_SESSION_TIME, MAX_SESSION_TIME, VALID_TIME_EMPTY, VALID_TIME_INVALID,
                VALID_TIME_RANGE);

        Assertions.assertEquals(List.of(VALID_TIME_RANGE), errorList);

        errorList = new ArrayList<>();

        field = String.valueOf(MIN_SESSION_TIME.minusHours(1));
        validService.validTimeField(errorList, field, MIN_SESSION_TIME, MAX_SESSION_TIME, VALID_TIME_EMPTY, VALID_TIME_INVALID,
                VALID_TIME_RANGE);

        Assertions.assertEquals(List.of(VALID_TIME_RANGE), errorList);
    }

    @Test
    void validTimeFieldDateTimeParseException() {

        field = String.valueOf(MIN_SESSION_TIME);
        MockedStatic<LocalTime> localTime = mockStatic(LocalTime.class);

        localTime.when(() -> LocalTime.parse(anyString())).thenThrow(DateTimeParseException.class);
        validService.validTimeField(errorList, field, MIN_SESSION_TIME, MAX_SESSION_TIME, VALID_TIME_EMPTY, VALID_TIME_INVALID,
                VALID_TIME_RANGE);

        Assertions.assertEquals(List.of(VALID_TIME_INVALID), errorList);
        localTime.close();
    }


    @Test
    void validDigitsField() {
        field = "15";
        validService.validDigitsField(errorList, field, MIN_TICKET_COST, MAX_TICKET_COST,
                VALID_PRICE_EMPTY, VALID_PRICE_INVALID, VALID_PRICE_RANGE);

        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validDigitsFieldNullOrBlank() {
        validService.validDigitsField(errorList, null, MIN_TICKET_COST, MAX_TICKET_COST,
                VALID_PRICE_EMPTY, VALID_PRICE_INVALID, VALID_PRICE_RANGE);
        Assertions.assertEquals(List.of(VALID_PRICE_EMPTY), errorList);

        errorList = new ArrayList<>();

        validService.validDigitsField(errorList, "", MIN_TICKET_COST, MAX_TICKET_COST,
                VALID_PRICE_EMPTY, VALID_PRICE_INVALID, VALID_PRICE_RANGE);
        Assertions.assertEquals(List.of(VALID_PRICE_EMPTY), errorList);
    }

    @Test
    void validDigitsFieldPattern() {
        field = "aa";
        validService.validDigitsField(errorList, field, MIN_TICKET_COST, MAX_TICKET_COST,
                VALID_PRICE_EMPTY, VALID_PRICE_INVALID, VALID_PRICE_RANGE);

        Assertions.assertEquals(List.of(VALID_PRICE_INVALID), errorList);
    }

    @Test
    void validDigitsFieldRange() {
        field = "10000000";

        validService.validDigitsField(errorList, field, MIN_TICKET_COST, MAX_TICKET_COST,
                VALID_PRICE_EMPTY, VALID_PRICE_INVALID, VALID_PRICE_RANGE);

        Assertions.assertEquals(List.of(VALID_PRICE_RANGE), errorList);

        errorList = new ArrayList<>();

        field = "0";
        validService.validDigitsField(errorList, field, MIN_TICKET_COST, MAX_TICKET_COST,
                VALID_PRICE_EMPTY, VALID_PRICE_INVALID, VALID_PRICE_RANGE);

        Assertions.assertEquals(List.of(VALID_PRICE_RANGE), errorList);
    }


    @Test
    void validStringField() {
        field = "Name";
        validService.validStringField(errorList, field, 1, MAX_FILM_NAME_LENGTH, VALID_FILM_NAME_EMPTY,
                VALID_FILM_DESC_LENGTH);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validStringFieldNullOrBlank() {
        validService.validStringField(errorList, null, 1, MAX_FILM_NAME_LENGTH, VALID_FILM_NAME_EMPTY,
                VALID_FILM_DESC_LENGTH);
        Assertions.assertEquals(List.of(VALID_FILM_NAME_EMPTY), errorList);

        errorList = new ArrayList<>();

        validService.validStringField(errorList, "", 1, MAX_FILM_NAME_LENGTH, VALID_FILM_NAME_EMPTY,
                VALID_FILM_DESC_LENGTH);
        Assertions.assertEquals(List.of(VALID_FILM_NAME_EMPTY), errorList);
    }

    @Test
    void validStringFieldLength() {
        field = RandomString.make(101);
        validService.validStringField(errorList, field, 1, MAX_FILM_NAME_LENGTH, VALID_FILM_NAME_EMPTY,
                VALID_FILM_DESC_LENGTH);
        Assertions.assertEquals(List.of(VALID_FILM_DESC_LENGTH), errorList);

        errorList = new ArrayList<>();
        field = "1";
        validService.validStringField(errorList, field, 2, MAX_FILM_NAME_LENGTH, VALID_FILM_NAME_EMPTY,
                VALID_FILM_DESC_LENGTH);
        Assertions.assertEquals(List.of(VALID_FILM_DESC_LENGTH), errorList);
    }


    @Test
    void validNullableStringField() {
        field = RandomString.make();
        validService.validNullableStringField(errorList, field, MAX_FILM_DESC_LENGTH, VALID_FILM_DESC_LENGTH);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validNullableStringFieldLength() {
        field = RandomString.make(1000);
        validService.validNullableStringField(errorList, field, MAX_FILM_DESC_LENGTH, VALID_FILM_DESC_LENGTH);
        Assertions.assertEquals(List.of(VALID_FILM_DESC_LENGTH), errorList);
    }


    @Test
    void validEmailField() {
        field = "example@gmail.com";
        validService.validEmailField(errorList, field);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validEmailFieldNullOrBlank() {
        validService.validEmailField(errorList, null);
        Assertions.assertEquals(List.of(VALID_EMAIL_EMPTY), errorList);

        errorList = new ArrayList<>();

        validService.validEmailField(errorList, "");
        Assertions.assertEquals(List.of(VALID_EMAIL_EMPTY), errorList);
    }

    @Test
    void validEmailFieldPattern() {
        field = "example";
        validService.validEmailField(errorList, field);
        Assertions.assertEquals(List.of(VALID_EMAIL_INVALID), errorList);
    }

    @Test
    void validEmailFieldLength() {
        field = RandomString.make(400) + "@gmail.com";
        validService.validEmailField(errorList, field);
        Assertions.assertEquals(List.of(VALID_EMAIL_LENGTH), errorList);
    }


    @Test
    void validPassField() {
        field = "Password12@";
        validService.validPasswordField(errorList, field);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validPassFieldNullOrEmpty() {
        validService.validPasswordField(errorList, null);
        Assertions.assertEquals(List.of(VALID_PASS_EMPTY), errorList);

        errorList = new ArrayList<>();

        validService.validPasswordField(errorList, "");
        Assertions.assertEquals(List.of(VALID_PASS_EMPTY), errorList);
    }

    @Test
    void validPassFieldLength() {
        field = "Pa";
        validService.validPasswordField(errorList, field);
        Assertions.assertEquals(List.of(VALID_PASS_LENGTH), errorList);

        errorList = new ArrayList<>();

        field = RandomString.make(200);
        validService.validPasswordField(errorList, field);
        Assertions.assertEquals(List.of(VALID_PASS_LENGTH), errorList);
    }


    @Test
    void validPassConfirmField() {
        field = RandomString.make();
        String password = field;
        validService.validPasswordConfirmField(errorList, password, field);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validPassConfirmFieldNullOrBlank() {
        validService.validPasswordConfirmField(errorList, "w", null);
        Assertions.assertEquals(List.of(VALID_PASS_CONFIRM_EMPTY), errorList);

        errorList = new ArrayList<>();

        validService.validPasswordConfirmField(errorList, "", "");
        Assertions.assertEquals(List.of(VALID_PASS_CONFIRM_EMPTY), errorList);
    }

    @Test
    void validPassConfirmFieldEquals() {
        field = RandomString.make();
        String password = field + "a";
        validService.validPasswordConfirmField(errorList, password, field);
        Assertions.assertEquals(List.of(VALID_PASS_CONFIRM_NOT_EQUAL), errorList);
    }


    @Test
    void validPhoneNumberField() {
        field = "380123456789";
        validService.validPhoneNumberField(errorList, field);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validPhoneNumberFieldPattern() {
        field = "3801345678a";
        validService.validPhoneNumberField(errorList, field);
        Assertions.assertEquals(List.of(VALID_PHONE_INVALID), errorList);
    }

    @Test
    void validCreditCardField() {
        Map<String, String> cardFields = new HashMap<>();
        cardFields.put(CREDIT_CARD_NUMBER, "1111 1111 1111 1111");
        cardFields.put(CREDIT_CARD_DATE, "11/11");
        cardFields.put(CREDIT_CARD_CVV, "111");
        validService.validCreditCardField(errorList, cardFields);
        Assertions.assertEquals(Collections.emptyList(), errorList);
    }

    @Test
    void validCreditCardFieldEmpty() {
        Map<String, String> cardFields = new HashMap<>();
        cardFields.put(CREDIT_CARD_NUMBER, "");
        cardFields.put(CREDIT_CARD_DATE, "");
        cardFields.put(CREDIT_CARD_CVV, "");
        validService.validCreditCardField(errorList, cardFields);
        Assertions.assertEquals(List.of(VALID_CREDIT_CARD_NUMBER_IS_EMPTY,
                VALID_CREDIT_CARD_DATE_IS_EMPTY, VALID_CREDIT_CARD_CVV_IS_EMPTY), errorList);
    }

    @Test
    void validCreditCardFieldPattern() {
        Map<String, String> cardFields = new HashMap<>();
        cardFields.put(CREDIT_CARD_NUMBER, "111");
        cardFields.put(CREDIT_CARD_DATE, "1");
        cardFields.put(CREDIT_CARD_CVV, "1");
        validService.validCreditCardField(errorList, cardFields);
        Assertions.assertEquals(List.of(VALID_CREDIT_CARD_NUMBER_IS_INVALID,
                VALID_CREDIT_CARD_DATE_IS_INVALID, VALID_CREDIT_CARD_CVV_IS_INVALID), errorList);
    }

    @Test
    void validCreditCardFieldNull() {
        validService.validCreditCardField(errorList, null);
        Assertions.assertEquals(List.of(VALID_CREDIT_CARD_FIELDS_IS_EMPTY), errorList);
    }

    private String randomDigitsString() {
        Random random = new Random();

        return random.ints(97, 122 + 1)
                .limit(46)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}