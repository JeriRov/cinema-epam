package epam.project.finalproject.services.impl;

import epam.project.finalproject.services.PaginationService;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Map;

import static org.mockito.Mockito.mock;

class PaginationServiceImplTest {
    private final PaginationService paginationService = Mockito.spy(PaginationServiceImpl.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);

    @Test
    void getPaginationParamsFromRequest() {
        final Map<String, Integer> map = paginationService.getPaginationParamsFromRequest(request);
        Assertions.assertFalse(map.isEmpty());
    }
}