package epam.project.finalproject.services.impl;

import epam.project.finalproject.dao.GenreDAO;
import epam.project.finalproject.dao.factories.DAOFactory;
import epam.project.finalproject.dao.factories.DAOFactoryDeliver;
import epam.project.finalproject.entities.Genre;
import epam.project.finalproject.exceptions.DAOException;
import epam.project.finalproject.exceptions.EmptyArrayException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.GenreService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;


class GenreServiceImplTest {
    @Spy
    private static GenreService genreService = new GenreServiceImpl();
    @Mock
    private static GenreDAO genreDao;
    @Mock
    private static MockedStatic<DAOFactoryDeliver> factoryDeliver;
    @Mock
    private List<Genre> genreList;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        DAOFactoryDeliver daoFactoryDeliver = mock(DAOFactoryDeliver.class);
        DAOFactory daoFactory = mock(DAOFactory.class);

        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactoryDeliver);
        factoryDeliver.when(() -> DAOFactoryDeliver.getInstance().getFactory()).thenReturn(daoFactory);
        when(daoFactoryDeliver.getFactory()).thenReturn(daoFactory);
        when(daoFactory.getGenreDAO()).thenReturn(genreDao);
    }

    @AfterEach
    void clean() {
        factoryDeliver.close();
    }

    @Test
    void getGenreListByIdArrayIsNull() {
        Assertions.assertThrows(EmptyArrayException.class, () -> genreService.getGenreListByIdArray(null));
    }

    @Test
    void getGenreListByIdArray() throws DAOException, ServiceException {
        String[] genreIds = {"2"};
        Genre genre = mock(Genre.class);
        when(genreDao.findById(anyInt())).thenReturn(genre);
        final List<Genre> list = genreService.getGenreListByIdArray(genreIds);
        Assertions.assertFalse(list.isEmpty());
    }

    @Test
    void getGenreListByIdArrayServiceException() throws DAOException {
        String[] genreIds = {"2"};
        when(genreDao.findById(anyInt())).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> genreService.getGenreListByIdArray(genreIds));
    }


    @Test
    void getAll() throws DAOException, ServiceException {
        when(genreDao.findAll()).thenReturn(genreList);
        final List<Genre> all = genreService.getAll();
        Assertions.assertNotNull(all);
        Assertions.assertFalse(all.isEmpty());
    }

    @Test
    void getAllServiceException() throws DAOException {
        when(genreDao.findAll()).thenThrow(DAOException.class);
        Assertions.assertThrows(ServiceException.class, () -> genreService.getAll());
    }
}