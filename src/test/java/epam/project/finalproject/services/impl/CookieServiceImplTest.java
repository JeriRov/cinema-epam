package epam.project.finalproject.services.impl;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.services.CookieService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.Locale;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class CookieServiceImplTest {
    @Spy
    private CookieService cookieService = new CookieServiceImpl();
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private User user;
    @Mock
    private Cookie cookieUserId;
    @Mock
    private Cookie cookieUserRole;
    @Mock
    private Cookie cookieUserLang;
    Cookie[] cookies;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(cookieUserId.getName()).thenReturn(USER_ID);
        when(cookieUserRole.getName()).thenReturn(USER_ROLE);
        when(cookieUserLang.getName()).thenReturn(LANG);

        when(cookieUserId.getValue()).thenReturn("1");
        when(cookieUserRole.getValue()).thenReturn(User.Role.getRandomRole().toString());
        when(cookieUserLang.getValue()).thenReturn(String.valueOf(Locale.getDefault()));

        cookies = new Cookie[]{cookieUserId, cookieUserRole, cookieUserLang};
    }

    @Test
    void loginCookieWithoutRememberMe() {
        final User.Role userRole = User.Role.getRandomRole();
        when(user.getId()).thenReturn(1);
        when(user.getUserRole()).thenReturn(userRole);

        cookieService.loginCookie(response, user, "");

        verify(response, times(2)).addCookie(any(Cookie.class));
    }

    @Test
    void loginCookieWithRememberMe() {
        final User.Role userRole = User.Role.getRandomRole();
        when(user.getId()).thenReturn(1);
        when(user.getUserRole()).thenReturn(userRole);

        cookieService.loginCookie(response, user, "rememberMe");

        verify(response, times(2)).addCookie(any(Cookie.class));
    }

    @Test
    void logoutCookie() {
        when(request.getCookies()).thenReturn(cookies);

        cookieService.logoutCookie(request, response);

        verify(cookieUserId, times(1)).setMaxAge(-1);
        verify(cookieUserRole, times(1)).setMaxAge(-1);
        verify(response, times(3)).addCookie(any(Cookie.class));
    }

    @Test
    void setLocaleCookie() {
        final String locale = String.valueOf(Locale.getDefault());
        cookieService.setLocaleCookie(response, locale);
        verify(response, times(1)).addCookie(any(Cookie.class));
    }

    @Test
    void initSessionWithCookie() {
        when(request.getSession(true)).thenReturn(session);
        when(request.getCookies()).thenReturn(cookies);

        cookieService.initSessionWithCookie(request);

        verify(session, times(cookies.length)).setAttribute(any(), any());
    }
}