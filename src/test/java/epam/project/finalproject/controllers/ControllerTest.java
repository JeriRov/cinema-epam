package epam.project.finalproject.controllers;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND;
import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND_VIEW_ERROR_PAGE;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static org.mockito.Mockito.*;

class ControllerTest {
    @Mock
    private Controller controller;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        controller = new Controller();
        controller.init();
    }

    @Test
    void doGet() throws ServletException, IOException {
        when(request.getParameter(COMMAND)).thenReturn(COMMAND_VIEW_ERROR_PAGE);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);

        controller.doGet(request, response);

        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }

    @Test
    void doPost() throws ServletException, IOException {
        when(request.getParameter(COMMAND)).thenReturn(COMMAND_VIEW_ERROR_PAGE);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);

        controller.doPost(request, response);

        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}