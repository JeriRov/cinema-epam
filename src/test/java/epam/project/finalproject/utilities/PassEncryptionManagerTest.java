package epam.project.finalproject.utilities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static epam.project.finalproject.utilities.constants.OtherConstants.SALT_LENGTH;
import static org.mockito.ArgumentMatchers.anyInt;

class PassEncryptionManagerTest {

    private final PassEncryptionManager manager = Mockito.spy(PassEncryptionManager.class);

    @Test
    void getSaltValue() {
        final String saltValue = manager.getSaltValue(anyInt());
        Assertions.assertNotNull(saltValue);
    }

    @Test
    void generateSecurePassword() {
        final String saltValue = manager.getSaltValue(SALT_LENGTH);
        final String password = manager.generateSecurePassword("213", saltValue);
        Assertions.assertNotNull(password);
    }

    @Test
    void verifyUserPassword() {
        final String saltValue = manager.getSaltValue(SALT_LENGTH);
        final String password = "123";
        final String encodePass = manager.generateSecurePassword(password, saltValue);
        final boolean b = manager.verifyUserPassword(password, encodePass, saltValue);
        Assertions.assertTrue(b);
    }

    @Test
    void notVerifyUserPassword() {
        final String saltValue = manager.getSaltValue(SALT_LENGTH);
        final String password = "123";
        final String encodePass = manager.generateSecurePassword(password, saltValue);
        final boolean b = manager.verifyUserPassword("321", encodePass, saltValue);
        Assertions.assertFalse(b);
    }
}