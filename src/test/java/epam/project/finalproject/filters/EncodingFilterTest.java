package epam.project.finalproject.filters;

import jakarta.servlet.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class EncodingFilterTest {

    private EncodingFilter encodingFilter;
    @Mock
    private FilterConfig filterConfig;
    @Mock
    private ServletRequest request;
    @Mock
    private ServletResponse response;
    @Mock
    private FilterChain filterChain;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        encodingFilter = new EncodingFilter();
    }

    @Test
    void init() {
        try {
            encodingFilter.init(filterConfig);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }

    @Test
    void doFilter() throws ServletException, IOException {
        encodingFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void destroy() {
        try {
            encodingFilter.destroy();
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }
}