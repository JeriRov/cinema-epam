package epam.project.finalproject.filters;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.utilities.RedirectManager;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.CommandConstants.*;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.USER_ROLE;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

class SecurityFilterTest {
    private SecurityFilter securityFilter;
    @Mock
    private FilterConfig filterConfig;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private FilterChain filterChain;
    @Mock
    private ServletContext servletContext;
    @Mock
    private RequestDispatcher dispatcher;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        securityFilter = new SecurityFilter();
    }

    @Test
    void init() {
        try {
            securityFilter.init(filterConfig);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }

    @Test
    void destroy() {
        try {
            securityFilter.destroy();
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }

    @Test
    void doFilterForUser() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(true)).thenReturn(session);
        when(request.getServletContext()).thenReturn(servletContext);
        when(session.getAttribute(USER_ROLE)).thenReturn(User.Role.USER);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);

        securityFilter.doFilter(request, response, filterChain);

        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(filterChain, never()).doFilter(request, response);
    }

    @Test
    void doFilterForAdmin() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(true)).thenReturn(session);
        when(request.getServletContext()).thenReturn(servletContext);
        when(session.getAttribute(USER_ROLE)).thenReturn(User.Role.ADMIN);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);

        securityFilter.doFilter(request, response, filterChain);

        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(filterChain, never()).doFilter(request, response);
    }

    @Test
    void doFilterForGuest() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(true)).thenReturn(session);
        when(request.getServletContext()).thenReturn(servletContext);
        when(session.getAttribute(USER_ROLE)).thenReturn(User.Role.GUEST);

        securityFilter.doFilter(request, response, filterChain);


        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(COMMAND_VIEW_LOGIN_PAGE));
        verify(filterChain, never()).doFilter(request, response);
    }

    @Test
    void doFilterSkipAllIf() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(true)).thenReturn(session);
        when(request.getServletContext()).thenReturn(servletContext);
        when(session.getAttribute(USER_ROLE)).thenReturn("Something else");

        securityFilter.doFilter(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void doFilterSkipAll() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(true)).thenReturn(session);
        when(request.getServletContext()).thenReturn(servletContext);
        when(session.getAttribute(USER_ROLE)).thenReturn(User.Role.ADMIN);
        when(request.getParameter(COMMAND)).thenReturn(COMMAND_VIEW_MAIN_PAGE);
        securityFilter.init(filterConfig);
        securityFilter.doFilter(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }
}