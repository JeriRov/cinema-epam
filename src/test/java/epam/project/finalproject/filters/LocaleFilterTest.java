package epam.project.finalproject.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.OtherConstants.LANG;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

class LocaleFilterTest {
    private LocaleFilter localeFilter;
    @Mock
    private FilterConfig filterConfig;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private FilterChain filterChain;
    @Mock
    private ServletContext servletContext;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        localeFilter = new LocaleFilter();
    }

    @Test
    void init() {
        try {
            localeFilter.init(filterConfig);
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }

    @Test
    void destroy() {
        try {
            localeFilter.destroy();
        } catch (Exception e) {
            fail("Should not have thrown any exception");
        }
    }

    @Test
    void doFilterWithoutLang() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(true)).thenReturn(session);
        when(request.getServletContext()).thenReturn(servletContext);

        localeFilter.doFilter(request, response, filterChain);
        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    void doFilterWithLang() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(true)).thenReturn(session);
        when(request.getServletContext()).thenReturn(servletContext);
        when(request.getParameter(LANG)).thenReturn("en");

        localeFilter.doFilter(request, response, filterChain);

        verify(response, times(1)).sendRedirect(request.getHeader("referer"));
        verify(filterChain, times(1)).doFilter(request, response);
    }
}