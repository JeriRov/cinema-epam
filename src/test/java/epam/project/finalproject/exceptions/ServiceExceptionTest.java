package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
class ServiceExceptionTest {
    @Test
    void emptyArrayExceptionTest() {
        final String message = "msg";
        ServiceException serviceException = new ServiceException(message);
        Assertions.assertEquals(message, serviceException.getMessage());

        final Throwable cause = mock(Throwable.class);
        serviceException = new ServiceException(message, cause);
        Assertions.assertEquals(message, serviceException.getMessage());
        Assertions.assertEquals(cause, serviceException.getCause());
    }
}