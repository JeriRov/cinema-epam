package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class TicketExceptionTest {
    @Test
    void ticketExceptionTest() {
        final String msg = "msg";
        TicketException ticketException = new TicketException(msg);
        Assertions.assertEquals(msg, ticketException.getMessage());

        final Throwable cause = mock(Throwable.class);
        ticketException = new TicketException(msg, cause);
        Assertions.assertEquals(msg, ticketException.getMessage());
        Assertions.assertEquals(cause, ticketException.getCause());
    }
}