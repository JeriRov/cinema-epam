package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class EmptyArrayExceptionTest {
    @Test
    void emptyArrayExceptionTest() {
        final String message = "msg";
        EmptyArrayException emptyArrayException = new EmptyArrayException(message);
        Assertions.assertEquals(message, emptyArrayException.getMessage());

        final Throwable cause = mock(Throwable.class);
        emptyArrayException = new EmptyArrayException(message, cause);
        Assertions.assertEquals(message, emptyArrayException.getMessage());
        Assertions.assertEquals(cause, emptyArrayException.getCause());
    }
}