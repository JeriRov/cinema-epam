package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ConnectionExceptionTest {
    @Test
    void connectionExceptionTest() {
        final String msg = "msg";
        ConnectionException connectionException = new ConnectionException(msg);
        Assertions.assertEquals(msg, connectionException.getMessage());

        connectionException = new ConnectionException();
        Assertions.assertEquals("ConnectionException was occurred", connectionException.getMessage());
    }
}