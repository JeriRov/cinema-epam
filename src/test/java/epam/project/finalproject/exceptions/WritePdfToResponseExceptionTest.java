package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class WritePdfToResponseExceptionTest {
    @Test
    void emptyArrayExceptionTest() {
        final String message = "msg";
        WritePdfToResponseException writePdfToResponseException = new WritePdfToResponseException(message);
        Assertions.assertEquals(message, writePdfToResponseException.getMessage());

        final Throwable cause = mock(Throwable.class);
        writePdfToResponseException = new WritePdfToResponseException(message, cause);
        Assertions.assertEquals(message, writePdfToResponseException.getMessage());
        Assertions.assertEquals(cause, writePdfToResponseException.getCause());
    }
}