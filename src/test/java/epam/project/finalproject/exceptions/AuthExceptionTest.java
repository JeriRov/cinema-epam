package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class AuthExceptionTest {
    @Test
    void authExceptionTest() {
        final String msg = "msg";
        AuthException authException = new AuthException(msg);
        Assertions.assertEquals(msg, authException.getMessage());

        final Throwable cause = mock(Throwable.class);
        authException = new AuthException(msg, cause);
        Assertions.assertEquals(msg, authException.getMessage());
        Assertions.assertEquals(cause, authException.getCause());
    }
}