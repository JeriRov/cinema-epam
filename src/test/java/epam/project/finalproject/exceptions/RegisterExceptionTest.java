package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class RegisterExceptionTest {
    @Test
    void registerExceptionTest() {
        final String msg = "msg";
        RegisterException registerException = new RegisterException(msg);
        Assertions.assertEquals(msg, registerException.getMessage());

        final Throwable cause = mock(Throwable.class);
        registerException = new RegisterException(msg, cause);
        Assertions.assertEquals(msg, registerException.getMessage());
        Assertions.assertEquals(cause, registerException.getCause());
    }

}