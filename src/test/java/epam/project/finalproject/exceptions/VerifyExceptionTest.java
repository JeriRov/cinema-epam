package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class VerifyExceptionTest {
    @Test
    void verifyExceptionTest() {
        final String msg = "msg";
        VerifyException verifyException = new VerifyException(msg);
        Assertions.assertEquals(msg, verifyException.getMessage());

        final Throwable cause = mock(Throwable.class);
        verifyException = new VerifyException(msg, cause);
        Assertions.assertEquals(msg, verifyException.getMessage());
        Assertions.assertEquals(cause, verifyException.getCause());
    }
}