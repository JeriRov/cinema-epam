package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class EmptyListExceptionTest {
    @Test
    void emptyListExceptionTest() {
        final String message = "msg";
        EmptyListException emptyListException = new EmptyListException(message);
        Assertions.assertEquals(message, emptyListException.getMessage());

        final Throwable cause = mock(Throwable.class);
        emptyListException = new EmptyListException(message, cause);
        Assertions.assertEquals(message, emptyListException.getMessage());
        Assertions.assertEquals(cause, emptyListException.getCause());
    }
}