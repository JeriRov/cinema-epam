package epam.project.finalproject.exceptions;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class DAOExceptionTest {
    @Test
    void daoExceptionTest() {
        final String msg = "msg";
        DAOException daoException = new DAOException(msg);
        Assertions.assertEquals(msg, daoException.getMessage());

        final Throwable cause = mock(Throwable.class);
        daoException = new DAOException(msg, cause);
        Assertions.assertEquals(msg, daoException.getMessage());
        Assertions.assertEquals(cause, daoException.getCause());
    }
}