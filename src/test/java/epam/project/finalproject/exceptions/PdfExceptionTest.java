package epam.project.finalproject.exceptions;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

class PdfExceptionTest {
    @Test
    void pdfExceptionTest() {
        final String msg = "msg";
        PdfException pdfException = new PdfException(msg);
        Assertions.assertEquals(msg, pdfException.getMessage());

        final Throwable cause = mock(Throwable.class);
        pdfException = new PdfException(msg, cause);
        Assertions.assertEquals(msg, pdfException.getMessage());
        Assertions.assertEquals(cause, pdfException.getCause());
    }
}