package epam.project.finalproject.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Duration;
import java.util.List;

class FilmTest {
    @Mock
    private List<Genre> genreList;
    private Film film;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        film = new Film();
    }

    @Test
    void filmConstructorsTest() {
        final int id = 1;
        final String name = "name";
        final String description = "description";
        final String poster = "poster";
        final Duration duration = Duration.ZERO;

        Film film = new Film(id, name, description, poster, duration, genreList);
        Assertions.assertNotNull(film);

        film = new Film(id, name, description, poster, duration);
        Assertions.assertNotNull(film);

        film = new Film(name, description, poster, duration);
        Assertions.assertNotNull(film);
    }

    @Test
    void id() {
        final int id = 1;
        film.setId(id);
        Assertions.assertEquals(id, film.getId());
    }

    @Test
    void duration() {
        final Duration duration = Duration.ofMinutes(10);
        film.setDuration(duration);
        Assertions.assertEquals(duration, film.getDuration());
    }

    @Test
    void durationInMinutes() {
        final Duration duration = Duration.ofMinutes(10);
        film.setDuration(duration);
        Assertions.assertEquals(10, film.getDurationInMinutes());
    }

    @Test
    void genreList() {
        film.setGenreList(genreList);
        Assertions.assertEquals(genreList, film.getGenreList());
    }

    @Test
    void name() {
        film.setName("name");
        Assertions.assertEquals("name", film.getName());
    }

    @Test
    void description() {
        film.setDescription("description");
        Assertions.assertEquals("description", film.getDescription());
    }

    @Test
    void posterUrl() {
        film.setPosterUrl("url");
        Assertions.assertEquals("url", film.getPosterUrl());
    }

    @Test
    void testToString() {
        final int id = 1;
        final String name = "name";
        final String description = "description";
        final String poster = "poster";
        final Duration duration = Duration.ZERO;
        String text = "Film{" +
                "duration=" + duration +
                ", genreList=" + genreList +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", posterUrl='" + poster + '\'' +
                '}';

        Film film = new Film(id, name, description, poster, duration, genreList);
        Assertions.assertEquals(text, film.toString());
    }
}