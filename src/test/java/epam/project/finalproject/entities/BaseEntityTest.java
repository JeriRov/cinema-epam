package epam.project.finalproject.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

class BaseEntityTest {
    @Test
    void baseEntityTest() {
        BaseEntity entity = Mockito.spy(BaseEntity.class);
        final int id = 1;
        entity.setId(id);
        Assertions.assertEquals(id, entity.getId());
        verify(entity, times(1)).setId(id);
        when(entity.getId()).thenReturn(id);
    }

}
