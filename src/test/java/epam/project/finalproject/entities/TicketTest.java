package epam.project.finalproject.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;

class TicketTest {

    private Ticket ticket;
    @Mock
    private Session session;
    @Mock
    private User user;
    @Mock
    private Seat seat;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        ticket = new Ticket();
    }

    @Test
    void ticketConstructorsTest() {
        final int id = 1;
        final BigDecimal price = BigDecimal.TEN;

        Ticket ticket = new Ticket(id, session, user, seat, price);
        Assertions.assertNotNull(ticket);

        ticket = new Ticket(session, user, seat, price);
        Assertions.assertNotNull(ticket);
    }

    @Test
    void id() {
        int id = 1;
        ticket.setId(id);
        Assertions.assertEquals(id, ticket.getId());
    }

    @Test
    void session() {
        ticket.setSession(session);
        Assertions.assertEquals(session, ticket.getSession());
    }

    @Test
    void user() {
        ticket.setUser(user);
        Assertions.assertEquals(user, ticket.getUser());
    }

    @Test
    void seat() {
        ticket.setSeat(seat);
        Assertions.assertEquals(seat, ticket.getSeat());
    }

    @Test
    void ticketPrice() {
        ticket.setTicketPrice(BigDecimal.TEN);
        Assertions.assertEquals(BigDecimal.TEN, ticket.getTicketPrice());
    }

    @Test
    void testToString() {
        final int id = 1;
        final BigDecimal price = BigDecimal.TEN;


        Ticket ticket = new Ticket(id, session, user, seat, price);
        String text = "Ticket{" +
                "session=" + session +
                ", user=" + user +
                ", seat=" + seat +
                ", ticketPrice=" + price +
                '}';
        Assertions.assertEquals(text, ticket.toString());
    }
}