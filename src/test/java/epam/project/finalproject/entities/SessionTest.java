package epam.project.finalproject.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.mockito.Mockito.*;

class SessionTest {
    private Session session;

    @BeforeEach
    public void setUp() {
        session = new Session();
    }

    @Test
    void sessionConstructorsTest() {
        final int id = 1;
        final Film film = mock(Film.class);
        final LocalDate date = LocalDate.EPOCH;
        final LocalTime time = LocalTime.NOON;
        final BigDecimal ticketPrice = BigDecimal.TEN;
        final int seatsAmount = 2;


        Session session = new Session(id, ticketPrice, date, time, film, seatsAmount);
        Assertions.assertNotNull(session);

        session = new Session(id, ticketPrice, date, time, seatsAmount);
        Assertions.assertNotNull(session);

        session = new Session(ticketPrice, date, time);
        Assertions.assertNotNull(session);
    }

    @Test
    void id() {
        int id = 1;
        session.setId(id);
        Assertions.assertEquals(id, session.getId());
    }

    @Test
    void ticketPrice() {
        session.setTicketPrice(BigDecimal.TEN);
        Assertions.assertEquals(BigDecimal.TEN, session.getTicketPrice());
    }

    @Test
    void date() {
        session.setDate(LocalDate.EPOCH);
        Assertions.assertEquals(LocalDate.EPOCH, session.getDate());
    }

    @Test
    void time() {
        session.setTime(LocalTime.NOON);
        Assertions.assertEquals(LocalTime.NOON, session.getTime());
    }

    @Test
    void film() {
        final Film film = mock(Film.class);
        session.setFilm(film);
        Assertions.assertEquals(film, session.getFilm());
    }

    @Test
    void seatsAmount() {
        int seatsAmount = 10;
        session.setSeatsAmount(seatsAmount);
        Assertions.assertEquals(seatsAmount, session.getSeatsAmount());
    }

    @Test
    void testToString() {
        final int id = 1;
        final Film film = mock(Film.class);
        final LocalDate date = LocalDate.EPOCH;
        final LocalTime time = LocalTime.NOON;
        final BigDecimal ticketPrice = BigDecimal.TEN;
        final int seatsAmount = 2;

        Session session = new Session(id, ticketPrice, date, time, film, seatsAmount);

        String text = "Session{" +
                "ticketPrice=" + ticketPrice +
                ", date=" + date +
                ", time=" + time +
                ", film=" + film +
                '}';
        Assertions.assertEquals(text, session.toString());
    }
}