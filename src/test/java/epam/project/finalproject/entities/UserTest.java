package epam.project.finalproject.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class UserTest {
    private User user;

    @BeforeEach
    public void setUp() {
        user = new User();
    }

    @Test
    void userConstructorsTest() {
        final int id = 1;
        final String firstName = "firstName";
        final String lastName = "lastName";
        final String salt = "salt";
        final String email = "email";
        final String number = "number";
        final String password = "password";
        final String verifyCode = "verify";
        final boolean notification = true;
        final boolean enabled = false;
        User.Role role = User.Role.USER;


        User user = new User(id, firstName, lastName, email, password, number, role, notification, salt, verifyCode, enabled);
        Assertions.assertNotNull(user);

        user = new User(id, firstName, lastName, email, password, notification, salt, verifyCode, enabled);
        Assertions.assertNotNull(user);

        user = new User(firstName, lastName, email, password, number, notification, salt, verifyCode, enabled);
        Assertions.assertNotNull(user);
    }

    @Test
    void id() {
        final int id = 1;
        user.setId(id);
        Assertions.assertEquals(id, user.getId());
    }

    @Test
    void firstName() {
        final String firstName = "firstName";
        user.setFirstName(firstName);
        Assertions.assertEquals(firstName, user.getFirstName());
    }

    @Test
    void lastName() {
        final String lastName = "lastName";
        user.setLastName(lastName);
        Assertions.assertEquals(lastName, user.getLastName());
    }

    @Test
    void email() {
        final String email = "email";
        user.setEmail(email);
        Assertions.assertEquals(email, user.getEmail());
    }

    @Test
    void password() {
        final String password = "password";
        user.setPassword(password);
        Assertions.assertEquals(password, user.getPassword());
    }

    @Test
    void phoneNumber() {
        final String number = "number";
        user.setPhoneNumber(number);
        Assertions.assertEquals(number, user.getPhoneNumber());
    }

    @Test
    void notification() {
        final boolean notification = true;
        user.setNotification(notification);
        Assertions.assertEquals(notification, user.getNotification());
    }

    @Test
    void userRole() {
        User.Role role = User.Role.USER;
        String userRole = "USER";
        String adminRole = "ADMIN";
        String guestRole = "GUEST";

        user.setUserRole(role);
        Assertions.assertEquals(role, user.getUserRole());
        Assertions.assertEquals(userRole, role.toString());

        role = User.Role.GUEST;
        user.setUserRole(role);
        Assertions.assertEquals(role, user.getUserRole());
        Assertions.assertEquals(guestRole, role.toString());

        role = User.Role.ADMIN;
        user.setUserRole(role);
        Assertions.assertEquals(role, user.getUserRole());
        Assertions.assertEquals(adminRole, role.toString());

        role = User.Role.getUserRoleFromString("user");
        user.setUserRole(role);
        Assertions.assertEquals(role, user.getUserRole());
        Assertions.assertEquals(userRole, role.toString());

        List<String> roles = Arrays.asList(userRole, adminRole, guestRole);
        role = User.Role.getRandomRole();
        Assertions.assertTrue(roles.contains(role.toString()));
        user.setUserRole(role);
        Assertions.assertEquals(role, user.getUserRole());
        Assertions.assertTrue(roles.contains(user.getUserRole().toString()));
    }

    @Test
    void salt() {
        final String salt = "salt";
        user.setSalt(salt);
        Assertions.assertEquals(salt, user.getSalt());
    }

    @Test
    void verificationCode() {
        final String verificationCode = "verificationCode";
        user.setVerificationCode(verificationCode);
        Assertions.assertEquals(verificationCode, user.getVerificationCode());
    }

    @Test
    void enabled() {
        final boolean enabled = true;
        user.setEnabled(enabled);
        Assertions.assertEquals(enabled, user.isEnabled());
    }
}