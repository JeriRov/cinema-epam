package epam.project.finalproject.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SeatTest {

    private Seat seat;

    @BeforeEach
    public void setUp() {
        seat = new Seat();
    }

    @Test
    void seatConstructorsTest() {
        final int id = 1;
        final int rowNumber = 2;
        final int placeNumber = 4;

        Seat seat = new Seat(id, rowNumber, placeNumber);
        Assertions.assertNotNull(seat);

        seat = new Seat(rowNumber, placeNumber);
        Assertions.assertNotNull(seat);
    }

    @Test
    void id() {
        final int id = 1;
        seat.setId(id);
        Assertions.assertEquals(id, seat.getId());
    }

    @Test
    void rowNumber() {
        final int rowNumber = 2;
        seat.setRowNumber(rowNumber);
        Assertions.assertEquals(rowNumber, seat.getRowNumber());
    }

    @Test
    void placeNumber() {
        final int placeNumber = 1;
        seat.setPlaceNumber(placeNumber);
        Assertions.assertEquals(placeNumber, seat.getPlaceNumber());
    }

    @Test
    void testToString() {
        final int id = 1;
        final int rowNumber = 2;
        final int placeNumber = 4;


        Seat seat = new Seat(id, rowNumber, placeNumber);
        String text = "Seat{" +
                "rowNumber=" + rowNumber +
                ", placeNumber=" + placeNumber +
                '}';
        Assertions.assertEquals(text, seat.toString());
    }
}