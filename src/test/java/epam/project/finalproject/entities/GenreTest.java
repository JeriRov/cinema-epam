package epam.project.finalproject.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GenreTest {
    private Genre genre;

    @BeforeEach
    public void setUp() {
        genre = new Genre();
    }

    @Test
    void genreConstructorsTest() {
        final int id = 1;
        final String name = "genre";
        Genre seat = new Genre(id, name);
        Assertions.assertNotNull(seat);
    }

    @Test
    void id() {
        final int id = 1;
        genre.setId(id);
        Assertions.assertEquals(id, genre.getId());
    }

    @Test
    void name() {
        final String name = "name";
        genre.setName(name);
        Assertions.assertEquals(name, genre.getName());
    }

}