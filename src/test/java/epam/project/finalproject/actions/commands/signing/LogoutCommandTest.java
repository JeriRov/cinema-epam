package epam.project.finalproject.actions.commands.signing;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.services.CookieService;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.USER_ID;
import static epam.project.finalproject.utilities.constants.OtherConstants.USER_ROLE;
import static org.mockito.Mockito.*;

class LogoutCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private CookieService cookieService;
    @Mock
    private HttpSession session;
    private LogoutCommand logoutCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        logoutCommand = new LogoutCommand(cookieService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(USER_ID)).thenReturn("4");
        when(session.getAttribute(USER_ROLE)).thenReturn(User.Role.getRandomRole());

        logoutCommand.execute(request, response);

        verify(response, times(1)).sendRedirect(CommandConstants.COMMAND_MAIN_SERVLET);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws IOException {
        logoutCommand.execute(request, response);

        verify(response, never()).sendRedirect(CommandConstants.COMMAND_MAIN_SERVLET);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
    }
}