package epam.project.finalproject.actions.commands.tickets;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.services.CookieService;
import epam.project.finalproject.services.TicketPdfService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.UserService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.TICKET_ID;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DownloadPDFTicketCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private TicketService ticketService;
    @Mock
    private TicketPdfService ticketPdfService;
    private DownloadPDFTicketCommand downloadPDFTicketCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        downloadPDFTicketCommand = new DownloadPDFTicketCommand(ticketService, ticketPdfService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() {
        when(request.getParameter(TICKET_ID)).thenReturn("1");
        downloadPDFTicketCommand.execute(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() {
        downloadPDFTicketCommand.execute(request, response);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
    }
}