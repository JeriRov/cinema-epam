package epam.project.finalproject.actions.commands.films;

import epam.project.finalproject.entities.Film;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.GenreService;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ADD_FILM_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class AddFilmCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private GenreService genreService;
    @Mock
    private FilmService filmService;
    private Map<String, String> parameters;
    private AddFilmCommand addFilmCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        parameters = new HashMap<>();
        addFilmCommand = new AddFilmCommand(filmService, genreService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);

        parameters.put(FILM_NAME_PARAM, RandomString.make());
        parameters.put(FILM_DESCR_PARAM, RandomString.make());
        parameters.put(POSTER_URL_PARAM, RandomString.make());
        parameters.put(FILM_DURATION_PARAM, "100");
        parameters.put(FILM_ID_PARAM, "1");
    }

    @Test
    void execute() throws IOException {
        requestParameters();
        addFilmCommand.execute(request, response);
        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_FILMS_SETTING_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeForwardWithErrors() throws IOException, ServletException {
        requestParameters();

        Map<String, String[]> genreParams = new HashMap<>();
        String[] genreIds = {"1", "14"};
        genreParams.put(GENRE_IDS_PARAM, genreIds);

        when(request.getParameterMap()).thenReturn(genreParams);

        List<String> errorList = new ArrayList<>() {{
            add(VALID_FILM_EMPTY);
        }};
        when(filmService.getFilmValidErrorList(anyMap(), any())).thenReturn(errorList);


        when(request.getRequestDispatcher(ADD_FILM_PAGE_PATH)).thenReturn(dispatcher);
        addFilmCommand.execute(request, response);

        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_FILMS_SETTING_PAGE));
        verify(request, times(1)).getRequestDispatcher(ADD_FILM_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServiceException, IOException, ServletException {
        requestParameters();

        Map<String, String[]> genreParams = new HashMap<>();
        String[] genreIds = {"1", "14"};
        genreParams.put(GENRE_IDS_PARAM, genreIds);

        when(request.getParameterMap()).thenReturn(genreParams);

        doThrow(ServiceException.class).when(filmService).create(any(Film.class));

        addFilmCommand.execute(request, response);

        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_FILMS_SETTING_PAGE));
        verify(request, never()).getRequestDispatcher(ADD_FILM_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }

    private void requestParameters() {
        for (var param : parameters.entrySet()) {
            when(request.getParameter(param.getKey())).thenReturn(param.getValue());
        }
    }
}