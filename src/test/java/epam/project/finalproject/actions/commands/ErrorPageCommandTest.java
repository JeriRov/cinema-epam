package epam.project.finalproject.actions.commands;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static org.mockito.Mockito.*;

class ErrorPageCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    private ErrorPageCommand errorPageCommand;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        errorPageCommand = new ErrorPageCommand();
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() {
        errorPageCommand.execute(request, response);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        doThrow(ServletException.class).when(dispatcher).forward(request, response);
        errorPageCommand.execute(null, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }
}