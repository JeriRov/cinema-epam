package epam.project.finalproject.actions.commands.films;

import epam.project.finalproject.entities.Film;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.GenreService;
import epam.project.finalproject.services.PaginationService;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.random.RandomGenerator;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.*;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

class FilmsSettingPageCommandTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private HttpSession session;
    @Mock
    private PaginationService paginationService;
    @Mock
    private Film film;
    @Mock
    private FilmService filmService;
    private Map<String, String> parameters;
    private FilmsSettingPageCommand filmsSettingPageCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        filmsSettingPageCommand = new FilmsSettingPageCommand(filmService, paginationService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServiceException, ServletException, IOException {
        when(filmService.countTotalPages(anyInt())).thenReturn(5);
        List<Film> filmList = new ArrayList<>();
        filmList.add(film);

        Map<String, Integer> map = new HashMap<>();
        map.put(PAGE_NO_PARAM, 2);
        map.put(PAGE_SIZE_PARAM, 4);

        when(filmService.getAll(anyInt(), anyInt())).thenReturn(filmList);
        when(paginationService.getPaginationParamsFromRequest(any())).thenReturn(map);
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher(FILMS_SETTING_PAGE_PATH)).thenReturn(dispatcher);

        filmsSettingPageCommand.execute(request, response);


        verify(request, times(1)).getRequestDispatcher(FILMS_SETTING_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        filmsSettingPageCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(FILMS_SETTING_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}