package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.entities.Film;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.FilmService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ADD_SESSION_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static org.mockito.Mockito.*;

class AddSessionPageCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private FilmService filmService;
    @Mock
    Film film;
    private AddSessionPageCommand addSessionPageCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        addSessionPageCommand = new AddSessionPageCommand(filmService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServiceException {
        List<Film> filmList = new ArrayList<>();
        filmList.add(film);
        when(filmService.getAll()).thenReturn(filmList);
        when(request.getRequestDispatcher(ADD_SESSION_PAGE_PATH)).thenReturn(dispatcher);

        addSessionPageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(ADD_SESSION_PAGE_PATH);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServiceException, ServletException, IOException {
        when(filmService.getAll()).thenThrow(ServiceException.class);

        addSessionPageCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(ADD_SESSION_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}