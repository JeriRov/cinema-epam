package epam.project.finalproject.actions.commands.signing;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.CookieService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.utilities.RedirectManager;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND_VIEW_MAIN_PAGE;
import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND_VIEW_PROFILE_PAGE;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.LOGIN_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class LoginCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private UserService userService;
    @Mock
    private CookieService cookieService;
    @Mock
    private User user;
    @Mock
    private HttpSession session;

    private LoginCommand loginCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        loginCommand = new LoginCommand(userService, cookieService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void executeForUser() throws ServiceException, IOException {
        when(request.getParameter(LOGIN)).thenReturn("login");
        when(request.getParameter(PASSWORD)).thenReturn("password");
        when(request.getParameter(REMEMBER_ME)).thenReturn("rememberMe");
        when(userService.validateUserEmail(anyString())).thenReturn("");
        when(userService.getUserByLogin(anyString())).thenReturn(user);
        when(user.getUserRole()).thenReturn(User.Role.USER);
        when(request.getSession(true)).thenReturn(session);

        loginCommand.execute(request, response);
        verify(request, never()).getRequestDispatcher(LOGIN_PAGE_PATH);
        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(COMMAND_VIEW_PROFILE_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeForAdmin() throws ServiceException, IOException {
        when(request.getParameter(LOGIN)).thenReturn("login");
        when(request.getParameter(PASSWORD)).thenReturn("password");
        when(request.getParameter(REMEMBER_ME)).thenReturn("rememberMe");
        when(userService.validateUserEmail(anyString())).thenReturn("");
        when(userService.getUserByLogin(anyString())).thenReturn(user);
        when(user.getUserRole()).thenReturn(User.Role.ADMIN);
        when(request.getSession(true)).thenReturn(session);
        when(request.getRequestDispatcher(LOGIN_PAGE_PATH)).thenReturn(dispatcher);

        loginCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(LOGIN_PAGE_PATH);
        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(COMMAND_VIEW_MAIN_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeForGuest() throws ServiceException, IOException {
        when(request.getParameter(LOGIN)).thenReturn("login");
        when(request.getParameter(PASSWORD)).thenReturn("password");
        when(request.getParameter(REMEMBER_ME)).thenReturn("rememberMe");
        when(userService.validateUserEmail(anyString())).thenReturn("");
        when(userService.getUserByLogin(anyString())).thenReturn(user);
        when(user.getUserRole()).thenReturn(User.Role.GUEST);
        when(request.getSession(true)).thenReturn(session);
        when(request.getRequestDispatcher(LOGIN_PAGE_PATH)).thenReturn(dispatcher);

        loginCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(LOGIN_PAGE_PATH);
        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(COMMAND_VIEW_MAIN_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeInvalidEmail() throws IOException, ServletException {
        when(request.getParameter(LOGIN)).thenReturn("login");
        when(request.getParameter(PASSWORD)).thenReturn("password");
        when(request.getParameter(REMEMBER_ME)).thenReturn("rememberMe");
        when(userService.validateUserEmail(anyString())).thenReturn(VALID_EMAIL_INVALID);
        when(request.getRequestDispatcher(LOGIN_PAGE_PATH)).thenReturn(dispatcher);

        loginCommand.execute(request, response);

        verify(dispatcher).forward(request, response);
        verify(request, times(1)).getRequestDispatcher(LOGIN_PAGE_PATH);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws IOException {
        when(request.getRequestDispatcher(LOGIN_PAGE_PATH)).thenReturn(dispatcher);
        loginCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(LOGIN_PAGE_PATH);
        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(COMMAND_VIEW_MAIN_PAGE));
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
    }
}