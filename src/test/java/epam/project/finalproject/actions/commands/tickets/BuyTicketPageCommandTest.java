package epam.project.finalproject.actions.commands.tickets;

import epam.project.finalproject.services.SeatService;
import epam.project.finalproject.services.SessionService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.UserService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.PAYING_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class BuyTicketPageCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private UserService userService;
    @Mock
    private TicketService ticketService;
    @Mock
    private SessionService sessionService;
    @Mock
    private SeatService seatService;
    @Mock
    private HttpSession session;
    private BuyTicketPageCommand buyTicketPageCommand;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        buyTicketPageCommand = new BuyTicketPageCommand(ticketService, sessionService, userService, seatService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServletException, IOException {
        String[] seats = {"1"};
        Map<String, String[]> seatsMap = new HashMap<>();
        seatsMap.put(SEAT_IDS, seats);
        when(request.getParameterMap()).thenReturn(seatsMap);

        when(request.getParameter(SESSION_ID)).thenReturn("5");

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(USER_ID)).thenReturn("5");

        when(request.getRequestDispatcher(PAYING_PAGE_PATH)).thenReturn(dispatcher);

        buyTicketPageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(PAYING_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        when(request.getRequestDispatcher(PAYING_PAGE_PATH)).thenReturn(dispatcher);

        buyTicketPageCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(PAYING_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}