package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.PaginationService;
import epam.project.finalproject.services.SessionService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.SESSIONS_SETTING_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.PAGE_NO_PARAM;
import static epam.project.finalproject.utilities.constants.OtherConstants.PAGE_SIZE_PARAM;
import static org.mockito.Mockito.*;

class SessionsSettingPageCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private SessionService sessionService;
    @Mock
    private PaginationService paginationService;
    private SessionsSettingPageCommand sessionInfoPageCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        sessionInfoPageCommand = new SessionsSettingPageCommand(sessionService, paginationService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServletException, IOException {
        Map<String, Integer> map = new HashMap<>();
        map.put(PAGE_NO_PARAM, 5);
        map.put(PAGE_SIZE_PARAM, 3);
        when(paginationService.getPaginationParamsFromRequest(request)).thenReturn(map);
        when(request.getRequestDispatcher(SESSIONS_SETTING_PAGE_PATH)).thenReturn(dispatcher);
        sessionInfoPageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(SESSIONS_SETTING_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }
    @Test
    void executeThrows() throws ServletException, IOException, ServiceException {
        when(request.getRequestDispatcher(SESSIONS_SETTING_PAGE_PATH)).thenReturn(dispatcher);
        when(sessionService.countTotalPages(anyInt())).thenThrow(ServiceException.class);
        sessionInfoPageCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(SESSIONS_SETTING_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}