package epam.project.finalproject.actions.commands.films;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.GenreService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.*;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static org.mockito.Mockito.*;

class AddFilmPageCommandTest {

    @Test
    void execute() throws ServletException, IOException, ServiceException {
        final GenreService genreService = mock(GenreService.class);

        when(genreService.getAll()).thenReturn(new ArrayList<>());

        final AddFilmPageCommand filmPageCommand = new AddFilmPageCommand(genreService);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getRequestDispatcher(ADD_FILM_PAGE_PATH)).thenReturn(dispatcher);

        filmPageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(ADD_FILM_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException, ServiceException {
        final GenreService genreService = mock(GenreService.class);

        when(genreService.getAll()).thenThrow(ServiceException.class);

        final AddFilmPageCommand filmPageCommand = new AddFilmPageCommand(genreService);

        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);

        filmPageCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(ADD_FILM_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}