package epam.project.finalproject.actions.commands.verify;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.VerifyService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.jsp.jstl.core.Config;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.SUCCESS_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.USER_ID;
import static org.mockito.Mockito.*;

class MailVerifyCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private UserService userService;
    @Mock
    private VerifyService verifyService;
    @Mock
    private HttpSession session;
    @Mock
    private User user;
    private MailVerifyCommand mailVerifyCommand;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mailVerifyCommand = new MailVerifyCommand(userService, verifyService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServiceException, ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(USER_ID)).thenReturn("4");
        when(userService.getById(anyInt())).thenReturn(user);
        when(userService.save(any(User.class))).thenReturn(user);
        when(request.getRequestDispatcher(SUCCESS_PAGE_PATH)).thenReturn(dispatcher);
        MockedStatic<ResourceBundle> resourceBundleMockedStatic = mockStatic(ResourceBundle.class);

        ResourceBundle bundle = mock(ResourceBundle.class);
        when(bundle.getString(anyString())).thenReturn("text");
        Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
        resourceBundleMockedStatic.when(() -> ResourceBundle.getBundle("i18n", locale)).thenReturn(bundle);

        mailVerifyCommand.execute(request, response);

        resourceBundleMockedStatic.close();

        verify(request, times(1)).getRequestDispatcher(SUCCESS_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        mailVerifyCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(SUCCESS_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }

}