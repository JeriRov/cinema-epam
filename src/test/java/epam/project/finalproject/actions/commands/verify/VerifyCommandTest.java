package epam.project.finalproject.actions.commands.verify;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.VerifyService;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.VERIFICATION_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class VerifyCommandTest {

    @Mock

    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private VerifyService verifyService;
    private VerifyCommand verifyCommand;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        verifyCommand = new VerifyCommand(verifyService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void executeGoodVerify() throws ServiceException {
        when(request.getParameter(VERIFY_CODE)).thenReturn(RandomString.make());
        when(request.getRequestDispatcher(VERIFICATION_PAGE_PATH)).thenReturn(dispatcher);
        when(verifyService.verify(anyString())).thenReturn(true);

        verifyCommand.execute(request, response);
        verify(request, times(1)).setAttribute(VERIFY_SUCCESS_PAGE_TEXT, GOOD_VERIFY_MESSAGE);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeBadVerify() throws ServiceException {
        when(request.getParameter(VERIFY_CODE)).thenReturn(RandomString.make());
        when(request.getRequestDispatcher(VERIFICATION_PAGE_PATH)).thenReturn(dispatcher);
        when(verifyService.verify(anyString())).thenReturn(false);

        verifyCommand.execute(request, response);
        verify(request, times(1)).setAttribute(VERIFY_SUCCESS_PAGE_TEXT, BAD_VERIFY_MESSAGE);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServiceException, ServletException, IOException {
        when(request.getParameter(VERIFY_CODE)).thenReturn(RandomString.make());
        when(request.getRequestDispatcher(VERIFICATION_PAGE_PATH)).thenReturn(dispatcher);
        when(verifyService.verify(anyString())).thenThrow(ServiceException.class);

        verifyCommand.execute(request, response);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}