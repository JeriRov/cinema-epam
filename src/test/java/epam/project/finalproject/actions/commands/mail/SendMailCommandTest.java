package epam.project.finalproject.actions.commands.mail;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.MailService;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.mail.MessagingException;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class SendMailCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private MailService mailService;
    private SendMailCommand sendMailCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        sendMailCommand = new SendMailCommand(mailService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws IOException {
        Object mail = "example@gmail.com";
        Object subject = RandomString.make();
        Object body = RandomString.make();

        when(request.getAttribute(USER_MAIL)).thenReturn(mail);
        when(request.getAttribute(MAIL_SUBJECT)).thenReturn(subject);
        when(request.getAttribute(MAIL_BODY)).thenReturn(body);

        sendMailCommand.execute(request, response);

        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_PROFILE_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws IOException, ServletException, ServiceException {
        Object mail = "example@gmail.com";
        Object subject = RandomString.make();
        Object body = RandomString.make();

        when(request.getAttribute(USER_MAIL)).thenReturn(mail);
        when(request.getAttribute(MAIL_SUBJECT)).thenReturn(subject);
        when(request.getAttribute(MAIL_BODY)).thenReturn(body);

        doThrow(ServiceException.class).when(mailService).sendEmail(anyString(), anyString(), anyString());

        sendMailCommand.execute(request, response);

        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_PROFILE_PAGE));
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}