package epam.project.finalproject.actions.commands.mail;

import epam.project.finalproject.entities.*;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.MailService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.UserService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Locale;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class SendTicketViaMailCommandTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private UserService userService;
    @Mock
    private TicketService ticketService;
    @Mock
    private Ticket ticket;
    @Mock
    private User user;
    @Mock
    private Film film;
    @Mock
    private HttpSession session;
    @Mock
    private Session cinemaSession;
    @Mock
    private MailService mailService;
    private SendTicketViaMailCommand sendMailCommand;
    private Locale locale;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        locale = Locale.getDefault();
        sendMailCommand = new SendTicketViaMailCommand(userService, ticketService, mailService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServiceException {
        when(request.getParameter(TICKET_ID)).thenReturn("2");

        when(ticketService.getById(anyInt())).thenReturn(ticket);
        when(ticket.getUser()).thenReturn(user);
        when(ticket.getSession()).thenReturn(cinemaSession);
        when(cinemaSession.getFilm()).thenReturn(film);
        when(ticket.getSeat()).thenReturn(new Seat(1, 2));

        when(user.getId()).thenReturn(4);
        when(session.getAttribute(anyString())).thenReturn(locale);
        when(request.getSession()).thenReturn(session);

        when(userService.getById(anyInt())).thenReturn(user);

        Object mail = "example@gmail.com";
        Object subject = RandomString.make();
        Object body = RandomString.make();

        when(request.getAttribute(USER_MAIL)).thenReturn(mail);
        when(request.getAttribute(MAIL_SUBJECT)).thenReturn(subject);
        when(request.getAttribute(MAIL_BODY)).thenReturn(body);

        sendMailCommand.execute(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        sendMailCommand.execute(request, response);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}