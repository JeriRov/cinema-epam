package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.services.SeatService;
import epam.project.finalproject.services.SessionService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.SESSIONS_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.SESSION_ID;
import static org.mockito.Mockito.*;

class SessionInfoPageCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private SessionService sessionService;
    @Mock
    private SeatService seatService;
    private SessionInfoPageCommand sessionInfoPageCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        sessionInfoPageCommand = new SessionInfoPageCommand(sessionService, seatService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServletException, IOException {
        when(request.getParameter(SESSION_ID)).thenReturn("1");
        when(request.getRequestDispatcher(SESSIONS_PAGE_PATH)).thenReturn(dispatcher);

        sessionInfoPageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(SESSIONS_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {

        when(request.getRequestDispatcher(SESSIONS_PAGE_PATH)).thenReturn(dispatcher);
        sessionInfoPageCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(SESSIONS_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}