package epam.project.finalproject.actions.commands.tickets;

import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.services.PaymentService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class BuyTicketCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private TicketService ticketService;
    @Mock
    private HttpSession session;
    @Mock
    private UserService userService;

    @Mock
    private PaymentService paymentService;
    private BuyTicketCommand buyTicketCommand;

    @Mock
    private BuyTicketPageCommand buyTicketPageCommand;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        buyTicketCommand = new BuyTicketCommand(ticketService, userService, paymentService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws IOException {
        Ticket ticket = mock(Ticket.class);
        List<Ticket> tickets = Collections.singletonList(ticket);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(TICKET_LIST)).thenReturn(tickets);
        when(request.getSession().getAttribute(USER_ID)).thenReturn(1);
        when(request.getParameter(CREDIT_CARD_NUMBER)).thenReturn("1111 1111 1111 1111");
        when(request.getParameter(CREDIT_CARD_DATE)).thenReturn("02/22");
        when(request.getParameter(CREDIT_CARD_CVV)).thenReturn("123");
        when(request.getParameter("totalCost")).thenReturn("400");
        buyTicketCommand.execute(request, response);

        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_SUCCESS_PAY_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws IOException, ServletException {
        buyTicketCommand.execute(request, response);

        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_SUCCESS_PAY_PAGE));
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}