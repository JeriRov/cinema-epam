package epam.project.finalproject.actions.commands;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.PaginationService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.UserService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.*;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class ProfilePageCommandTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private PaginationService paginationService;
    @Mock
    private UserService userService;
    @Mock
    private TicketService ticketService;
    @Mock
    private HttpSession session;
    @Mock
    private User user;
    private ProfilePageCommand profilePageCommand;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        profilePageCommand = new ProfilePageCommand(ticketService, paginationService, userService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServletException, IOException, ServiceException {
        Map<String, Integer> paginationMap = new HashMap<>();
        paginationMap.put(PAGE_NO_PARAM, 3);
        paginationMap.put(PAGE_SIZE_PARAM, 2);
        when(paginationService.getPaginationParamsFromRequest(request)).thenReturn(paginationMap);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(USER_ID)).thenReturn("5");
        when(userService.getById(anyInt())).thenReturn(user);
        when(request.getRequestDispatcher(USER_PROFILE_PAGE_PATH)).thenReturn(dispatcher);

        profilePageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(USER_PROFILE_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        when(request.getRequestDispatcher(USER_PROFILE_PAGE_PATH)).thenReturn(dispatcher);

        profilePageCommand.execute(request, response);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(request, never()).getRequestDispatcher(USER_PROFILE_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}