package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.services.SessionService;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.SESSION_ID;
import static org.mockito.Mockito.*;

class DeleteSessionCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private SessionService sessionService;
    private DeleteSessionCommand deleteSessionCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        deleteSessionCommand = new DeleteSessionCommand(sessionService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws IOException {
        when(request.getParameter(SESSION_ID)).thenReturn("4");
        deleteSessionCommand.execute(request, response);
        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_SESSIONS_SETTING_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws IOException, ServletException {
        deleteSessionCommand.execute(request, response);
        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_SESSIONS_SETTING_PAGE));
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}
