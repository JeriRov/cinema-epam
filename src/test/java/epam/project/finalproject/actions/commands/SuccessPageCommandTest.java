package epam.project.finalproject.actions.commands;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.Locale;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.SUCCESS_PAGE_PATH;
import static org.mockito.Mockito.*;

class SuccessPageCommandTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private HttpSession session;
    private SuccessPageCommand successPayPageCommand;
    private Locale locale;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        locale = Locale.getDefault();
        successPayPageCommand = new SuccessPageCommand();
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher(SUCCESS_PAGE_PATH)).thenReturn(dispatcher);
        when(session.getAttribute(anyString())).thenReturn(locale);

        successPayPageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(SUCCESS_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        when(request.getRequestDispatcher(SUCCESS_PAGE_PATH)).thenReturn(dispatcher);

        successPayPageCommand.execute(request, response);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(request, never()).getRequestDispatcher(SUCCESS_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}