package epam.project.finalproject.actions.commands;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.PaginationService;
import epam.project.finalproject.services.SessionService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.SCHEDULE_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class ScheduleCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private PaginationService paginationService;
    @Mock
    private SessionService sessionService;
    private ScheduleCommand scheduleCommand;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        scheduleCommand = new ScheduleCommand(sessionService, paginationService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void executeParamMapIsEmpty() throws ServletException, IOException {
        Map<String, Integer> paginationMap = new HashMap<>();
        paginationMap.put(PAGE_NO_PARAM, 3);
        paginationMap.put(PAGE_SIZE_PARAM, 2);
        when(paginationService.getPaginationParamsFromRequest(request)).thenReturn(paginationMap);
        when(request.getRequestDispatcher(SCHEDULE_PAGE_PATH)).thenReturn(dispatcher);

        scheduleCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(SCHEDULE_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeParamMapIsNotEmpty() throws ServletException, IOException, ServiceException {
        Map<String, Integer> paginationMap = new HashMap<>();
        paginationMap.put(PAGE_NO_PARAM, 3);
        paginationMap.put(PAGE_SIZE_PARAM, 2);
        when(paginationService.getPaginationParamsFromRequest(request)).thenReturn(paginationMap);
        when(request.getRequestDispatcher(SCHEDULE_PAGE_PATH)).thenReturn(dispatcher);
        Map<String, String> paramMap = Map.of(SESSION_SORT_METHOD_PARAM_NAME, SESSION_SORT_METHOD_DESC);
        when(sessionService.getFilterSortMapFromParams(anyMap())).thenReturn(paramMap);

        scheduleCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(SCHEDULE_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(sessionService, times(1)).getFilteredAndSorted(anyMap(), anyInt(), anyInt());
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }


    @Test
    void executeThrows() throws ServletException, IOException {
        when(request.getRequestDispatcher(SCHEDULE_PAGE_PATH)).thenReturn(dispatcher);

        scheduleCommand.execute(request, response);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(request, never()).getRequestDispatcher(SCHEDULE_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}