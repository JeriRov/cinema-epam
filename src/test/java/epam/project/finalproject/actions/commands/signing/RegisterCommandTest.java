package epam.project.finalproject.actions.commands.signing;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.CaptchaService;
import epam.project.finalproject.services.CookieService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class RegisterCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private UserService userService;
    @Mock
    private CookieService cookieService;
    @Mock
    private CaptchaService captchaService;
    @Mock
    private HttpSession session;
    private RegisterCommand registerCommand;
    private Map<String, String> parameters;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        registerCommand = new RegisterCommand(userService, cookieService, captchaService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);

        parameters = new HashMap<>();
        parameters.put(F_NAME_PARAM, RandomString.make());
        parameters.put(L_NAME_PARAM, RandomString.make());
        parameters.put(EMAIL_PARAM, RandomString.make());
        parameters.put(PASS_PARAM, RandomString.make());
        parameters.put(PASS_CONFIRM_PARAM, RandomString.make());
        parameters.put(PHONE_PARAM, RandomString.make());
        parameters.put(NOTIFICATION_PARAM, RandomString.make());
    }

    @Test
    void execute() throws ServiceException, IOException {
        requestParameters();
        when(userService.create(any(User.class))).thenReturn(true);
        when(request.getSession(true)).thenReturn(session);
        when(request.getRequestDispatcher(JspPagePathConstants.REGISTER_PAGE_PATH)).thenReturn(dispatcher);

        registerCommand.execute(request, response);
        verify(request, never()).getRequestDispatcher(JspPagePathConstants.REGISTER_PAGE_PATH);
        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_PROFILE_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeForwardWithErrors() throws ServletException, IOException {
        requestParameters();
        List<String> errorList = List.of(VALID_PHONE_INVALID);
        when(userService.getUserValidErrorList(anyMap())).thenReturn(errorList);
        when(request.getRequestDispatcher(JspPagePathConstants.REGISTER_PAGE_PATH)).thenReturn(dispatcher);
        registerCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(JspPagePathConstants.REGISTER_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_PROFILE_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws IOException, ServiceException {
        requestParameters();
        when(userService.create(any(User.class))).thenReturn(false);
        when(request.getRequestDispatcher(JspPagePathConstants.REGISTER_PAGE_PATH)).thenReturn(dispatcher);
        registerCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(JspPagePathConstants.REGISTER_PAGE_PATH);
        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_PROFILE_PAGE));
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    private void requestParameters() {
        for (var param : parameters.entrySet()) {
            when(request.getParameter(param.getKey())).thenReturn(param.getValue());
        }
    }
}