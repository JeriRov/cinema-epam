package epam.project.finalproject.actions.commands;

import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.PaginationService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.MAIN_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.PAGE_NO_PARAM;
import static epam.project.finalproject.utilities.constants.OtherConstants.PAGE_SIZE_PARAM;
import static org.mockito.Mockito.*;

class MainPageCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private FilmService filmService;
    @Mock
    private PaginationService paginationService;
    @Mock private HttpSession session;
    private MainPageCommand mainPageCommand;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mainPageCommand = new MainPageCommand(filmService, paginationService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServletException, IOException {
        Map<String, Integer> paginationMap = new HashMap<>();
        paginationMap.put(PAGE_NO_PARAM , 3);
        paginationMap.put( PAGE_SIZE_PARAM, 2);
        when(paginationService.getPaginationParamsFromRequest(request)).thenReturn(paginationMap);
        when(request.getSession()).thenReturn(session);
        when(request.getRequestDispatcher(MAIN_PAGE_PATH)).thenReturn(dispatcher);

        mainPageCommand.execute(request, response);
        verify(request, times(1)).getRequestDispatcher(MAIN_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        when(request.getRequestDispatcher(MAIN_PAGE_PATH)).thenReturn(dispatcher);

        mainPageCommand.execute(request, response);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(request, never()).getRequestDispatcher(MAIN_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}