package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.SessionService;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND_VIEW_SESSIONS_SETTING_PAGE;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ADD_SESSION_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.Mockito.*;

class AddSessionCommandTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private FilmService filmService;
    @Mock
    private SessionService sessionService;
    private AddSessionCommand addSessionCommand;
    private Map<String, String> parameters;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        parameters = new HashMap<>();
        addSessionCommand = new AddSessionCommand(sessionService, filmService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);

        parameters.put(FILM_ID_PARAM, "5");
        parameters.put(SESSION_TIME_PARAM, String.valueOf(LocalTime.of(9, 0)));
        parameters.put(SESSION_DATE_PARAM, String.valueOf(LocalDate.now()));
        parameters.put(SESSION_PRICE_PARAM, "100");
    }

    @Test
    void execute() throws IOException {
        requestParameters();
        addSessionCommand.execute(request, response);
        verify(response, times(1)).sendRedirect(RedirectManager.getRedirectLocation(COMMAND_VIEW_SESSIONS_SETTING_PAGE));
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeForwardWithErrors() throws IOException, ServletException {
        requestParameters();

        List<String> errorList = new ArrayList<>();
        errorList.add(VALID_FILM_EMPTY);
        when(sessionService.getSessionFieldsValidErrorList(anyMap())).thenReturn(errorList);
        when(request.getRequestDispatcher(ADD_SESSION_PAGE_PATH)).thenReturn(dispatcher);

        addSessionCommand.execute(request, response);

        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(COMMAND_VIEW_SESSIONS_SETTING_PAGE));
        verify(request, times(1)).getRequestDispatcher(ADD_SESSION_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws IOException, ServletException {
        addSessionCommand.execute(request, response);

        verify(response, never()).sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_SESSIONS_SETTING_PAGE));
        verify(request, never()).getRequestDispatcher(ADD_SESSION_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }

    private void requestParameters() {
        for (var param : parameters.entrySet()) {
            when(request.getParameter(param.getKey())).thenReturn(param.getValue());
        }
    }

}