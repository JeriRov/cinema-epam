package epam.project.finalproject.actions.commands.signing;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.LOGIN_PAGE_PATH;
import static org.mockito.Mockito.*;

class LoginPageCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;

    private LoginPageCommand loginPageCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        loginPageCommand = new LoginPageCommand();
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServletException, IOException {
        when(request.getRequestDispatcher(LOGIN_PAGE_PATH)).thenReturn(dispatcher);
        loginPageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(LOGIN_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        RequestDispatcher secondDispatcher = mock(RequestDispatcher.class);
        when(request.getRequestDispatcher(LOGIN_PAGE_PATH)).thenReturn(secondDispatcher);
        doThrow(ServletException.class).when(secondDispatcher).forward(request, response);
        loginPageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}