package epam.project.finalproject.actions.commands.films;

import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.GenreService;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.JspPagePathConstants.*;
import static epam.project.finalproject.utilities.constants.JspPagePathConstants.ERROR_PAGE_PATH;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

class FilmInfoPageCommandTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher dispatcher;
    @Mock
    private FilmService filmService;
    private FilmInfoPageCommand filmInfoPageCommand;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        filmInfoPageCommand = new FilmInfoPageCommand(filmService);
        when(request.getRequestDispatcher(ERROR_PAGE_PATH)).thenReturn(dispatcher);
    }

    @Test
    void execute() throws ServletException, IOException {
        when(request.getParameter(anyString())).thenReturn("2");
        when(request.getRequestDispatcher(FILM_INFO_PAGE_PATH)).thenReturn(dispatcher);

        filmInfoPageCommand.execute(request, response);

        verify(request, times(1)).getRequestDispatcher(FILM_INFO_PAGE_PATH);
        verify(dispatcher).forward(request, response);
        verify(request, never()).getRequestDispatcher(ERROR_PAGE_PATH);
    }

    @Test
    void executeThrows() throws ServletException, IOException {
        filmInfoPageCommand.execute(request, response);

        verify(request, never()).getRequestDispatcher(FILM_INFO_PAGE_PATH);
        verify(request, times(1)).getRequestDispatcher(ERROR_PAGE_PATH);
        verify(dispatcher).forward(request, response);
    }
}