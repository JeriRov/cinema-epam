package epam.project.finalproject.actions;

import epam.project.finalproject.actions.commands.MainPageCommand;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND;
import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND_VIEW_MAIN_PAGE;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

class CommandFactoryTest {
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final CommandFactory factory = Mockito.spy(CommandFactory.class);

    @Test
    void defineCommand() {
        Assertions.assertInstanceOf(BaseCommand.class, factory.defineCommand(request));
        when(request.getParameter(COMMAND)).thenReturn(null);
        Assertions.assertInstanceOf(MainPageCommand.class, factory.defineCommand(request));
        when(request.getParameter(COMMAND)).thenReturn("");
        Assertions.assertInstanceOf(MainPageCommand.class, factory.defineCommand(request));
        when(request.getParameter(COMMAND)).thenReturn(COMMAND_VIEW_MAIN_PAGE);
        Assertions.assertInstanceOf(MainPageCommand.class, factory.defineCommand(request));
    }
}