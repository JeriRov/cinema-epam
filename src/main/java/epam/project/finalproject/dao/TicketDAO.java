package epam.project.finalproject.dao;

import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.exceptions.DAOException;

import java.util.List;

public interface TicketDAO extends DAO<Ticket> {
    /**
     * Get List of all User's tickets
     * @param userId id of User
     * @return list of tickets
     */
    List<Ticket> findAllByUserId(int userId, int start, int size) throws DAOException;

    int countTotalRowByUserId(int userId) throws DAOException;
}
