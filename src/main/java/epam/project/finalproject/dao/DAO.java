package epam.project.finalproject.dao;

import epam.project.finalproject.entities.BaseEntity;
import epam.project.finalproject.exceptions.DAOException;

import java.util.List;

public interface DAO<T extends BaseEntity> {
    /**
     * Insert T element to the DataBase
     *
     * @param element element to insert
     * @return true - inserted successfully
     * false - did not insert
     */
    boolean insert(T element) throws DAOException;

    /**
     * Find element by id
     *
     * @param id id of the element to find
     * @return element
     */
    T findById(int id) throws DAOException;

    /**
     * Get all T elements from Database in List
     *
     * @return List of received elements
     */
    List<T> findAll() throws DAOException;

    /**
     * Replace element in Database with one another
     *
     * @param element changed the Database element but with the same id
     * @return the Database element (old version)
     */
    T update(T element) throws DAOException;

    /**
     * Delete the element in Database
     *
     * @param element element to delete
     * @return <code>true</code> if removed successfully;
     * <code>false</code> if element wasn't removed: couldn't find the element in Database or got error.
     */
    boolean delete(T element) throws DAOException;
}
