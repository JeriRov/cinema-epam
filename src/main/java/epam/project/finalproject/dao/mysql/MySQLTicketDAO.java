package epam.project.finalproject.dao.mysql;

import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.entities.Session;
import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.DAOException;
import epam.project.finalproject.utilities.LoggerManager;
import org.slf4j.Logger;
import epam.project.finalproject.dao.BaseDAO;
import epam.project.finalproject.dao.TicketDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MySQLTicketDAO extends BaseDAO implements TicketDAO {
    private static final Logger logger = LoggerManager.getLogger(MySQLTicketDAO.class);
    private static final String INSERT = "INSERT INTO tickets VALUES (ticket_id, ?,?,?,?)";
    private static final String SELECT_BY_USER_ID = "SELECT * FROM tickets WHERE user_id=?";
    private static final String SELECT_BY_ID = "SELECT * FROM tickets t WHERE ticket_id=?";
    private static final String COUNT_TOTAL_ROWS = "SELECT COUNT(*) FROM tickets WHERE user_id=?";
    private static final String ORDER_TICKET = " ORDER BY ticket_id";
    private static final String DESCENDING = " DESC";
    private static final String LIMIT = " LIMIT ?, ?";

    @Override
    public boolean insert(Ticket ticket) throws DAOException {
        try (PreparedStatement statement = getConnection().prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            setTicketToStatement(ticket, statement);
            ticketInsertTransaction(ticket, statement);
        } catch (SQLException e) {
            logger.error("Couldn't insert Ticket to DataBase");
            throw new DAOException("Couldn't insert Ticket to DataBase");
        }
        return true;
    }

    /**
     * Transaction method for preventing Ticket writing to Database without writing its to reserved_seats
     *
     * @param ticket    Ticket item
     * @param statement PreparedStatement
     */
    private void ticketInsertTransaction(Ticket ticket, PreparedStatement statement) throws SQLException, DAOException {
        getConnection().setAutoCommit(false);
        statement.executeUpdate();
        final boolean freeSeatsAmount = getSessionDAO().decrementFreeSeatsAmount(ticket.getSession().getId());
        if (!freeSeatsAmount) {
            getConnection().rollback();
            getConnection().setAutoCommit(true);
            logger.debug("rollback and setAutoCommit(true)");
            throw new DAOException("Ticket and reserved seat were not inserted, cause there is no free seats");
        }
        final boolean insertReservedSeats = getSeatDAO().reserveSeatBySession(ticket.getSeat(), ticket.getSession());
        if (insertReservedSeats) {
            getConnection().commit();
        } else {
            getConnection().rollback();
            getConnection().setAutoCommit(true);
            logger.debug("rollback and setAutoCommit(true), Ticket and reserved seat were not inserted");
            throw new DAOException("Ticket and reserved seat were not inserted");
        }
        getConnection().setAutoCommit(true);
    }

    private void setTicketToStatement(Ticket ticket, PreparedStatement statement) throws SQLException {
        try {
            statement.setInt(1, ticket.getSession().getId());
            statement.setInt(2, ticket.getUser().getId());
            statement.setInt(3, ticket.getSeat().getId());
            statement.setBigDecimal(4, ticket.getTicketPrice());
        } catch (SQLException e) {
            throw new SQLException(e);
        }
    }

    @Override
    public Ticket findById(int id) throws DAOException {
        Ticket ticket = null;
        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_ID)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ticket = getTicketFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Couldn't find ticket by id in Database", e);
            throw new DAOException("Couldn't find ticket by id in Database");
        }
        return ticket;
    }

    @Override
    public List<Ticket> findAll() throws DAOException {
        return new ArrayList<>();
    }


    @Override
    public Ticket update(Ticket element) throws DAOException {
        return null;
    }

    @Override
    public boolean delete(Ticket element) throws DAOException {
        return false;
    }

    private Ticket getTicketFromResultSet(ResultSet rs) throws DAOException {
        Ticket ticket;
        try {
            User user = getUserDAO().findById(rs.getInt("user_id"));
            Session session = getSessionDAO().findById(rs.getInt("session_id"));
            Seat seat = getSeatDAO().findById(rs.getInt("seat_id"));
            ticket = new Ticket(
                    rs.getInt("ticket_id"),
                    session, user, seat,
                    rs.getBigDecimal("ticket_price")
            );
        } catch (SQLException e) {
            logger.error("Couldn't get ticket from ResultSet", e);
            throw new DAOException("Couldn't get ticket from ResultSet");
        }
        return ticket;
    }

    private MySQLSeatDAO getSeatDAO() {
        final MySQLSeatDAO mySQLSeatDAO = new MySQLSeatDAO();
        mySQLSeatDAO.setConnection(getConnection());
        return mySQLSeatDAO;
    }

    private MySQLSessionDAO getSessionDAO() {
        final MySQLSessionDAO mySQLSessionDAO = new MySQLSessionDAO();
        mySQLSessionDAO.setConnection(getConnection());
        return mySQLSessionDAO;
    }

    private MySQLUserDAO getUserDAO() {
        final MySQLUserDAO mySQLUserDAO = new MySQLUserDAO();
        mySQLUserDAO.setConnection(getConnection());
        return mySQLUserDAO;
    }

    @Override
    public List<Ticket> findAllByUserId(int userId, int start, int size) throws DAOException {
        List<Ticket> ticketList = new ArrayList<>();
        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_USER_ID + ORDER_TICKET + DESCENDING + LIMIT)) {
            statement.setInt(1, userId);
            statement.setInt(2, start - 1);
            statement.setInt(3, size);
            ResultSet resultSet = statement.executeQuery();
            logger.debug("userId = {}, start={}, size = {}", userId, start, size);
            logger.debug("Statement: {}", statement);
            while (resultSet.next()) {
                final Ticket ticket = getTicketFromResultSet(resultSet);
                ticketList.add(ticket);
            }
        } catch (SQLException e) {
            logger.error("Couldn't get paginated user ticket list from Database", e);
            throw new DAOException("Couldn't get paginated user ticket list from Database");
        }
        return ticketList;
    }

    @Override
    public int countTotalRowByUserId(int userId) throws DAOException {
        int amount = 0;
        try (PreparedStatement statement = getConnection().prepareStatement(COUNT_TOTAL_ROWS)) {
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                amount = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            logger.error("Couldn't count total row amount of tickets from Database", e);
            throw new DAOException("Couldn't count total row amount of tickets from Database");
        }
        return amount;
    }
}
