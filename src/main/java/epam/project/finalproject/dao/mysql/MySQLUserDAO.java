package epam.project.finalproject.dao.mysql;

import epam.project.finalproject.dao.BaseDAO;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.AuthException;
import epam.project.finalproject.exceptions.DAOException;
import epam.project.finalproject.exceptions.RegisterException;
import epam.project.finalproject.utilities.LoggerManager;
import org.slf4j.Logger;
import epam.project.finalproject.dao.UserDAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class MySQLUserDAO extends BaseDAO implements UserDAO {
    private static final Logger logger = LoggerManager.getLogger(MySQLUserDAO.class);
    private static final String GET_MAX_ID = "SELECT MAX(user_id) FROM users";
    private final String SELECT = "SELECT * FROM users s JOIN roles r on s.role_id = r.role_id WHERE s.email=?";
    private final String SELECT_BY_ID = "SELECT * FROM users s JOIN roles r on s.role_id = r.role_id WHERE s.user_id=?";
    private final String INSERT = "INSERT INTO users(user_id, first_name, second_name, email, password, phone_number, notification, salt, verification_code, enabled) VALUES(user_id,?,?,?,?,?,?,?,?,?)";
    private final String UPDATE = "UPDATE users SET first_name=?, second_name=?, email=?, password=?, phone_number=?, notification=?, salt=?, verification_code=?, enabled=? WHERE user_id=?";
    private final String SELECT_PASS_AND_SALT = "SELECT password, salt FROM users WHERE email=?";
    private final String SELECT_BY_VERIFY_CODE = "SELECT * FROM users WHERE verification_code=?";

    @Override
    public boolean insert(User user) throws DAOException {
        try (PreparedStatement statement = getConnection().prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
            setUserToStatement(user, statement);
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Couldn't add user to Database", e);
            if (e.getMessage().contains("Duplicate entry '" + user.getEmail() + "'"))
                throw new RegisterException("There is a user with such an email already");
            else
                throw new DAOException("Couldn't add user");
        }
        return true;
    }

    private void setUserToStatement(User user, PreparedStatement statement) throws SQLException {
        try {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());
            statement.setString(5, user.getPhoneNumber());
            statement.setBoolean(6, user.getNotification());
            statement.setString(7, user.getSalt());
            statement.setString(8, user.getVerificationCode());
            statement.setBoolean(9, user.isEnabled());
        } catch (SQLException e) {
            throw new SQLException("Couldn't set user to PreparedStatement");
        }
    }

    @Override
    public User findById(int id) throws DAOException {
        User user = null;
        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_ID)) {
            statement.setInt(1, id);
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = getUserFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException("Couldn't find user by id", e);
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        return Collections.emptyList();
    }

    @Override
    public User update(User user) throws DAOException {
        try (PreparedStatement statement = getConnection().prepareStatement(UPDATE)) {
            setUserToStatement(user, statement);
            statement.setInt(10, user.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            logger.error("Couldn't update user with user_id={}", user.getId(), e);
        }
        return user;
    }

    @Override
    public boolean delete(User element) throws DAOException {
        return false;
    }

    @Override
    public User getUserByLogin(String login) throws AuthException, DAOException {
        User user = null;
        try (PreparedStatement statement = getConnection().prepareStatement(SELECT)) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = getUserFromResultSet(resultSet);
            }
            if (user == null) throw new AuthException("Couldn't find user with these login and password");
        } catch (SQLException e) {
            throw new DAOException("Couldn't get user from Database", e);
        }
        return user;
    }

    @Override
    public int getMaxId() throws DAOException {
        int maxId = 0;
        try (Statement statement = getConnection().createStatement()) {
            final ResultSet resultSet = statement.executeQuery(GET_MAX_ID);
            while (resultSet.next())
                maxId = resultSet.getInt(1);
            if (maxId == 0) throw new DAOException("Received maxId = 0");
        } catch (SQLException e) {
            throw new DAOException("Couldn't get id from Database", e);
        }
        return maxId;
    }

    @Override
    public Map<String, String> getSaltAndPassByLogin(String login) throws AuthException, DAOException {
        Map<String, String> map = new HashMap<>();
        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_PASS_AND_SALT)) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                final String salt = resultSet.getString("salt");
                map.put("salt", salt);
                final String password = resultSet.getString("password");
                map.put("password", password);
            }
            if (map.isEmpty()) throw new AuthException("Couldn't find user with this login");
        } catch (SQLException e) {
            throw new DAOException("Couldn't get salt and password from Database", e);
        }
        return map;
    }

    @Override
    public User findByVerificationCode(String verificationCode) throws AuthException, DAOException {
        User user = null;
        try (PreparedStatement statement = getConnection().prepareStatement(SELECT_BY_VERIFY_CODE)) {
            statement.setString(1, verificationCode);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = getUserFromResultSet(resultSet);
            }
            if (user == null) throw new AuthException("Couldn't find user with these verification code");
        } catch (SQLException e) {
            throw new DAOException("Couldn't get user from Database", e);
        }
        return user;
    }

    private User getUserFromResultSet(ResultSet rs) throws DAOException {
        User user;
        try {
            user = new User(
                    rs.getInt("user_id"),
                    rs.getString("first_name"),
                    rs.getString("second_name"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getBoolean("notification"),
                    rs.getString("salt"),
                    rs.getString("verification_code"),
                    rs.getBoolean("enabled")
            );
            final String phoneNumber = rs.getString("phone_number");
            if (phoneNumber != null)
                user.setPhoneNumber(phoneNumber);

            final User.Role role = getUserRole(rs);
            user.setUserRole(role);
        } catch (SQLException e) {
            throw new DAOException("Couldn't get user from ResultSet", e);
        }
        return user;
    }

    private User.Role getUserRole(ResultSet rs) {
        User.Role role = User.Role.GUEST;
        try {
            String roleName = rs.getString("role_name");
            role = User.Role.getUserRoleFromString(roleName);
        } catch (IllegalArgumentException e) {
            logger.error("There is no such User Role", e);
        } catch (SQLException e) {
            logger.error("Couldn't get user's role from ResultSet, so set it GUEST", e);
        }
        return role;
    }
}
