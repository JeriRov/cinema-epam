package epam.project.finalproject.dao;

import java.sql.Connection;

/**
 * Base of DAO class stored the connection
 */
public abstract class BaseDAO {
    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    protected BaseDAO() {}

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
