package epam.project.finalproject.dao.factories;

import epam.project.finalproject.dao.*;

public interface DAOFactory extends AutoCloseable {
    /**
     * Get Film DAO
     *
     * @return Film DAO
     */
    FilmDAO getFilmDAO();

    /**
     * Get Genre DAO
     *
     * @return Genre DAO
     */
    GenreDAO getGenreDAO();

    /**
     * Get Seat DAO
     *
     * @return Seat DAO
     */
    SeatDAO getSeatDao();

    /**
     * Get Session DAO
     *
     * @return Session DAO
     */
    SessionDAO getSessionDao();

    /**
     * Get Ticket DAO
     *
     * @return Ticket DAO
     */
    TicketDAO getTicketDao();

    /**
     * Get User DAO
     *
     * @return User DAO
     */
    UserDAO getUserDao();


}
