package epam.project.finalproject.dao.factories;

/**
 * Class delivering appropriate DaoFactory
 */
public class DAOFactoryDeliver {

    private DAOFactoryDeliver() {
    }

    /**
     * Holder for stream-safety singleton
     */
    private static final class FactoryDeliverHolder {
        private static final DAOFactoryDeliver factoryDeliver = new DAOFactoryDeliver();
    }

    /**
     * Get DaoFactoryDeliver
     * @return DaoFactoryDeliver
     */
    public static DAOFactoryDeliver getInstance() {
        return FactoryDeliverHolder.factoryDeliver;
    }

    /**
     * Call general specified factory
     * @return MySQLFactory
     */
    public DAOFactory getFactory() {
        return createMySQLFactory();
    }

    public MySQLFactory createMySQLFactory() {
        return new MySQLFactory();
    }
}
