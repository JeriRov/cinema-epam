package epam.project.finalproject.dao;

import epam.project.finalproject.entities.Film;
import epam.project.finalproject.exceptions.DAOException;

public interface FilmDAO extends DAO<Film>, PaginatableDAO<Film> {
    /**
     * Delete film from DB
     *
     * @param filmId film id
     * @return true if film was deleted and false if not
     */
    boolean delete(final int filmId) throws DAOException;
}
