package epam.project.finalproject.dao;

import epam.project.finalproject.entities.BaseEntity;
import epam.project.finalproject.exceptions.DAOException;

import java.util.List;

public interface PaginatableDAO<T extends BaseEntity> {
    /**
     * Get T elements from Database in paginated List
     *
     * @return List of received elements
     */
    List<T> findAll(int start, int size) throws DAOException;

    /**
     * Get total amount of rows
     * @return total amount of rows
     */
    int countTotalRow() throws DAOException;
}
