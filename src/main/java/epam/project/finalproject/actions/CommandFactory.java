package epam.project.finalproject.actions;

import epam.project.finalproject.actions.commands.*;
import epam.project.finalproject.actions.commands.films.*;
import epam.project.finalproject.actions.commands.mail.SendTicketViaMailCommand;
import epam.project.finalproject.actions.commands.sessions.*;
import epam.project.finalproject.actions.commands.signing.*;
import epam.project.finalproject.actions.commands.tickets.BuyTicketCommand;
import epam.project.finalproject.actions.commands.tickets.BuyTicketPageCommand;
import epam.project.finalproject.actions.commands.tickets.DownloadPDFTicketCommand;
import epam.project.finalproject.actions.commands.verify.MailVerifyCommand;
import epam.project.finalproject.actions.commands.verify.VerifyCommand;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND;

/**
 * Factory class from Factory method pattern
 * Define received command and create appropriate object which implementing BaseCommand interface
 */
public class CommandFactory {
    private static final Logger logger = LoggerManager.getLogger(CommandFactory.class);

    /**
     * Map contains String commands and BaseCommand objects
     */
    private Map<String, BaseCommand> commandMap;

    private void init() {
        commandMap = new HashMap<>();

        // general pages
        commandMap.put(CommandConstants.COMMAND_VIEW_MAIN_PAGE, new MainPageCommand());
        commandMap.put(CommandConstants.COMMAND_VIEW_SCHEDULE_PAGE, new ScheduleCommand());
        commandMap.put(CommandConstants.COMMAND_VIEW_ERROR_PAGE, new ErrorPageCommand());

        // login, register, logout
        commandMap.put(CommandConstants.COMMAND_VIEW_LOGIN_PAGE, new LoginPageCommand());
        commandMap.put(CommandConstants.COMMAND_LOGIN, new LoginCommand());
        commandMap.put(CommandConstants.COMMAND_VIEW_REGISTER_PAGE, new RegisterPageCommand());
        commandMap.put(CommandConstants.COMMAND_REGISTER, new RegisterCommand());
        commandMap.put(CommandConstants.COMMAND_LOGOUT, new LogoutCommand());

        // profile
        commandMap.put(CommandConstants.COMMAND_VIEW_PROFILE_PAGE, new ProfilePageCommand());

        // films
        commandMap.put(CommandConstants.COMMAND_VIEW_ADD_FILM_PAGE, new AddFilmPageCommand());
        commandMap.put(CommandConstants.COMMAND_ADD_FILM, new AddFilmCommand());
        commandMap.put(CommandConstants.COMMAND_VIEW_FILMS_SETTING_PAGE, new FilmsSettingPageCommand());
        commandMap.put(CommandConstants.COMMAND_VIEW_FILM_PAGE_PAGE, new FilmInfoPageCommand());
        commandMap.put(CommandConstants.COMMAND_DELETE_FILM, new DeleteFilmCommand());

        // sessions
        commandMap.put(CommandConstants.COMMAND_VIEW_ADD_SESSION_PAGE, new AddSessionPageCommand());
        commandMap.put(CommandConstants.COMMAND_ADD_SESSION, new AddSessionCommand());
        commandMap.put(CommandConstants.COMMAND_VIEW_SESSIONS_SETTING_PAGE, new SessionsSettingPageCommand());
        commandMap.put(CommandConstants.COMMAND_VIEW_SESSION_PAGE, new SessionInfoPageCommand());

        //ticket
        commandMap.put(CommandConstants.COMMAND_VIEW_BUY_TICKET_PAGE, new BuyTicketPageCommand());
        commandMap.put(CommandConstants.COMMAND_BUY_TICKET, new BuyTicketCommand());
        commandMap.put(CommandConstants.COMMAND_VIEW_SUCCESS_PAY_PAGE, new SuccessPageCommand());
        commandMap.put(CommandConstants.COMMAND_DELETE_SESSION, new DeleteSessionCommand());

        commandMap.put(CommandConstants.COMMAND_SEND_TICKET_VIA_MAIL, new SendTicketViaMailCommand());
        commandMap.put(CommandConstants.COMMAND_DOWNLOAD_PDF_TICKET, new DownloadPDFTicketCommand());

        //verify
        commandMap.put(CommandConstants.COMMAND_MAIL_VERIFICATION, new MailVerifyCommand());
        commandMap.put(CommandConstants.COMMAND_VIEW_VERIFICATION, new VerifyCommand());
    }

    /**
     * Method defining received String command and return appropriate BaseCommand object
     *
     * @param request HttpServletRequest
     * @return appropriate BaseCommand object by received String command
     */
    public BaseCommand defineCommand(HttpServletRequest request) {
        if (commandMap == null || commandMap.isEmpty()) {
            init();
        }

        String command = request.getParameter(COMMAND);
        logger.info("CommandFactory received command: {}", command);

        if (command == null || command.isEmpty())
            return commandMap.get(CommandConstants.COMMAND_VIEW_MAIN_PAGE);
        return commandMap.get(command);
    }
}
