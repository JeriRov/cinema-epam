package epam.project.finalproject.actions.commands.verify;

import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.services.VerifyService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.VerifyServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

public class VerifyCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(VerifyCommand.class);
    private static final String CLASS_NAME = VerifyCommand.class.getName();
    private final VerifyService verifyService;

    public VerifyCommand() {
        verifyService = new VerifyServiceImpl();
    }

    public VerifyCommand(VerifyService verifyService) {
        this.verifyService = verifyService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            String code = request.getParameter(VERIFY_CODE);
            String text;
            if (verifyService.verify(code)) {
                text = GOOD_VERIFY_MESSAGE;
            } else {
                text = BAD_VERIFY_MESSAGE;
            }
            request.setAttribute(VERIFY_SUCCESS_PAGE_TEXT, text);
            request.getRequestDispatcher(JspPagePathConstants.VERIFICATION_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
