package epam.project.finalproject.actions.commands;

import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.jsp.jstl.core.Config;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Forward to SUCCESS_PAID_PAGE_PATH when payment is successful
 */
public class SuccessPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(SuccessPageCommand.class);
    private static final String CLASS_NAME = SuccessPageCommand.class.getName();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            logger.debug("Forward to page of successful payment");
            final Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
            ResourceBundle bundle = ResourceBundle.getBundle("i18n", locale);
            request.setAttribute("text", bundle.getString("successPay.text"));
            request.getRequestDispatcher(JspPagePathConstants.SUCCESS_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
