package epam.project.finalproject.actions.commands.films;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.FilmServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import java.io.IOException;

/**
 * Admin delete film command
 */
public class DeleteFilmCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(DeleteFilmCommand.class);
    private static final String CLASS_NAME = DeleteFilmCommand.class.getName();
    private final FilmService filmService;

    public DeleteFilmCommand() {
        filmService = new FilmServiceImpl();
    }

    public DeleteFilmCommand(FilmService filmService) {
        this.filmService = filmService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final int filmId = Integer.parseInt(request.getParameter("filmId"));
            filmService.delete(filmId);
            response.sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_FILMS_SETTING_PAGE));
        } catch (ServiceException | IOException e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }

}
