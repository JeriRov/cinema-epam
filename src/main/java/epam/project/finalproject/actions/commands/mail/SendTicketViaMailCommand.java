package epam.project.finalproject.actions.commands.mail;

import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.mail.messages.TicketMailMessage;
import epam.project.finalproject.services.MailService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.TicketServiceImpl;
import epam.project.finalproject.services.impl.UserServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.jsp.jstl.core.Config;
import org.slf4j.Logger;

import java.util.Locale;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * Send ticket via email command
 */
public class SendTicketViaMailCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(SendTicketViaMailCommand.class);
    private static final String CLASS_NAME = SendTicketViaMailCommand.class.getName();
    private final UserService userService;
    private final TicketService ticketService;
    private final SendMailCommand sendMailCommand;
    public SendTicketViaMailCommand() {
        ticketService = new TicketServiceImpl();
        userService = new UserServiceImpl();
        sendMailCommand = new SendMailCommand();
    }

    public SendTicketViaMailCommand(UserService userService, TicketService ticketService, MailService mailService) {
        this.userService = userService;
        this.ticketService = ticketService;
        this.sendMailCommand = new SendMailCommand(mailService);
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            logger.info("Entry to {} execute command", CLASS_NAME);
            final Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
                logger.info("LOCALE: {}", Config.get(request.getSession(), Config.FMT_LOCALE));

            int ticketId = Integer.parseInt(request.getParameter(TICKET_ID));
            final Ticket ticket = ticketService.getById(ticketId);

            final int userId = ticket.getUser().getId();
            logger.debug("userId: {}", userId);
            final User user = userService.getById(userId);

            TicketMailMessage ticketMailMessage = new TicketMailMessage(ticket, locale);

            request.setAttribute(USER_MAIL, user.getEmail());
            request.setAttribute(MAIL_SUBJECT, ticketMailMessage.subject());
            request.setAttribute(MAIL_BODY, ticketMailMessage.body());

            sendMailCommand.execute(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
