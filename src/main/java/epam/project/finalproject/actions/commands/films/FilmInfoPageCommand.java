package epam.project.finalproject.actions.commands.films;

import epam.project.finalproject.entities.Film;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.FilmServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

/**
 * Film info page command
 */
public class FilmInfoPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(FilmInfoPageCommand.class);
    private static final String CLASS_NAME = FilmInfoPageCommand.class.getName();
    private final FilmService filmService;

    public FilmInfoPageCommand() {
        filmService = new FilmServiceImpl();
    }

    public FilmInfoPageCommand(FilmService filmService) {
        this.filmService = filmService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final int filmId = Integer.parseInt(request.getParameter("filmId"));
            final Film film = filmService.getById(filmId);
            request.setAttribute("film", film);
            request.getRequestDispatcher(JspPagePathConstants.FILM_INFO_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }

}
