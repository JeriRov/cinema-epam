package epam.project.finalproject.actions.commands.films;

import epam.project.finalproject.entities.Genre;
import epam.project.finalproject.services.GenreService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.GenreServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import java.util.List;

/**
 * Admin film page command
 */
public class AddFilmPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(AddFilmPageCommand.class);
    private static final String CLASS_NAME = AddFilmPageCommand.class.getName();
    private final GenreService genreService;

    public AddFilmPageCommand() {
        genreService = new GenreServiceImpl();
    }

    public AddFilmPageCommand(GenreService genreService) {
        this.genreService = genreService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final List<Genre> genreList = genreService.getAll();
            request.setAttribute("genreList", genreList);
            logger.debug("Forwarded to admin add film page");
            request.getRequestDispatcher(JspPagePathConstants.ADD_FILM_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
