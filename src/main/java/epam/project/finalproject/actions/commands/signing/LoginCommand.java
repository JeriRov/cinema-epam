package epam.project.finalproject.actions.commands.signing;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.services.CookieService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.impl.CookieServiceImpl;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.UserServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * User login class, check if user exist and verify login and password
 */
public class LoginCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(LoginCommand.class);
    private static final String CLASS_NAME = LoginCommand.class.getName();
    private final UserService userService;
    private final CookieService cookieService;

    public LoginCommand() {
        userService = new UserServiceImpl();
        cookieService = new CookieServiceImpl();
    }

    public LoginCommand(UserService userService, CookieService cookieService) {
        this.userService = userService;
        this.cookieService = cookieService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final String login = request.getParameter(LOGIN);
            final String emailError = userService.validateUserEmail(login);
            if (emailError.isBlank()) {
                final String password = request.getParameter(PASSWORD);
                final String rememberMe = request.getParameter(REMEMBER_ME);
                logger.info("LOL WTF {}", password);
                userService.authenticateUser(login, password);
                final User user = userService.getUserByLogin(login);
                prepareUser(user, request, response, rememberMe);

                final String redirectPage = getRedirectPage(user.getUserRole());
                response.sendRedirect(RedirectManager.getRedirectLocation(redirectPage));
            } else {
                request.setAttribute(emailError, true);
                request.getRequestDispatcher(JspPagePathConstants.LOGIN_PAGE_PATH).forward(request, response);
            }
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }

    /**
     * @param user       User
     * @param request    HttpServletRequest
     * @param response   HttpServletResponse
     * @param rememberMe checkbox value Remember me
     */
    private void prepareUser(User user, HttpServletRequest request, HttpServletResponse response, String rememberMe) {
        HttpSession session = request.getSession(true);
        session.setAttribute(USER_ID, user.getId());
        session.setAttribute(USER_ROLE, user.getUserRole());
        logger.info("User with id: {}, role = {} login", user.getId(), user.getUserRole());
        cookieService.loginCookie(response, user, rememberMe);
    }

    /**
     * If user is USER get profile page, if ADMIN - get main page
     *
     * @param userRole User role
     * @return page to redirect
     */
    private String getRedirectPage(User.Role userRole) {
        if (userRole == User.Role.USER)
            return CommandConstants.COMMAND_VIEW_PROFILE_PAGE;
        return CommandConstants.COMMAND_VIEW_MAIN_PAGE;
    }
}
