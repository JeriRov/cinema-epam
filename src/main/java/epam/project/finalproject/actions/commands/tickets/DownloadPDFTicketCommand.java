package epam.project.finalproject.actions.commands.tickets;

import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.services.TicketPdfService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.TicketPdfServiceImpl;
import epam.project.finalproject.services.impl.TicketServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.jsp.jstl.core.Config;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import java.io.ByteArrayOutputStream;
import java.util.Locale;

import static epam.project.finalproject.utilities.constants.OtherConstants.TICKET_ID;

/**
 * Command to download Ticket in PDF format
 */
public class DownloadPDFTicketCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(DownloadPDFTicketCommand.class);
    private static final String CLASS_NAME = DownloadPDFTicketCommand.class.getName();
    private final TicketService ticketService;
    private final TicketPdfService ticketPdfService;

    public DownloadPDFTicketCommand() {
        ticketService = new TicketServiceImpl();
        ticketPdfService = new TicketPdfServiceImpl();
    }

    public DownloadPDFTicketCommand(TicketService ticketService, TicketPdfService ticketPdfService) {
        this.ticketService = ticketService;
        this.ticketPdfService = ticketPdfService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            int ticketId = Integer.parseInt(request.getParameter(TICKET_ID));
            final Ticket ticket = ticketService.getById(ticketId);
            final Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);
            final ByteArrayOutputStream stream = ticketPdfService.formPDFTicket(ticket, locale);
            ticketPdfService.writePdfToResponse(stream, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
