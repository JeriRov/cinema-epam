package epam.project.finalproject.actions.commands;

import epam.project.finalproject.entities.Film;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.PaginationService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.FilmServiceImpl;
import epam.project.finalproject.services.impl.PaginationServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * Command preparing and forward to Main page
 */
public class MainPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(MainPageCommand.class);
    private static final String CLASS_NAME = MainPageCommand.class.getName();
    private final FilmService filmService;
    private final PaginationService paginationService;

    public MainPageCommand() {
        filmService = new FilmServiceImpl();
        paginationService = new PaginationServiceImpl();
    }

    public MainPageCommand(FilmService filmService, PaginationService paginationService) {
        this.filmService = filmService;
        this.paginationService = paginationService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final Map<String, Integer> paginationMap = paginationService.getPaginationParamsFromRequest(request);
            int page = paginationMap.get(PAGE_NO_PARAM);
            int size = paginationMap.get(PAGE_SIZE_PARAM);

            final List<Film> all = filmService.getAll(page, size);
            final int totalPages = filmService.countTotalPages(size);

            request.setAttribute("totalPages", totalPages);
            request.getSession().setAttribute("filmList", all);
            request.getRequestDispatcher(JspPagePathConstants.MAIN_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }

}
