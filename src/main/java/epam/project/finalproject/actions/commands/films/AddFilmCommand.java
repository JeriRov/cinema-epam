package epam.project.finalproject.actions.commands.films;

import epam.project.finalproject.entities.Film;
import epam.project.finalproject.entities.Genre;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.GenreService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.FilmServiceImpl;
import epam.project.finalproject.services.impl.GenreServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.RequestBuilder;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import java.time.Duration;
import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * Admin adding film command
 */
public class AddFilmCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(AddFilmCommand.class);
    private static final String CLASS_NAME = AddFilmCommand.class.getName();
    private final FilmService filmService;
    private final GenreService genreService;
    private final AddFilmPageCommand addFilmPageCommand;

    public AddFilmCommand() {
        filmService = new FilmServiceImpl();
        genreService = new GenreServiceImpl();
        addFilmPageCommand = new AddFilmPageCommand();
    }

    public AddFilmCommand(FilmService filmService, GenreService genreService) {
        this.filmService = filmService;
        this.genreService = genreService;
        this.addFilmPageCommand = new AddFilmPageCommand(genreService);
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final Map<String, String> filmParamMap = getFilmParamMap(request);
            final String[] genreIds = request.getParameterMap().get(GENRE_IDS_PARAM);
            final List<String> errorList = filmService.getFilmValidErrorList(filmParamMap, genreIds);

            if (errorList.isEmpty()) {
                Film film = getFilm(filmParamMap);
                final List<Genre> genreList = genreService.getGenreListByIdArray(genreIds);
                film.setGenreList(genreList);
                filmService.create(film);
                response.sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_FILMS_SETTING_PAGE));
            } else {
                forwardWithErrors(request, response, errorList);
            }
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }

    /**
     * Forward to add film page with error list
     *
     * @param request   {@link HttpServletRequest}
     * @param response  {@link HttpServletResponse}
     * @param errorList received error List from validation service
     */
    private void forwardWithErrors(HttpServletRequest request, HttpServletResponse response, List<String> errorList) {
        VALID_ERROR_FILM_PARAM_LIST.stream()
                .filter(error -> request.getAttribute(error) != null)
                .forEach(error -> request.setAttribute(error, false));
        errorList.forEach(error -> request.setAttribute(error, true));
        logger.warn("Forwarded with errors errorList {}", errorList);
        addFilmPageCommand.execute(request, response);
    }

    /**
     * Get film from params Map
     *
     * @param filmParamMap film parameters map
     * @return formed Film object
     */
    private Film getFilm(Map<String, String> filmParamMap) {
        final long filmDuration = Long.parseLong(filmParamMap.get(FILM_DURATION_PARAM));
        return new Film(
                filmParamMap.get(FILM_NAME_PARAM),
                filmParamMap.get(FILM_DESCR_PARAM),
                filmParamMap.get(POSTER_URL_PARAM),
                Duration.ofMinutes(filmDuration)
        );
    }

    /**
     * Get Map of needed parameters for film creation
     *
     * @param request {@link HttpServletRequest}
     * @return map of needed parameters for film creation
     */
    private Map<String, String> getFilmParamMap(HttpServletRequest request) {
        return RequestBuilder.buildMap(request, FILM_NAME_PARAM, FILM_DESCR_PARAM, POSTER_URL_PARAM, FILM_DURATION_PARAM);
    }
}
