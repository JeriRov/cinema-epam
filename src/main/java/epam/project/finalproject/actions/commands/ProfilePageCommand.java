package epam.project.finalproject.actions.commands;

import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.services.PaginationService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.PaginationServiceImpl;
import epam.project.finalproject.services.impl.TicketServiceImpl;
import epam.project.finalproject.services.impl.UserServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * Command to show User Profile page
 */
public class ProfilePageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(ProfilePageCommand.class);
    private static final String CLASS_NAME = ProfilePageCommand.class.getName();
    private final TicketService ticketService;
    private final PaginationService paginationService;
    private final UserService userService;

    public ProfilePageCommand() {
        ticketService = new TicketServiceImpl();
        paginationService = new PaginationServiceImpl();
        userService = new UserServiceImpl();
    }

    public ProfilePageCommand(TicketService ticketService, PaginationService paginationService, UserService userService) {
        this.ticketService = ticketService;
        this.paginationService = paginationService;
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final Map<String, Integer> paginationMap = paginationService.getPaginationParamsFromRequest(request);
            int page = paginationMap.get(PAGE_NO_PARAM);
            int size = paginationMap.get(PAGE_SIZE_PARAM);
            final int userId = Integer.parseInt(request.getSession().getAttribute(USER_ID).toString());
            final User user = userService.getById(userId);
            final List<Ticket> ticketList = ticketService.getAllByUserId(userId, page, size);
            final int totalPages = ticketService.countTotalPagesByUserId(userId, size);

            request.setAttribute("totalPages", totalPages);
            request.setAttribute("ticketList", ticketList);
            request.setAttribute("isEnabled", user.isEnabled());
            request.getRequestDispatcher(JspPagePathConstants.USER_PROFILE_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
