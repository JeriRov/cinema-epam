package epam.project.finalproject.actions.commands.verify;

import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.actions.commands.signing.LoginCommand;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.VerifyService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.UserServiceImpl;
import epam.project.finalproject.services.impl.VerifyServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.jsp.jstl.core.Config;
import net.bytebuddy.utility.RandomString;
import org.slf4j.Logger;

import java.util.Locale;
import java.util.ResourceBundle;

import static epam.project.finalproject.utilities.constants.OtherConstants.USER_ID;

public class MailVerifyCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(MailVerifyCommand.class);
    private static final String CLASS_NAME = LoginCommand.class.getName();

    private final UserService userService;
    private final VerifyService verifyService;

    public MailVerifyCommand() {
        userService = new UserServiceImpl();
        verifyService = new VerifyServiceImpl();
    }

    public MailVerifyCommand(UserService userService, VerifyService verifyService) {
        this.userService = userService;
        this.verifyService = verifyService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            HttpSession session = request.getSession();
            int userId = Integer.parseInt(session.getAttribute(USER_ID).toString());
            User user = userService.getById(userId);

            final Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);

            ResourceBundle bundle = ResourceBundle.getBundle("i18n", locale);
            String text = bundle.getString("mail.verify.success");
            request.setAttribute("text", text);
            final String randomCode = RandomString.make(64);

            verifyService.saveVerificationCodeToUser(user, randomCode);
            verifyService.sendMail(user, verifyService.getVerifyURL(request, randomCode), locale);

            logger.info("{} forward with text: {}", CLASS_NAME, text);
            request.getRequestDispatcher(JspPagePathConstants.SUCCESS_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }


}