package epam.project.finalproject.actions.commands.tickets;

import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.services.SeatService;
import epam.project.finalproject.services.SessionService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.impl.*;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.entities.Session;
import epam.project.finalproject.entities.User;

import java.math.BigDecimal;
import java.util.List;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * Command show page of chosen tickets
 */
public class BuyTicketPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(BuyTicketPageCommand.class);
    private static final String CLASS_NAME = BuyTicketPageCommand.class.getName();
    private final TicketService ticketService;
    private final SessionService sessionService;
    private final UserService userService;
    private final SeatService seatService;

    public BuyTicketPageCommand() {
        ticketService = new TicketServiceImpl();
        sessionService = new SessionServiceImpl();
        userService = new UserServiceImpl();
        seatService = new SeatServiceImpl();
    }
    public BuyTicketPageCommand(TicketService ticketService, SessionService sessionService, UserService userService, SeatService seatService) {
        this.ticketService = ticketService;
        this.sessionService = sessionService;
        this.userService = userService;
        this.seatService = seatService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final int sessionId = Integer.parseInt(request.getParameter(SESSION_ID));
            final int userId = Integer.parseInt(request.getSession().getAttribute(USER_ID).toString());
            final String[] seatIds = request.getParameterMap().get(SEAT_IDS);

            final Session session = sessionService.getById(sessionId);
            final User user = userService.getById(userId);
            final List<Seat> seatList = seatService.getSeatListByIdArray(seatIds);
            final List<Ticket> ticketList = ticketService.formTicketList(session, seatList, user);
            final BigDecimal totalCost = ticketService.countTotalCostOfTicketList(ticketList);

            request.getSession().setAttribute("ticketList", ticketList);
            request.setAttribute("session", session);
            request.setAttribute("totalCost", totalCost);


            request.getRequestDispatcher(JspPagePathConstants.PAYING_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
