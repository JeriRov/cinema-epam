package epam.project.finalproject.actions.commands.signing;

import epam.project.finalproject.services.CookieService;
import epam.project.finalproject.services.impl.CookieServiceImpl;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.entities.User;

import static epam.project.finalproject.utilities.constants.OtherConstants.USER_ID;
import static epam.project.finalproject.utilities.constants.OtherConstants.USER_ROLE;

/**
 * Command to log out User, end sessions and clean cookie
 */
public class LogoutCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(LogoutCommand.class);
    private static final String CLASS_NAME = LogoutCommand.class.getName();
    private final CookieService cookieService;

    public LogoutCommand() {
        cookieService = new CookieServiceImpl();
    }

    public LogoutCommand(CookieService cookieService) {
        this.cookieService = cookieService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final HttpSession session = request.getSession();
            logUserLogout(session);
            session.invalidate();
            cookieService.logoutCookie(request, response);
            response.sendRedirect(CommandConstants.COMMAND_MAIN_SERVLET);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }

    private void logUserLogout(HttpSession session) {
        final int userId = Integer.parseInt(session.getAttribute(USER_ID).toString());
        final User.Role userRole = User.Role.valueOf(session.getAttribute(USER_ROLE).toString());
        logger.info("User with id = {}, role = {} logout", userId, userRole);
    }
}
