package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.entities.Session;
import epam.project.finalproject.services.SeatService;
import epam.project.finalproject.services.SessionService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.SeatServiceImpl;
import epam.project.finalproject.services.impl.SessionServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import java.util.List;

import static epam.project.finalproject.utilities.constants.OtherConstants.SESSION_ID;

/**
 * Command to show Sessions page
 */
public class SessionInfoPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(SessionInfoPageCommand.class);
    private static final String CLASS_NAME = SessionInfoPageCommand.class.getName();
    private final SessionService sessionService;
    private final SeatService seatService;

    public SessionInfoPageCommand() {
        sessionService = new SessionServiceImpl();
        seatService = new SeatServiceImpl();
    }

    public SessionInfoPageCommand(SessionService sessionService, SeatService seatService) {
        this.sessionService = sessionService;
        this.seatService = seatService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final int sessionId = Integer.parseInt(request.getParameter(SESSION_ID));
            Session session = sessionService.getById(sessionId);
            List<Seat> freeSeatList = seatService.getFreeSeatsBySessionId(sessionId);
            List<Seat> allSeatList = seatService.getAll();

            request.setAttribute("session", session);
            request.setAttribute("allSeatList", allSeatList);
            request.setAttribute("freeSeatList", freeSeatList);

            request.getRequestDispatcher(JspPagePathConstants.SESSIONS_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
