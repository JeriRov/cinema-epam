package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.services.PaginationService;
import epam.project.finalproject.services.SessionService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.PaginationServiceImpl;
import epam.project.finalproject.services.impl.SessionServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.entities.Session;

import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.OtherConstants.PAGE_NO_PARAM;
import static epam.project.finalproject.utilities.constants.OtherConstants.PAGE_SIZE_PARAM;

/**
 * Command to show Admin Sessions setting page
 */
public class SessionsSettingPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(SessionsSettingPageCommand.class);
    private static final String CLASS_NAME = SessionsSettingPageCommand.class.getName();
    private final SessionService sessionService;
    private final PaginationService paginationService;

    public SessionsSettingPageCommand() {
        sessionService = new SessionServiceImpl();
        paginationService = new PaginationServiceImpl();
    }

    public SessionsSettingPageCommand(SessionService sessionService, PaginationService paginationService) {
        this.sessionService = sessionService;
        this.paginationService = paginationService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final Map<String, Integer> paginationMap = paginationService.getPaginationParamsFromRequest(request);
            int page = paginationMap.get(PAGE_NO_PARAM);
            int size = paginationMap.get(PAGE_SIZE_PARAM);
            final int totalPages = sessionService.countTotalPages(size);

            request.setAttribute("totalPages", totalPages);
            final List<Session> sessionList = sessionService.getAll(page, size);
            request.setAttribute("sessionList", sessionList);
            request.getRequestDispatcher(JspPagePathConstants.SESSIONS_SETTING_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
