package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.entities.Film;
import epam.project.finalproject.entities.Session;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.SessionService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.FilmServiceImpl;
import epam.project.finalproject.services.impl.SessionServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.RequestBuilder;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * Admin add Session Command
 */
public class AddSessionCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(AddSessionCommand.class);
    private static final String CLASS_NAME = AddSessionCommand.class.getName();
    private final SessionService sessionService;
    private final FilmService filmService;
    private final AddSessionPageCommand addSessionPageCommand;

    public AddSessionCommand() {
        sessionService = new SessionServiceImpl();
        filmService = new FilmServiceImpl();
        addSessionPageCommand = new AddSessionPageCommand();
    }
    public AddSessionCommand(SessionService sessionService, FilmService filmService) {
        this.sessionService = sessionService;
        this.filmService = filmService;
        addSessionPageCommand = new AddSessionPageCommand(filmService);
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final Map<String, String> sessionParamMap = getSessionParamMap(request);
            final List<String> errorList = sessionService.getSessionFieldsValidErrorList(sessionParamMap);
            final Session session = getSession(sessionParamMap);
            errorList.addAll(sessionService.sessionValid(session));
            if (errorList.isEmpty()) {
                sessionService.create(session);
                response.sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_SESSIONS_SETTING_PAGE));
            } else {
                forwardWithErrors(request, response, errorList);
            }
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }

    /**
     * Forward to add session page with error list
     *
     * @param request   {@link HttpServletRequest}
     * @param response  {@link HttpServletResponse}
     * @param errorList received error List from validation service
     */
    private void forwardWithErrors(HttpServletRequest request, HttpServletResponse response, List<String> errorList) {
        VALID_ERROR_SESSION_PARAM_LIST.stream()
                .filter(error -> request.getAttribute(error) != null)
                .forEach(error -> request.setAttribute(error, false));
        errorList.forEach(error -> request.setAttribute(error, true));
        addSessionPageCommand.execute(request, response);
    }

    /**
     * Get session from params Map
     *
     * @param sessionParamMap session parameters map
     * @return formed Session object
     */
    private Session getSession(Map<String, String> sessionParamMap) throws ServiceException {
        final int filmId = Integer.parseInt(sessionParamMap.get(FILM_ID_PARAM));
        final Film film = filmService.getById(filmId);
        final Session session = new Session();
        session.setTime(LocalTime.parse(sessionParamMap.get(SESSION_TIME_PARAM)));
        session.setDate(LocalDate.parse(sessionParamMap.get(SESSION_DATE_PARAM)));
        session.setTicketPrice(new BigDecimal(sessionParamMap.get(SESSION_PRICE_PARAM)));
        session.setFilm(film);
        return session;
    }

    /**
     * Get Map of needed parameters for session creation
     *
     * @param request {@link HttpServletRequest}
     * @return map of needed parameters for session creation
     */
    private Map<String, String> getSessionParamMap(HttpServletRequest request) {
        return RequestBuilder.buildMap(request, FILM_ID_PARAM, SESSION_TIME_PARAM, SESSION_DATE_PARAM, SESSION_PRICE_PARAM);
    }

}
