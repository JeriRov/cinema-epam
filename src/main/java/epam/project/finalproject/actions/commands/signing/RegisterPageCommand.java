package epam.project.finalproject.actions.commands.signing;

import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

/**
 * Command for getting registration page
 */
public class RegisterPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(RegisterPageCommand.class);
    private static final String CLASS_NAME = RegisterPageCommand.class.getName();

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            logger.debug("Forward to register page");
            request.getRequestDispatcher(JspPagePathConstants.REGISTER_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
