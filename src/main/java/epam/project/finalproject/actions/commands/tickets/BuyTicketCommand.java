package epam.project.finalproject.actions.commands.tickets;

import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.services.PaymentService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.impl.*;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.jsp.jstl.core.Config;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.*;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * Command to save tickets
 */
public class BuyTicketCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(BuyTicketCommand.class);
    private static final String CLASS_NAME = BuyTicketCommand.class.getName();
    private final TicketService ticketService;
    private final UserService userService;
    private final PaymentService paymentService;


    public BuyTicketCommand() {
        paymentService = new PaymentServiceImpl();
        ticketService = new TicketServiceImpl();
        userService = new UserServiceImpl();
    }

    public BuyTicketCommand(TicketService ticketService, UserService userService, PaymentService paymentService) {
        this.ticketService = ticketService;
        this.userService = userService;
        this.paymentService = paymentService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final List<Ticket> ticketList = (List<Ticket>) request.getSession().getAttribute(TICKET_LIST);
            logger.debug("Received ticketList = {}", request.getSession().getAttribute(TICKET_LIST));
            final int userId = Integer.parseInt(request.getSession().getAttribute(USER_ID).toString());
            final User user = userService.getById(userId);
            final Locale locale = (Locale) Config.get(request.getSession(), Config.FMT_LOCALE);

            String creditCardNumber = request.getParameter(CREDIT_CARD_NUMBER);
            String creditCardDate = request.getParameter(CREDIT_CARD_DATE);
            final String creditCardCvv = request.getParameter(CREDIT_CARD_CVV);
            final BigDecimal totalCost = new BigDecimal(request.getParameter("totalCost"));

            Map<String, String> card = new HashMap<>();
            card.put(CREDIT_CARD_NUMBER, creditCardNumber);
            card.put(CREDIT_CARD_DATE, creditCardDate);
            card.put(CREDIT_CARD_CVV, creditCardCvv);
            paymentService.validateCard(card);

            creditCardNumber = creditCardNumber.replace(" ", "");
            creditCardDate += "/01";
            card.put(CREDIT_CARD_NUMBER, creditCardNumber);
            card.put(CREDIT_CARD_DATE, creditCardDate);
            card.put(CREDIT_CARD_CVV, creditCardCvv);

            paymentService.makePayment(card, totalCost);
            ticketService.saveAll(ticketList);
            ticketService.sendTicketNotification(ticketList, user, locale);
            response.sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_SUCCESS_PAY_PAGE));
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
