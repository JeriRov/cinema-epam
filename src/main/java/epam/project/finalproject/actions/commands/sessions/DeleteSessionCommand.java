package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.services.SessionService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.SessionServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import static epam.project.finalproject.utilities.constants.OtherConstants.SESSION_ID;

/**
 * Deleting Session (Admin)
 */
public class DeleteSessionCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(DeleteSessionCommand.class);
    private static final String CLASS_NAME = DeleteSessionCommand.class.getName();
    private final SessionService sessionService;

    public DeleteSessionCommand() {
        sessionService = new SessionServiceImpl();
    }

    public DeleteSessionCommand(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final int sessionId = Integer.parseInt(request.getParameter(SESSION_ID));
            sessionService.delete(sessionId);
            response.sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_SESSIONS_SETTING_PAGE));
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
