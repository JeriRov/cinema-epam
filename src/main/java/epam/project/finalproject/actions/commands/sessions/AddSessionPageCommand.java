package epam.project.finalproject.actions.commands.sessions;

import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.entities.Film;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.FilmServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;

import java.util.List;

/**
 * Admin add Session page command
 */
public class AddSessionPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(AddSessionPageCommand.class);
    private static final String CLASS_NAME = AddSessionPageCommand.class.getName();
    private final FilmService filmService;

    public AddSessionPageCommand() {
        filmService = new FilmServiceImpl();
    }

    public AddSessionPageCommand(FilmService filmService) {
        this.filmService = filmService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final List<Film> filmList = filmService.getAll();
            request.setAttribute("filmList", filmList);

            logger.debug("Forward to add session page");
            request.getRequestDispatcher(JspPagePathConstants.ADD_SESSION_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
