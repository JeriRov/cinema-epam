package epam.project.finalproject.actions.commands.mail;

import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.services.MailService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.MailServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.RedirectManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;

import static epam.project.finalproject.utilities.constants.CommandConstants.COMMAND_VIEW_PROFILE_PAGE;
import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * Send mail command
 */
public class SendMailCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(SendMailCommand.class);
    private static final String CLASS_NAME = SendMailCommand.class.getName();
    private final MailService mailService;

    public SendMailCommand() {
        mailService = new MailServiceImpl();
    }

    public SendMailCommand(MailService mailService) {
        this.mailService = mailService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            logger.info("Entry to {} execute command", CLASS_NAME);

            String recipient = request.getAttribute(USER_MAIL).toString();
            String subject = request.getAttribute(MAIL_SUBJECT).toString();
            String body = request.getAttribute(MAIL_BODY).toString();

            logger.debug("Info receiver, recipient: {} subject: {} content: {}", recipient, subject, body);

            mailService.sendEmail(recipient, subject, body);

            response.sendRedirect(RedirectManager.getRedirectLocation(COMMAND_VIEW_PROFILE_PAGE));
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
