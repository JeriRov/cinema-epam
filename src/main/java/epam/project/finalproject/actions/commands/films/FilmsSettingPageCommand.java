package epam.project.finalproject.actions.commands.films;

import epam.project.finalproject.entities.Film;
import epam.project.finalproject.services.FilmService;
import epam.project.finalproject.services.PaginationService;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.FilmServiceImpl;
import epam.project.finalproject.services.impl.PaginationServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;

import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.OtherConstants.PAGE_NO_PARAM;
import static epam.project.finalproject.utilities.constants.OtherConstants.PAGE_SIZE_PARAM;

/**
 * Admin film setting page
 */
public class FilmsSettingPageCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(FilmsSettingPageCommand.class);
    private static final String CLASS_NAME = FilmsSettingPageCommand.class.getName();
    private final FilmService filmService;
    private final PaginationService paginationService;

    public FilmsSettingPageCommand() {
        filmService = new FilmServiceImpl();
        paginationService = new PaginationServiceImpl();
    }

    public FilmsSettingPageCommand(FilmService filmService, PaginationService paginationService) {
        this.filmService = filmService;
        this.paginationService = paginationService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            final Map<String, Integer> paginationMap = paginationService.getPaginationParamsFromRequest(request);
            int page = paginationMap.get(PAGE_NO_PARAM);
            int size = paginationMap.get(PAGE_SIZE_PARAM);
            final int totalPages = filmService.countTotalPages(size);

            final List<Film> all = filmService.getAll(page, size);
            request.setAttribute("totalPages", totalPages);
            request.getSession().setAttribute("filmList", all);
            request.getRequestDispatcher(JspPagePathConstants.FILMS_SETTING_PAGE_PATH).forward(request, response);
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }
}
