package epam.project.finalproject.actions.commands.signing;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.RegisterException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.CookieService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.CaptchaService;
import epam.project.finalproject.services.impl.CookieServiceImpl;
import epam.project.finalproject.services.impl.ErrorService;
import epam.project.finalproject.services.impl.UserServiceImpl;
import epam.project.finalproject.services.impl.CaptchaServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.PassEncryptionManager;
import epam.project.finalproject.utilities.RedirectManager;
import epam.project.finalproject.utilities.RequestBuilder;
import epam.project.finalproject.utilities.constants.CommandConstants;
import epam.project.finalproject.utilities.constants.JspPagePathConstants;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import epam.project.finalproject.actions.BaseCommand;
import epam.project.finalproject.utilities.constants.OtherConstants;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

/**
 * Command for User registration
 */
public class RegisterCommand implements BaseCommand {
    private static final Logger logger = LoggerManager.getLogger(RegisterCommand.class);
    private static final String CLASS_NAME = RegisterCommand.class.getName();
    private final UserService userService;
    private final CookieService cookieService;
    private final CaptchaService captchaService;

    public RegisterCommand() {
        userService = new UserServiceImpl();
        cookieService = new CookieServiceImpl();
        captchaService = new CaptchaServiceImpl();
    }

    public RegisterCommand(UserService userService, CookieService cookieService, CaptchaService captchaService) {
        this.userService = userService;
        this.cookieService = cookieService;
        this.captchaService = captchaService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        logger.debug("Called execute() in {}", CLASS_NAME);
        try {
            captchaService.captchaValidation(request, response);

            final Map<String, String> userParamMap = getUserParamMap(request);
            final List<String> errorList = userService.getUserValidErrorList(userParamMap);
            if (errorList.isEmpty()) {
                final User user = getEncryptedUser(userParamMap);
                saveUser(request, response, user);
                response.sendRedirect(RedirectManager.getRedirectLocation(CommandConstants.COMMAND_VIEW_PROFILE_PAGE));
            } else {
                forwardWithErrors(request, response, errorList);
            }
        } catch (Exception e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }

    /**
     * Forward to add film page with error list
     *
     * @param request   {@link HttpServletRequest}
     * @param response  {@link HttpServletResponse}
     * @param errorList received error List from validation service
     */
    private void forwardWithErrors(HttpServletRequest request, HttpServletResponse response, List<String> errorList) throws ServletException, IOException {
        VALID_ERROR_USER_PARAM_LIST.stream()
                .filter(error -> request.getAttribute(error) != null)
                .forEach(error -> request.setAttribute(error, false));
        errorList.forEach(error -> request.setAttribute(error, true));
        request.getRequestDispatcher(JspPagePathConstants.REGISTER_PAGE_PATH).forward(request, response);
    }

    /**
     * Call service method to save user
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     */
    private void saveUser(HttpServletRequest request, HttpServletResponse response, User user) throws ServiceException {
        final boolean inserted = userService.create(user);
        final int userId = userService.getMaxId();
        user.setId(userId);
        if (inserted) {
            prepareUserSessionAndCookie(request, response, user);
        } else {
            logger.warn("User wasn't saved");
            throw new RegisterException("Couldn't create user, perhaps such user already exists");
        }
    }

    /**
     * Set cookies and Session attributes (UserId and UserRole)
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @param user     User
     */
    private void prepareUserSessionAndCookie(HttpServletRequest request, HttpServletResponse response, User user) {
        logger.info("User was inserted");
        final HttpSession session = request.getSession(true);
        session.setAttribute(USER_ID, user.getId());
        session.setAttribute(USER_ROLE, user.getUserRole());
        final String rememberMe = request.getParameter("rememberMe");
        cookieService.loginCookie(response, user, rememberMe);
    }

    /**
     * Get Map of needed parameters for user creation
     *
     * @param request {@link HttpServletRequest}
     * @return map of needed parameters for user creation
     */
    private Map<String, String> getUserParamMap(HttpServletRequest request) {
        return RequestBuilder.buildMap(request,
                F_NAME_PARAM, L_NAME_PARAM, EMAIL_PARAM, PASS_PARAM,
                PASS_CONFIRM_PARAM, PHONE_PARAM, NOTIFICATION_PARAM);
    }

    /**
     * Get User object from params Map with encrypted password
     *
     * @param userParamMap parameter map
     * @return User object
     */
    private User getEncryptedUser(Map<String, String> userParamMap) {
        PassEncryptionManager passManager = new PassEncryptionManager();
        String saltValue = passManager.getSaltValue(OtherConstants.SALT_LENGTH);
        String securePassword = passManager.generateSecurePassword(userParamMap.get(PASS_PARAM), saltValue);
        return new User(
                userParamMap.get(F_NAME_PARAM),
                userParamMap.get(L_NAME_PARAM),
                userParamMap.get(EMAIL_PARAM),
                securePassword,
                userParamMap.get(PHONE_PARAM),
                fromCheckboxToBoolean(userParamMap.get(NOTIFICATION_PARAM)),
                saltValue,
                userParamMap.get(VERIFICATION_CODE_PARAM),
                fromCheckboxToBoolean(userParamMap.get(ENABLED_PARAM))
        );
    }

    /**
     * Get boolean from checkbox value
     *
     * @param checkboxValue checkboxValue
     * @return checkboxValue in boolean
     */
    private boolean fromCheckboxToBoolean(String checkboxValue) {
        return checkboxValue != null && checkboxValue.equals("on");
    }
}
