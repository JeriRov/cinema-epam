package epam.project.finalproject.utilities;

import jakarta.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Map;

public class RequestBuilder {

    private RequestBuilder() {
        throw new IllegalStateException("Utility class");
    }
    public static Map<String, String> buildMap(HttpServletRequest request, String... parameters) {
        Map<String, String> paramsMap = new HashMap<>();
        for (String param : parameters) {
            paramsMap.put(param, request.getParameter(param));
        }
        return paramsMap;
    }
}
