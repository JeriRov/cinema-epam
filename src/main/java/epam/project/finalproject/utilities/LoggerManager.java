package epam.project.finalproject.utilities;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class manager for Logger (Log4j)
 */
public class LoggerManager {
    private static final String PROPERTIES_FILE = "/log4j.properties";

    private LoggerManager() {}

    /**
     * Return Logger object
     *
     * @param aClass class which will use the logger
     * @return Logger object
     */
    public static Logger getLogger(Class<?> aClass) {
        final Logger logger = LoggerFactory.getLogger(aClass);
        try {
           PropertyConfigurator.configure(LoggerManager.class.getResourceAsStream(PROPERTIES_FILE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return logger;
    }
}
