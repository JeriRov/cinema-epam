package epam.project.finalproject.utilities;

import epam.project.finalproject.connection.ConnectionPool;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {
    private static final Logger logger = LoggerManager.getLogger(PropertiesManager.class);
    
    public static Properties getProperties(String path) {
        Properties properties = new Properties();
        try {
            properties.load(ConnectionPool.class.getResourceAsStream(path));
        } catch (IOException e) {
            logger.error("Can't load properties file for SMTP", e);
        }
        return properties;
    }
}
