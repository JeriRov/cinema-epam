package epam.project.finalproject.utilities;

import epam.project.finalproject.utilities.constants.CommandConstants;

/**
 * Manager for app inner redirection among commands
 */
public class RedirectManager {
    private RedirectManager() {
    }

    /**
     * Form path to redirect to page of some command
     *
     * @param command received command
     * @return path
     */
    public static String getRedirectLocation(String command) {
        return CommandConstants.COMMAND_MAIN_SERVLET + "?command=" + command;
    }
}
