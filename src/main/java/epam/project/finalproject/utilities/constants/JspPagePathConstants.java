package epam.project.finalproject.utilities.constants;

/**
 * Constants of all JSP page paths
 */
public final class JspPagePathConstants {
    public static final String MAIN_PAGE_PATH = "/WEB-INF/jsp/main.jsp";
    public static final String SCHEDULE_PAGE_PATH = "/WEB-INF/jsp/schedule.jsp";

    public static final String REGISTER_PAGE_PATH = "/WEB-INF/jsp/register.jsp";
    public static final String LOGIN_PAGE_PATH = "/WEB-INF/jsp/login.jsp";

    public static final String ERROR_PAGE_PATH = "/WEB-INF/jsp/errors/error.jsp";

    public static final String USER_PROFILE_PAGE_PATH = "/WEB-INF/jsp/profile.jsp";

    public static final String ADD_FILM_PAGE_PATH = "/WEB-INF/jsp/admin/addFilm.jsp";
    public static final String FILMS_SETTING_PAGE_PATH = "/WEB-INF/jsp/admin/filmsSetting.jsp";
    public static final String FILM_INFO_PAGE_PATH = "/WEB-INF/jsp/film.jsp";


    public static final String ADD_SESSION_PAGE_PATH = "/WEB-INF/jsp/admin/addSession.jsp";
    public static final String SESSIONS_SETTING_PAGE_PATH = "/WEB-INF/jsp/admin/sessionsSetting.jsp";
    public static final String SESSIONS_PAGE_PATH = "/WEB-INF/jsp/session.jsp";

    public static final String PAYING_PAGE_PATH = "/WEB-INF/jsp/paying.jsp";
    public static final String SUCCESS_PAGE_PATH = "/WEB-INF/jsp/success.jsp";

    public static final String VERIFICATION_PAGE_PATH = "/WEB-INF/jsp/verify.jsp";

    private JspPagePathConstants() {
    }
}
