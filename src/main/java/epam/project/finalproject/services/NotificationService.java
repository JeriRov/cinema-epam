package epam.project.finalproject.services;

import epam.project.finalproject.exceptions.ServiceException;
import org.quartz.JobDetail;
import org.quartz.Trigger;

public interface NotificationService {
    /**
     * @param jobDetail Details about job
     * @param trigger Trigger for job
     * @throws ServiceException Job can't run
     */
    void createNotification(JobDetail jobDetail, Trigger trigger) throws ServiceException;
}
