package epam.project.finalproject.services;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.ServiceException;
import jakarta.servlet.http.HttpServletRequest;

import java.util.Locale;

/**
 * Verification service
 */
public interface VerifyService {
    /**
     * A method that checks whether the verification code is valid
     *
     * @param verificationCode Verification code
     * @return True if the verification code is valid otherwise false
     * @throws ServiceException Couldn't find user with this verification code або couldn't get user from Database
     */
    boolean verify(String verificationCode) throws ServiceException;

    /**
     * Method for saving the user's verification code to the database
     *
     * @param user The user who needs to save the code
     * @param verifyCode Verification code
     * @throws ServiceException Couldn't update user
     */
    void saveVerificationCodeToUser(User user, String verifyCode) throws ServiceException;

    /**
     * Method for send a verification code to an email address
     *
     * @param user User to whom the email is being sent
     * @param verifyURL Link for verification
     * @param locale Locale for translation
     * @throws ServiceException The message is not generated or the address is not correct
     */
    void sendMail(User user, String verifyURL, Locale locale) throws ServiceException;

    /**
     * Method for generate verify url
     *
     * @param request Request to get app URL
     * @param code Verification code
     * @return Verify URL
     */
    String getVerifyURL(HttpServletRequest request, String code);
}
