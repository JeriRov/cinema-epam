package epam.project.finalproject.services.impl;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.mail.MailSender;
import epam.project.finalproject.services.MailService;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.PropertiesManager;
import org.slf4j.Logger;

import java.util.Properties;

/**
 * Class service of Email sender
 */
public class MailServiceImpl implements MailService {
    private static final Logger logger = LoggerManager.getLogger(MailServiceImpl.class);
    private static final String CLASS_NAME = MailServiceImpl.class.getName();
    private static final String PROPERTIES_FILE = "/mail.properties";


    @Override
    public void sendEmail(String mailTo, String subject, String body) throws ServiceException {
        logger.debug("SendEmail from {}", CLASS_NAME);
        try {
            Properties properties = PropertiesManager.getProperties(PROPERTIES_FILE);
            logger.info("Email properties: {}", properties);
            MailSender sender = new MailSender(mailTo, subject, body, properties);
            sender.send();
        } catch (Exception e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
}
