package epam.project.finalproject.services.impl;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.PaymentService;
import epam.project.finalproject.services.ValidService;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.PropertiesManager;
import org.json.JSONObject;
import org.slf4j.Logger;

import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static epam.project.finalproject.utilities.constants.OtherConstants.*;

public class PaymentServiceImpl implements PaymentService {
    private static final Logger logger = LoggerManager.getLogger(PaymentServiceImpl.class);
    private static final String PROPERTIES_FILE = "/bank.properties";
    private final Properties properties;
    private final ValidService validService;

    public PaymentServiceImpl() {
        properties = PropertiesManager.getProperties(PROPERTIES_FILE);
        validService = new ValidServiceImpl();
    }

    @Override
    public void makePayment(Map<String, String> card, BigDecimal totalCost) throws IOException, ServiceException {
        String cashlessLink = properties.getProperty("bank.api.link") + properties.getProperty("bank.api.cashless");
        URL url = new URL(cashlessLink);
        HttpURLConnection http = (HttpURLConnection) url.openConnection();
        http.setRequestMethod("POST");
        http.setDoOutput(true);
        http.setRequestProperty("Accept", "application/json");
        http.setRequestProperty("Content-Type", "application/json");
        JSONObject jsonBody = makePaymentBody(card, totalCost);

        logger.info("Transaction body: {}", jsonBody);

        byte[] out = jsonBody.toString().getBytes(StandardCharsets.UTF_8);

        OutputStream stream = http.getOutputStream();
        stream.write(out);
        http.disconnect();
        int responseCode = http.getResponseCode();
        if (responseCode < 200 || responseCode > 299) {
            throw new ServiceException("The payment was not successful");
        }
    }

    private JSONObject makePaymentBody(Map<String, String> card, BigDecimal totalCost) {
        JSONObject jsonBody = new JSONObject();
        jsonBody.put("senderCardNumber", card.get(CREDIT_CARD_NUMBER));
        jsonBody.put("receiverCardNumber", properties.getProperty("bank.account.number"));
        jsonBody.put("senderCardCVV", card.get(CREDIT_CARD_CVV));
        jsonBody.put("senderExpiryDate", card.get(CREDIT_CARD_DATE));
        jsonBody.put("sum", totalCost);
        jsonBody.put("category", "Cinema");
        jsonBody.put("currencyName", CURRENCY);
        jsonBody.put("purpose", "AMCinema ticket.");

        return jsonBody;
    }

    @Override
    public void validateCard(Map<String, String> card) throws ServiceException {
        List<String> errorList = new ArrayList<>();
        validService.validCreditCardField(errorList, card);
        if (!errorList.isEmpty())
            throw new ServiceException("The card data is incorrect");
    }
}
