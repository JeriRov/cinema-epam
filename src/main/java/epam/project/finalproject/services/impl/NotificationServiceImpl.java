package epam.project.finalproject.services.impl;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.services.NotificationService;
import epam.project.finalproject.utilities.LoggerManager;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;

public class NotificationServiceImpl implements NotificationService {
    private static final Logger logger = LoggerManager.getLogger(NotificationServiceImpl.class);
    private final StdSchedulerFactory schedulerFactory;

    public NotificationServiceImpl() {
        schedulerFactory = new StdSchedulerFactory();
    }

    public NotificationServiceImpl(StdSchedulerFactory schedulerFactory) {
        this.schedulerFactory = schedulerFactory;
    }

    @Override
    public void createNotification(JobDetail jobDetail, Trigger trigger) throws ServiceException {
        try {
            Scheduler scheduler = schedulerFactory.getScheduler();
            if (scheduler.checkExists(jobDetail.getKey())) {
                logger.info("Key already exist: {}", jobDetail.getKey());
                return;
            }
            scheduler.start();
            scheduler.scheduleJob(jobDetail, trigger);

        } catch (SchedulerException e) {
            throw new ServiceException(e.getMessage());
        }
    }
}
