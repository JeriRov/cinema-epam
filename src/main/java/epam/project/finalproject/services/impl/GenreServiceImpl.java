package epam.project.finalproject.services.impl;

import epam.project.finalproject.exceptions.EmptyArrayException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.utilities.LoggerManager;
import org.slf4j.Logger;
import epam.project.finalproject.dao.GenreDAO;
import epam.project.finalproject.dao.factories.DAOFactory;
import epam.project.finalproject.dao.factories.DAOFactoryDeliver;
import epam.project.finalproject.entities.Genre;
import epam.project.finalproject.services.GenreService;

import java.util.ArrayList;
import java.util.List;

/**
 * Service class for Genre
 */
public class  GenreServiceImpl implements GenreService {
    private static final Logger logger = LoggerManager.getLogger(GenreServiceImpl.class);
    private static final String CLASS_NAME = GenreServiceImpl.class.getName();

    @Override
    public List<Genre> getGenreListByIdArray(String[] genreIds) throws ServiceException {
        if (genreIds == null || genreIds.length == 0) {
            logger.error("Genre Array is null or empty");
            throw new EmptyArrayException("Genre Array is null or empty");
        }
        List<Genre> genreList = new ArrayList<>();
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final GenreDAO genreDAO = factory.getGenreDAO();
            for (String genreId : genreIds) {
                final int id = Integer.parseInt(genreId);
                final Genre genre = genreDAO.findById(id);
                genreList.add(genre);
            }
        } catch (Exception e) {
            throwServiceException(e);
        }
        return genreList;
    }

    @Override
    public List<Genre> getAll() throws ServiceException {
        List<Genre> genreList = new ArrayList<>();
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final GenreDAO genreDAO = factory.getGenreDAO();
            genreList = genreDAO.findAll();
        } catch (Exception e) {
            throwServiceException(e);
        }
        return genreList;
    }

    private void throwServiceException(Exception e) throws ServiceException {
        logger.error("Couldn't get genre list", e);
        throw new ServiceException("Couldn't get genre list", e);
    }

    private void logCreatingDaoFactory() {
        logger.debug("Created DAOFactory in {}", CLASS_NAME);
    }

}
