package epam.project.finalproject.services.impl;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.mail.messages.VerifyMailMessage;
import epam.project.finalproject.services.MailService;
import epam.project.finalproject.services.UserService;
import epam.project.finalproject.services.VerifyService;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.constants.CommandConstants;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;

import java.util.Locale;

public class VerifyServiceImpl implements VerifyService {
    private static final Logger logger = LoggerManager.getLogger(VerifyServiceImpl.class);
    private final UserService userService;
    private final MailService mailService;

    public VerifyServiceImpl() {
        this.userService = new UserServiceImpl();
        this.mailService = new MailServiceImpl();
    }

    public VerifyServiceImpl(UserService userService, MailService mailService) {
        this.userService = userService;
        this.mailService = mailService;
    }

    public boolean verify(String verificationCode) throws ServiceException {
        if (verificationCode == null) {
            throw new NullPointerException("Verification code cannot be null");
        }

        User user = userService.findByVerificationCode(verificationCode);
        if (user == null || user.isEnabled()) {
            return false;
        } else {
            user.setVerificationCode(null);
            user.setEnabled(true);
            userService.save(user);
            return true;
        }
    }

    @Override
    public void saveVerificationCodeToUser(User user, String verifyCode) throws ServiceException {
        user.setVerificationCode(verifyCode);
        userService.save(user);
        logger.info("Verification email saved");
    }

    @Override
    public void sendMail(User user, String verifyURL, Locale locale) throws ServiceException {
        VerifyMailMessage message = new VerifyMailMessage(user, verifyURL, locale);
        mailService.sendEmail(user.getEmail(), message.subject().toString(), message.body().toString());
        logger.info("Verification email sent");
    }

    @Override
    public String getVerifyURL(HttpServletRequest request, String code) {
        return request.getRequestURL() + "?command=" + CommandConstants.COMMAND_VIEW_VERIFICATION + "&code=" + code;
    }
}
