package epam.project.finalproject.services.impl;

import epam.project.finalproject.exceptions.VerifyException;
import epam.project.finalproject.services.CaptchaService;
import epam.project.finalproject.utilities.LoggerManager;
import epam.project.finalproject.utilities.PropertiesManager;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.Properties;

/**
 * Class service for Google Recaptcha verification
 */
public class CaptchaServiceImpl implements CaptchaService {
    private static final Logger logger = LoggerManager.getLogger(CaptchaServiceImpl.class);
    private static final String CLASS_NAME = CaptchaServiceImpl.class.getName();
    private static final String USER_AGENT = "Mozilla/5.0";
    private static final String PROPERTIES_FILE = "/captcha.properties";

    @Override
    public void captchaValidation(HttpServletRequest request, HttpServletResponse response) {
        try {
            String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
            logger.info("gRecaptchaResponse={}", gRecaptchaResponse);
            if (!verify(gRecaptchaResponse)) throw new VerifyException("Couldn't verify the captcha, try again");
        } catch (IOException e) {
            ErrorService.handleException(request, response, CLASS_NAME, e);
        }
    }

    /**
     * Verification of Google Recaptcha
     *
     * @param gRecaptchaResponse Google recaptcha string response
     * @return true if Google Recaptcha is successful and false if not
     */
    private boolean verify(String gRecaptchaResponse) throws IOException {
        if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
            logger.warn("gRecaptchaResponse is null or empty, couldn't verify");
            return false;
        }
        Properties properties = PropertiesManager.getProperties(PROPERTIES_FILE);
        String captchaURL = properties.getProperty("captcha.url");
        URL obj = new URL(captchaURL);
        HttpsURLConnection connection = (HttpsURLConnection) obj.openConnection();

        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        String postParams = "secret=" + properties.getProperty("captcha.secret.key") + "&response=" + gRecaptchaResponse;
        connection.setDoOutput(true);
        DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
        dataOutputStream.writeBytes(postParams);
        dataOutputStream.flush();
        dataOutputStream.close();

        int responseCode = connection.getResponseCode();
        logger.debug("Sending 'POST' request to URL : {}", captchaURL);
        logger.debug("Post parameters: {}, response Code: {}", postParams, responseCode);
        return readFromConnection(connection);
    }

    /**
     * Reading received info from connection
     *
     * @param connection HttpsURLConnection
     * @return true if Google Recaptcha is successful and false if not
     */
    private boolean readFromConnection(HttpsURLConnection connection) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = reader.readLine()) != null) {
            response.append(inputLine);
        }
        reader.close();
        JsonReader jsonReader = Json.createReader(new StringReader(response.toString()));
        JsonObject jsonObject = jsonReader.readObject();
        jsonReader.close();
        return jsonObject.getBoolean("success");
    }
}
