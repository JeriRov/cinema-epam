package epam.project.finalproject.services.impl;

import epam.project.finalproject.exceptions.EmptyArrayException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.utilities.LoggerManager;
import org.slf4j.Logger;
import epam.project.finalproject.dao.SeatDAO;
import epam.project.finalproject.dao.factories.DAOFactory;
import epam.project.finalproject.dao.factories.DAOFactoryDeliver;
import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.services.SeatService;

import java.util.ArrayList;
import java.util.List;

/**
 * Service class for Film
 */
public class SeatServiceImpl implements SeatService {
    private static final Logger logger = LoggerManager.getLogger(SeatServiceImpl.class);
    private static final String CLASS_NAME = SeatServiceImpl.class.getName();

    @Override
    public List<Seat> getSeatListByIdArray(String[] seatIds) throws ServiceException {
        if (seatIds == null || seatIds.length == 0) {
            logger.error("Seat Array is null or empty");
            throw new EmptyArrayException("Seat Array is null or empty");
        }
        List<Seat> seatList = new ArrayList<>();
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final SeatDAO seatDao = factory.getSeatDao();
            for (String genreId : seatIds) {
                final int id = Integer.parseInt(genreId);
                final Seat seat = seatDao.findById(id);
                seatList.add(seat);
            }
        } catch (Exception e) {
            throwServiceException(e);
        }
        return seatList;
    }

    @Override
    public List<Seat> getFreeSeatsBySessionId(int id) throws ServiceException {
        List<Seat> seatList = new ArrayList<>();
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final SeatDAO seatDAO = factory.getSeatDao();
            seatList = seatDAO.findAllFreeSeatBySessionId(id);
        } catch (Exception e) {
            throwServiceException(e);
        }
        return seatList;
    }

    @Override
    public List<Seat> getAll() throws ServiceException {
        List<Seat> seatList = new ArrayList<>();
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final SeatDAO seatDAO = factory.getSeatDao();
            seatList = seatDAO.findAll();
        } catch (Exception e) {
            throwServiceException(e);
        }
        return seatList;
    }

    @Override
    public boolean isSeatFreeBySessionId(int seatId, int sessionId) throws ServiceException {
        boolean isFree = false;
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final SeatDAO seatDAO = factory.getSeatDao();
            isFree = seatDAO.isSeatFree(seatId, sessionId);
        } catch (Exception e) {
            throwServiceException(e);
        }
        return isFree;
    }

    private void logCreatingDaoFactory() {
        logger.debug("Created DAOFactory in {}", CLASS_NAME);
    }

    private void throwServiceException(Exception e) throws ServiceException {
        logger.error("Couldn't get seat list", e);
        throw new ServiceException("Couldn't get seat list", e);
    }

}
