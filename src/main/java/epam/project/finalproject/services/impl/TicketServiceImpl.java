package epam.project.finalproject.services.impl;

import epam.project.finalproject.dao.TicketDAO;
import epam.project.finalproject.dao.factories.DAOFactory;
import epam.project.finalproject.dao.factories.DAOFactoryDeliver;
import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.entities.Session;
import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.exceptions.EmptyListException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.exceptions.TicketException;
import epam.project.finalproject.mail.notification.NotificationTicketJob;
import epam.project.finalproject.services.NotificationService;
import epam.project.finalproject.services.SeatService;
import epam.project.finalproject.services.TicketService;
import epam.project.finalproject.utilities.LoggerManager;
import org.quartz.*;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Service class for Film
 */
public class TicketServiceImpl implements TicketService {
    private static final Logger logger = LoggerManager.getLogger(TicketServiceImpl.class);
    private static final String CLASS_NAME = TicketServiceImpl.class.getName();
    private final SeatService seatService;
    private final NotificationService notificationService;

    public TicketServiceImpl() {
        seatService = new SeatServiceImpl();
        notificationService = new NotificationServiceImpl();
    }

    public TicketServiceImpl(SeatService seatService, NotificationService notificationService) {
        this.seatService = seatService;
        this.notificationService = notificationService;
    }

    @Override
    public void saveAll(List<Ticket> ticketList) throws ServiceException {
        if (ticketList == null || ticketList.isEmpty()) {
            logger.warn("Received ticket list is null or empty");
            throw new EmptyListException("There are no tickets that could be purchased");
        }
        try {
            for (Ticket ticket : ticketList) {
                save(ticket);
            }
        } catch (Exception e) {
            throwServiceException("Some of the tickets you wanted to order have already been sold", e);
        }
    }

    @Override
    public void sendTicketNotification(List<Ticket> tickets, User user, Locale locale) throws ServiceException {
        if (!user.getNotification()) return;

        Ticket ticket = tickets.get(0);
        Session session = ticket.getSession();
        String notificationName = "notification" + session.getId();
        String notificationTriggerName = "notificationTrigger" + session.getId();

        JobDataMap dataMap = new JobDataMap();
        dataMap.put("ticket", ticket);
        dataMap.put("user", user);
        dataMap.put("locale", locale);

        JobDetail jobDetail = JobBuilder.newJob(NotificationTicketJob.class)
                .withIdentity(notificationName, "notificationGroup").setJobData(dataMap).build();

        Date startAt = Date.valueOf(session.getDate());

        logger.info("Start at : {}", startAt);
        SimpleTrigger trigger = (SimpleTrigger) TriggerBuilder.newTrigger()
                .withIdentity(notificationTriggerName, "notificationGroup")
                .startAt(startAt)
                .forJob(notificationName, "notificationGroup")
                .build();
        notificationService.createNotification(jobDetail, trigger);
    }


    @Override
    public void save(Ticket ticket) throws ServiceException {
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final int seatId = ticket.getSeat().getId();
            final int sessionId = ticket.getSession().getId();
            TicketDAO ticketDao = factory.getTicketDao();
            if (seatService.isSeatFreeBySessionId(seatId, sessionId)) {
                logger.debug("Seat is free, id: {} and sessionId: {}", seatId, sessionId);
                ticketDao.insert(ticket);
            } else {
                logger.warn("Seat is already reserved");
                throw new TicketException("Seat is already reserved, choose another one");
            }
        } catch (Exception e) {
            throwServiceException("Couldn't save ticket", e);
        }
    }

    @Override
    public List<Ticket> formTicketList(Session session, List<Seat> seatList, User user) {
        final List<Ticket> ticketList = new ArrayList<>();
        for (Seat seat : seatList) {
            final Ticket ticket = new Ticket(session, user, seat, session.getTicketPrice());
            ticketList.add(ticket);
        }
        if (ticketList.isEmpty()) {
            logger.error("Couldn't form TicketList, it is Empty");
            throw new EmptyListException("Couldn't form TicketList, it is Empty");
        }
        return ticketList;
    }


    @Override
    public BigDecimal countTotalCostOfTicketList(final List<Ticket> ticketList) {
        if (ticketList == null || ticketList.isEmpty()) {
            logger.error("seatList is null or empty, can't count total cost");
            throw new EmptyListException("seatList is null or empty, can't count total cost");
        }
        BigDecimal totalCost = new BigDecimal(0);
        for (Ticket ticket : ticketList) {
            totalCost = totalCost.add(ticket.getTicketPrice());
        }
        return totalCost;
    }

    @Override
    public List<Ticket> getAllByUserId(int userId, int page, int size) throws ServiceException {
        List<Ticket> ticketList = new ArrayList<>();
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final TicketDAO ticketDao = factory.getTicketDao();
            int start = page;
            if (page > 1) {
                start--;
                start = start * size + 1;
            }
            ticketList = ticketDao.findAllByUserId(userId, start, size);
        } catch (Exception e) {
            throwServiceException("Couldn't get ticket list by user id", e);
        }
        return ticketList;
    }

    @Override
    public int countTotalPagesByUserId(int userId, int size) throws ServiceException {
        int amount = 0;
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final TicketDAO ticketDao = factory.getTicketDao();
            final int count = ticketDao.countTotalRowByUserId(userId);
            amount = count / size;
            amount = count % size == 0 ? amount : amount + 1;
        } catch (Exception e) {
            throwServiceException("Couldn't get paginated user's ticket list", e);
        }
        return amount;
    }

    @Override
    public Ticket getById(int id) throws ServiceException {
        Ticket ticket = null;
        try (DAOFactory factory = DAOFactoryDeliver.getInstance().getFactory()) {
            logCreatingDaoFactory();
            final TicketDAO ticketDao = factory.getTicketDao();
            ticket = ticketDao.findById(id);
        } catch (Exception e) {
            throwServiceException("Couldn't find ticket", e);
        }
        return ticket;
    }

    private void logCreatingDaoFactory() {
        logger.debug("Created DAOFactory in {}", CLASS_NAME);
    }

    private void throwServiceException(String message, Exception e) throws ServiceException {
        logger.error(message, e);
        throw new ServiceException(message, e);
    }
}
