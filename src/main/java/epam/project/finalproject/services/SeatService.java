package epam.project.finalproject.services;

import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.exceptions.ServiceException;

import java.util.List;

public interface SeatService {
    /**
     * Get seat list by its ids
     *
     * @param seatIds array of ids of seats
     * @return list of seats
     */
    List<Seat> getSeatListByIdArray(String[] seatIds) throws ServiceException;

    /**
     * Get list of free seats by session id
     *
     * @param id session id
     * @return list of free seats
     */
    List<Seat> getFreeSeatsBySessionId(int id) throws ServiceException;

    /**
     * Get all seats of cinema hall
     *
     * @return seat list
     */
    List<Seat> getAll() throws ServiceException;

    /**
     * Checks if seat is free for the sesion
     *
     * @param seatId    seat id
     * @param sessionId session id
     * @return true - is free, otherwise - false
     */
    boolean isSeatFreeBySessionId(int seatId, int sessionId) throws ServiceException;
}
