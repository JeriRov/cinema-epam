package epam.project.finalproject.services;

import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.entities.Session;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface SessionService {
    /**
     * Get session by id
     *
     * @param id session id
     * @return Session
     */
    Session getById(int id) throws ServiceException;

    /**
     * Delete session by its id
     *
     * @param id session id
     */
    void delete(int id) throws ServiceException;

    /**
     * Count total page amount by received size
     *
     * @param size page size
     * @return amount of page
     */
    int countTotalPages(int size) throws ServiceException;

    /**
     * Save session
     *
     * @param session session
     */
    void create(Session session) throws ServiceException;

    /**
     * Get all paginated session list
     *
     * @param page page number
     * @param size page suze
     * @return list of sessions
     */
    List<Session> getAll(int page, int size) throws ServiceException;

    /**
     * Get paginated and filtered list of session by filterSortMap
     *
     * @param filterSortMap map of params for filtering and sorting
     * @param page          page nubmer
     * @param size          page size
     * @return list of sessions
     */
    List<Session> getFilteredAndSorted(Map<String, String> filterSortMap, int page, int size) throws ServiceException;

    /**
     * Clean off received parameterMap of non filter/sort parameters
     *
     * @param parameterMap request's parameterMap
     * @return map containing only filter/sort parameters
     */
    Map<String, String> getFilterSortMapFromParams(Map<String, String[]> parameterMap);

    /**
     * A method for validating a session
     *
     * @param session Session to be checked
     * @return Error list
     */
    Collection<String> sessionValid(Session session) throws ServiceException;

    /**
     * Get error list after session validation
     *
     * @param sessionParamMap map with session params
     * @return List of validate errors
     */
    List<String> getSessionFieldsValidErrorList(Map<String, String> sessionParamMap);
}
