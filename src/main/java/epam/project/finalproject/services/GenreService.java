package epam.project.finalproject.services;

import epam.project.finalproject.entities.Genre;
import epam.project.finalproject.exceptions.ServiceException;

import java.util.List;

public interface GenreService {
    /**
     * Get genres by its ids
     *
     * @param genreIds array of genre ids
     * @return list of genres
     */
    List<Genre> getGenreListByIdArray(String[] genreIds) throws ServiceException;

    /**
     * Get all genres
     * @return list of all genres
     */
    List<Genre> getAll() throws ServiceException;

}
