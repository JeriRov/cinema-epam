package epam.project.finalproject.services;

import epam.project.finalproject.exceptions.ServiceException;

public interface MailService {
    /**
     * A method to send an email message to a user
     *
     * @param mailTo The address of the recipient of the email message
     * @param subject Subject message
     * @param body Body message
     * @throws ServiceException The message is not generated or the address is not correct
     */
    void sendEmail(String mailTo, String subject, String body) throws ServiceException;
}
