package epam.project.finalproject.services;

import epam.project.finalproject.exceptions.ServiceException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

public interface PaymentService {
    void makePayment(Map<String, String> card, BigDecimal totalCost) throws IOException, ServiceException;

    void validateCard(Map<String, String> card) throws ServiceException;
}
