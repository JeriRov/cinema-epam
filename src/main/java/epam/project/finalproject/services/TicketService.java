package epam.project.finalproject.services;

import epam.project.finalproject.entities.Seat;
import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.entities.Session;
import epam.project.finalproject.entities.User;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

public interface TicketService {
    /**
     * Save ticket list
     *
     * @param ticketList ticket list
     */
    void saveAll(List<Ticket> ticketList) throws ServiceException;

    /**
     * Save ticket
     *
     * @param ticket ticket
     */
    void save(Ticket ticket) throws ServiceException;

    /**
     * Form Ticket List for User by chosen Session and Seats
     *
     * @param session  chosen Session
     * @param seatList chosen Seats
     * @param user     other-words customer
     * @return formed TicketList or throw an exception if no tickets added
     */
    List<Ticket> formTicketList(Session session, List<Seat> seatList, User user);

    /**
     * Count total cost of all tickets
     *
     * @param ticketList chosen tickets
     * @return total cost of tickets
     */
    BigDecimal countTotalCostOfTicketList(List<Ticket> ticketList);

    /**
     * Get ticket list by user id
     *
     * @param userId use rid
     * @param page   page number
     * @param size   page size
     * @return ticket list
     */
    List<Ticket> getAllByUserId(int userId, int page, int size) throws ServiceException;

    /**
     * Count page amount of all user tickets
     *
     * @param userId user id
     * @param size   page size
     * @return page amount
     */
    int countTotalPagesByUserId(int userId, int size) throws ServiceException;

    /**
     * Get ticket by id
     *
     * @param id ticket id
     * @return Ticket
     */
    Ticket getById(int id) throws ServiceException;

    /**
     * @param tickets All tickets that the user wishes to purchase
     * @param user User who buys tickets
     * @param locale {@link Locale}
     * @throws ServiceException Notification failed
     */
    void sendTicketNotification(List<Ticket> tickets, User user, Locale locale) throws ServiceException;
}
