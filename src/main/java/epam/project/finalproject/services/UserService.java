package epam.project.finalproject.services;

import epam.project.finalproject.exceptions.AuthException;
import epam.project.finalproject.exceptions.ServiceException;
import epam.project.finalproject.entities.User;

import java.util.List;
import java.util.Map;

public interface UserService {
    /**
     * Authenticate user by its login and plain password
     *
     * @param login    email
     * @param password plain password
     * @throws AuthException    throws when couldn't authenticate user
     */
    void authenticateUser(String login, String password) throws ServiceException, AuthException;

    /**
     * Get user by its login
     *
     * @param login user email
     * @return User
     */
    User getUserByLogin(String login) throws ServiceException;

    /**
     * Get error list after user validation
     *
     * @param userParamMap map with user params
     * @return List of validate errors
     */
    List<String> getUserValidErrorList(Map<String, String> userParamMap);

    /**
     * Validate user email
     *
     * @param email email
     * @return error param name or blank string when there is no errors
     */
    String validateUserEmail(String email);

    /**
     * Get User by its id
     *
     * @param id user id
     * @return User
     */
    User getById(int id) throws ServiceException;

    /**
     * Get max id of user table
     *
     * @return max user id of table
     */
    int getMaxId() throws ServiceException;

    /**
     * Create user
     *
     * @param user user
     * @return true if created, otherwise false
     */
    boolean create(User user) throws ServiceException;

    /**
     * Save user
     *
     * @param user The user to update
     * @return Updated user
     * @throws ServiceException The user cannot be updated
     */
    User save(User user) throws ServiceException;

    /**
     * User search by confirmation code
     *
     * @param verificationCode Verification code
     * @return A user with such a confirmation code
     * @throws ServiceException There is no user with this code or the user cannot be retrieved from the database
     */
    User findByVerificationCode(String verificationCode) throws ServiceException;
}
