package epam.project.finalproject.mail;


public interface MessageBuilder {
    StringBuilder subject();
    StringBuilder body();
}
