package epam.project.finalproject.mail.messages;

import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.mail.MessageBuilder;
import epam.project.finalproject.utilities.LoggerManager;
import org.slf4j.Logger;

import java.util.Locale;
import java.util.ResourceBundle;

import static epam.project.finalproject.utilities.constants.OtherConstants.LOGO_IMAGE_URI;

public class TicketMailMessage implements MessageBuilder {

    private static final Logger logger = LoggerManager.getLogger(TicketMailMessage.class);
    private final Ticket ticket;
    private final ResourceBundle bundle;

    public TicketMailMessage(Ticket ticket, Locale locale) {
        this.ticket = ticket;
        this.bundle = ResourceBundle.getBundle("i18n", locale);
    }

    @Override
    public StringBuilder subject() {
        return new StringBuilder(bundle.getString("ticket.mail.title")).append(": ").append(ticket.getSession().getFilm().getName());
    }

    @Override
    public StringBuilder body() {

        String logoImage = "<img alt='logo' src='" + LOGO_IMAGE_URI + "' width='500'/> <br/>";
        String ticketNumber = "<strong>" + bundle.getString("ticket.mail.number") + ": " + ticket.getSession().getFilm().getId() + "</strong>" + "<br/>";
        String time = "<strong>" + bundle.getString("ticket.mail.time") + ": " + ticket.getSession().getTime() + "</strong>" + "<br/>";
        String date = "<strong>" + bundle.getString("ticket.mail.date") + ": " + ticket.getSession().getDate() + "</strong>" + "<br/>";
        String row = "<strong>" + bundle.getString("ticket.mail.row") + ": " + ticket.getSeat().getRowNumber() + "</strong>" + "<br/>";
        String place = "<strong>" + bundle.getString("ticket.mail.place") + ": " + ticket.getSeat().getPlaceNumber() + "</strong>" + "<br/>";
        String duration = "<strong>" + bundle.getString("ticket.mail.duration") + ": " + ticket.getSession().getFilm().getDurationInMinutes() +
                bundle.getString("ticket.mail.minutes") + "</strong>" + "<br/>";
        String price = "<strong>" + bundle.getString("ticket.mail.price") + ": " + ticket.getTicketPrice() + "</strong>" + "<br/>";


        StringBuilder result = new StringBuilder(logoImage).append(ticketNumber).append(date).append(time).append(row).append(place).append(duration).append(price);
        logger.info("Mail body: {}", result);

        return result;
    }
}
