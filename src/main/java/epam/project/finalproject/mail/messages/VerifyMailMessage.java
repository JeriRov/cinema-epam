package epam.project.finalproject.mail.messages;

import epam.project.finalproject.entities.User;
import epam.project.finalproject.mail.MessageBuilder;

import java.util.Locale;
import java.util.ResourceBundle;

public class VerifyMailMessage implements MessageBuilder {

    private final User user;
    private final String verifyURI;

    private final ResourceBundle bundle;

    public VerifyMailMessage(User user, String verifyURI, Locale locale) {
        this.user = user;
        this.verifyURI = verifyURI;
        this.bundle = ResourceBundle.getBundle("i18n", locale);
    }

    @Override
    public StringBuilder subject() {
        return new StringBuilder(bundle.getString("mail.verify.message.subject"));
    }

    @Override
    public StringBuilder body() {
        String bodyTitle = bundle.getString("mail.verify.message.title") + " " + user.getFirstName() + " " + user.getLastName() + ",<br>";
        String bodyContent = bundle.getString("mail.verify.message.body") + ":<br>";
        String verifyLink = "<h3><a href='" + verifyURI + "'>" + bundle.getString("mail.verify.message.link") + "</a></h3>";
        String bodyEnd = bundle.getString("mail.verify.message.end") + ",<br> AMCinema.";

        return new StringBuilder(bodyTitle).append(bodyContent).append(verifyLink).append(bodyEnd);
    }
}
