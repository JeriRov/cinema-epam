package epam.project.finalproject.mail.messages;

import epam.project.finalproject.entities.Film;
import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.mail.MessageBuilder;

import java.util.Locale;
import java.util.ResourceBundle;

public class TicketNotificationMessage implements MessageBuilder {

    private final Ticket ticket;
    private final ResourceBundle bundle;

    public TicketNotificationMessage(Ticket ticket, Locale locale) {
        this.ticket = ticket;
        this.bundle = ResourceBundle.getBundle("i18n", locale);
    }

    @Override
    public StringBuilder subject() {
        return new StringBuilder(bundle.getString("notification.ticket.subject"));
    }

    @Override
    public StringBuilder body() {
        Film film = ticket.getSession().getFilm();
        String imageUrl = film.getPosterUrl();
        String image = "<img alt='poster' width='150' src= '" + imageUrl + "'> </img><br>";

        String body = bundle.getString("notification.ticket.body");
        String filmName = "<strong>" + film.getName() + "</strong>";

        body = body.replace("<movie>", filmName);

        return new StringBuilder(image).append(body);
    }
}
