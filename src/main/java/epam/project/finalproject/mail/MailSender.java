package epam.project.finalproject.mail;

import jakarta.mail.*;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

import java.util.Properties;

/**
 * Class for sending mail messages
 */
public class MailSender {
    private MimeMessage message;
    private final String sendToEmail;
    private final String mailSubject;
    private final String mailText;
    private final Properties properties;

    public MailSender(String sendToEmail, String mailSubject, String mailText,
                      Properties properties) {
        this.sendToEmail = sendToEmail;
        this.mailSubject = mailSubject;
        this.mailText = mailText;
        this.properties = properties;
    }


    /**
     * Method for sending the message
     *
     * @throws AddressException   Invalid email address
     * @throws MessagingException The message cannot be generated
     */
    public void send() throws MessagingException {
        try {
            initMessage();
            Transport.send(message);
        } catch (AddressException e) {
            throw new AddressException("Invalid address: " + sendToEmail + " " + e.getMessage());
        } catch (MessagingException e) {
            throw new MessagingException("Error generating or sending message: " + e.getMessage());
        }
    }

    /**
     * Method to init message
     *
     * @throws MessagingException The message cannot be generated
     */
    private void initMessage() throws MessagingException {
        Session mailSession = MailSessionFactory.createSession(properties);
        mailSession.setDebug(true);
        message = new MimeMessage(mailSession);

        message.setSubject(mailSubject, "UTF-8");
        message.setContent(mailText, "text/html; charset=utf-8");
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(sendToEmail));
    }
}
