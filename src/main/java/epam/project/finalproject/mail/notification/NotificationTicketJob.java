package epam.project.finalproject.mail.notification;

import epam.project.finalproject.entities.Ticket;
import epam.project.finalproject.entities.User;
import epam.project.finalproject.mail.messages.TicketNotificationMessage;
import epam.project.finalproject.services.MailService;
import epam.project.finalproject.services.impl.MailServiceImpl;
import epam.project.finalproject.utilities.LoggerManager;
import org.quartz.*;
import org.slf4j.Logger;

import java.util.Locale;

public class NotificationTicketJob implements Job {
    private static final Logger logger = LoggerManager.getLogger(NotificationTicketJob.class);
    private static final String CLASS_NAME = NotificationTicketJob.class.getName();
    private final MailService mailService;

    public NotificationTicketJob() {
        mailService = new MailServiceImpl();
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("Job is in progress {}", CLASS_NAME);
        JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        Ticket ticket = (Ticket) dataMap.get("ticket");
        User user = (User) dataMap.get("user");
        Locale locale = (Locale) dataMap.get("locale");
        logger.info("Ticket : {}", ticket);
        logger.info("User : {}", user);
        logger.info("Locale : {}", locale);
        TicketNotificationMessage notificationMessage = new TicketNotificationMessage(ticket, locale);
        try {
            mailService.sendEmail(user.getEmail(), notificationMessage.subject().toString(), notificationMessage.body().toString());
        } catch (Exception e) {
            throw new JobExecutionException(e.getMessage());
        }
    }
}
