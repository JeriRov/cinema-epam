<%@ tag pageEncoding="UTF-8" language="java" %>

<footer class="footer-container py-4 bg-custom">
    <div class="row">
        <div class="col-12 mx-auto footer-info">
            <address class="footer-made-by">Made by Artem Mahey</address>
            <small class="d-block mb-3 text-muted">EPAM University Program 2022-2023</small>
        </div>
    </div>
</footer>
