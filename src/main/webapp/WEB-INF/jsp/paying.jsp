<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ftg" %>

<fmt:bundle basename="i18n">
    <fmt:message key="general.currency.short" var="currency"/>
    <fmt:message key="session.time" var="time"/>
    <fmt:message key="session.timePrefix" var="timePrefix"/>
    <fmt:message key="session.seatsRemain" var="seatsRemain"/>
    <fmt:message key="session.ticketPrice" var="ticketPrice"/>
    <fmt:message key="session.buyTicket" var="buyTicket"/>
    <fmt:message key="film.duration" var="duration"/>
    <fmt:message key="film.duration.postfix" var="durationPostfix"/>
    <fmt:message key="ticket.cost" var="costTitle"/>
    <fmt:message key="ticket.film" var="filmTitle"/>
    <fmt:message key="ticket.ticketTitle" var="ticketTitle"/>
    <fmt:message key="ticket.seatRow" var="seatRowTitle"/>
    <fmt:message key="ticket.seatPlace" var="seatPlaceTitle"/>
    <fmt:message key="ticket.payBtn" var="payBtn"/>
    <fmt:message key="paying.pageTitle" var="pageTitle"/>
    <fmt:message key="ticket.date" var="dateTitle"/>
    <fmt:message key="paying.totalCost" var="totalCostTitle"/>
</fmt:bundle>

<ftg:header pageTitle="${pageTitle}"/>
<ftg:nav-bar userRole="${sessionScope.userRole}"/>

<c:set var="ticketList" value="${sessionScope.ticketList}"/>
<c:set var="totalCost" value="${requestScope.totalCost}"/>


<main class="container" data-new-gr-c-s-check-loaded="14.1062.0">
    <div class="d-flex flex-row">
        <div class="container-fluid bg-custom pay-ticket-info-container">
            <h1>${pageTitle}</h1>
            <div class="row scroll-list">
                <div class="col-md-12 p-2">
                    <c:forEach var="ticket" items="${ticketList}">
                        <c:set var="session" value="${ticket.session}"/>
                        <c:set var="film" value="${session.film}"/>
                        <c:set var="seat" value="${ticket.seat}"/>

                        <div class="px-4 py-3 film-post card w-100">
                            <h3>${ticketTitle}: </h3>
                            <div class="ticket-element">
                                <h6>${dateTitle}: ${session.date}</h6>
                                <h6>${time}: ${session.time}</h6>
                            </div>
                            <div class="ticket-element">
                                <h6>${seatRowTitle}: ${seat.rowNumber}</h6>
                                <h6>${seatPlaceTitle}: ${seat.placeNumber}</h6>
                            </div>
                            <div class="ticket-element">
                                <h6>${filmTitle}: ${film.name}</h6>
                                <h6>${duration}: ${film.getDurationInMinutes()} ${durationPostfix}</h6>
                            </div>
                            <h5>${costTitle}: ${session.ticketPrice} ${currency}</h5>
                        </div>
                    </c:forEach>
                    <br/>
                </div>
            </div>
        </div>
        <div class="card-paying-container bg-custom">
            <div class="p-4">
                <form class="pay-form" name="seatIds" method="post" action="main">
                    <jsp:directive.include file="../jsp/creditCardComponent.jsp"/>
                    <input type="hidden" name="totalCost" value="${totalCost}">
                    <input type="hidden" name="command" value="buyTicket">
                    <button type="submit" class="btn w-100 btn-success">${payBtn}</button>
                </form>
                <h2>${totalCostTitle}: ${totalCost}</h2>
            </div>
        </div>
    </div>
</main>
<ftg:footer/>

