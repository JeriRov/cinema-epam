<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ftg" %>

<fmt:bundle basename="i18n">
    <fmt:message key="successPay.pageTitle" var="pageTitle"/>
</fmt:bundle>

<ftg:header pageTitle="${pageTitle}"/>
<ftg:nav-bar userRole="${sessionScope.userRole}"/>

<div class="success" id="main">
    <h3>${requestScope.text}</h3>
</div>