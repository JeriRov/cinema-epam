<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ftg" %>
<fmt:bundle basename="i18n">
    <fmt:message key="error.pageTitle" var="pageTitle"/>
    <fmt:message key="error.cantFindPage" var="cantFindPage"/>
    <fmt:message key="error.failedReqFrom" var="failedReqFrom"/>
    <fmt:message key="error.backPrevPage" var="backPrevPage"/>
    <fmt:message key="error.goMainPage" var="goMainPage"/>
</fmt:bundle>
<ftg:header pageTitle="${pageTitle}"/>
<ftg:nav-bar userRole="${sessionScope.userRole}"/>

<div class="d-flex justify-content-center align-items-center bg-custom error-modal" id="main">
    <h1 class="mr-3 px-2 align-top border-right inline-block align-content-center">404</h1>
    <div class="inline-block align-middle">
        <h2 class="font-weight-normal lead" id="desc">${cantFindPage}</h2>
        <h2 class="font-weight-normal lead">${failedReqFrom} ${pageContext.errorData.requestURI}</h2>
        <button onclick="prevPage()" class="btn btn-link">${backPrevPage}</button>
        <a href="${pageContext.request.contextPath}/main" class="btn btn-link">${goMainPage}</a>

    </div>
</div>

