<style>
    <jsp:directive.include file="/WEB-INF/css/creditCard.css" />
</style>

<div class="creditCardBody">
    <div class="creditCardContainer">
        <div class="wrapper">
            <div class="outer-card bg-custom">
                <div class="forms">
                    <div class="input-items"><span>Card Number</span> <input class="creditCardInput"
                                                                             name="creditCardNumber"
                                                                             placeholder=".... .... .... ...."
                                                                             data-slots="."
                                                                             data-accept="\d" size="19"></div>
                    <div class="input-items"><span>Name on card</span> <input class="creditCardInput"
                                                                              name="creditCardUserName"
                                                                              placeholder="Samuel Iscon"></div>
                    <div class="one-line">
                        <div class="input-items"><span>Expiry Date</span> <input class="creditCardInput"
                                                                                 name="creditCardDate"
                                                                                 placeholder="yy/mm" data-slots="my">
                        </div>
                        <div class="input-items"><span>CVV</span> <input class="creditCardInput" placeholder="..."
                                                                         name="creditCardCvv"
                                                                         data-slots="."
                                                                         data-accept="\d"
                                                                         size="3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    <jsp:directive.include file="/WEB-INF/js/creditCard.js" />
</script>