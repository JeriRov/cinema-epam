<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ftg" %>

<fmt:bundle basename="i18n">
    <fmt:message key="successPay.pageTitle" var="pageTitle"/>
    <fmt:message key="successPay.backMain" var="backMain"/>
    <fmt:message key="successPay.success" var="success"/>
    <fmt:message key="error.backPrevPage" var="backPrevPage"/>
</fmt:bundle>

<ftg:header pageTitle="${pageTitle}"/>
<ftg:nav-bar userRole="${sessionScope.userRole}"/>
<div class="success" id="main">
    <div class="success-container">
        <h1 class="align-top inline-block align-content-center">${success}</h1>
        <div class="success-back inline-block align-middle">
            <h2 class="font-weight-normal lead success-text">${requestScope.text}</h2>
            <a href="main" class="btn btn-link">${backMain}</a>
        </div>
    </div>
</div>
