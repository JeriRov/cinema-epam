
$('#password, #confirm_password').on('keyup', function () {
    if ($('#password').val() === $('#confirm_password').val()) {
        $('#message').html('${passwordMatch}').css('color', 'green');
    } else
        $('#message').html('${passwordNotMatch}').css('color', 'red');
});
$("form").submit(function () {
    const v = grecaptcha.getResponse();
    if (v.length === 0) {
        document.getElementById('captcha').innerHTML = "You can't leave Captcha Code empty";
        return false;
    }
    return $('#password').val() === $('#confirm_password').val();
});