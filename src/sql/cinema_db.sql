-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Гру 02 2022 р., 02:06
-- Версія сервера: 10.4.24-MariaDB
-- Версія PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `cinema_db`
--

--
-- Видалення таблиць якщо вони існують
--

DROP TABLE IF EXISTS `films_genres`;
DROP TABLE IF EXISTS `free_seats`;
DROP TABLE IF EXISTS `tickets`;
DROP TABLE IF EXISTS `sessions`;
DROP TABLE IF EXISTS `films`;
DROP TABLE IF EXISTS `genres`;
DROP TABLE IF EXISTS `seats`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `roles`;
-- --------------------------------------------------------

--
-- Структура таблиці `films`
--

CREATE TABLE IF NOT EXISTS `films` (
  `film_id` int(11) NOT NULL,
  `film_name` varchar(120) NOT NULL,
  `description` varchar(800) DEFAULT NULL,
  `poster_url` varchar(2000) NOT NULL DEFAULT 'https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg',
  `duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `films`
--

INSERT INTO `films` (`film_id`, `film_name`, `description`, `poster_url`, `duration`) VALUES
(2, 'Пухнасті бешкетники', 'Один король ведмедів на ім\'я Бантура присвоїв собі те, що за фактом йому не належить – це магічний Камінь Води. Цей артефакт завжди був у лісі, завдяки якому струмок завжди був сповнений цілющої рідини. Зараз же без Каменя води в потічку майже немає, він буквально із дня на день пересохне зовсім. Щоб виправити цю ситуацію, потрібно, щоб хтось вирушив до лігва Бантури та забрав у нього камінь. На це погодилася смілива їжачиха Латте та її друг білченя Т\'юм. На їх шляху зустрінеться чимало пригод, і найбільше клопоту їм принесуть ватажок банди вовків Лупо та Рись. Це буде справжнє випробування для дружби Латте та Т\'юма.', 'https://kinoafisha.ua/upload/2019/07/films/8906/20fejso5ejik-latte-i-magicseskii-kamen.jpg', 81),
(4, 'Бетмен', NULL, 'https://woodmallcinema.com/storage/app/uploads/public/8a2/3e7/b0e/thumb__450_0_0_0_auto.jpg', 176),
(25, 'Гра тіней', '', 'https://woodmallcinema.com/storage/app/uploads/public/e19/ec4/a50/thumb__450_0_0_0_auto.jpg', 108),
(26, 'Світ Юрського періоду 3: Домініон', '', 'https://woodmallcinema.com/storage/app/uploads/public/4d7/699/e54/thumb__450_0_0_0_auto.jpg', 120),
(32, 'Фантастичні звірі: Таємниці Дамблдора', 'У 1932 році любитель магічних істот на ім\'я Ньют Саламандер знаходиться в місті Квейлін, розташованому в Китаї. Він приймає пологи у драконоподібного Цилиня, здатного бачити душу людини та події майбутнього. На світ з\'являються близнюки, але в цей момент нападають поплічники Геллерта Ґріндельвальда, очолювані підлим Кріденсом Бербоуном і вбивають матір, забравши одного з дитинчат. Про наявність другого лиходії не здогадуються, а головний герой умудряється втекти разом із новонародженим. Коли антагоніст отримує звіра, то вбиває, забравши надприродний дар передбачення.', 'https://woodmallcinema.com/storage/app/uploads/public/1bb/c7a/67d/thumb__450_0_0_0_auto.jpg', 142),
(33, 'Удaчa', 'Cем Грінфілд - найневезучіша дівчинка в світі. Несподівано опинилися у чарівній Країні Удачі, вона з\'єднується з наслідуючими її істотами, щоб повернути собі успіху.', 'https://st.kinob.net/storage/360x534/posters/2022/08/14444790d88ab1b5627e.jpg', 105),
(34, 'Дух Різдва', 'У центрі уваги життєва історія скупого та самотнього чоловіка. Він не любив дорослих та ненавидів дітей. Його дратували посмішки, дзвінкий сміх та радість на обличчях оточуючих. Шумна і химерна дітвора боялася навіть поглянути у бік злісного старого. Можна лише уявити, наскільки озлоблений і буркотливий він був у період зимових свят через їх безтурботну метушню. Герой фільму «Дух Різдва» засуджував зайві витрати на, як він вважав, непотрібні подарунки та багатолюдні гуляння. Одного разу вночі до нього з\'явилась незвичайна сутність, що представилася різдвяним духом. Вона тягне за собою, обіцяючи відкрити таємничу завісу майбутнього.', 'https://media-assets.wired.it/photos/6377a8be2b481175298529e9/3:4/w_471,h_628,c_limit/Apple_TV_Spirited_key_art_graphic_header_4_1_show_home.jpg.og.jpg', 127),
(35, 'Чорний Адам', 'Сюжет обертається довкола легендарного антигероя Чорного Адама. Провівши понад 5000 років у в\'язниці, центральний персонаж виходить на волю, щоб зіткнутися із проблемами, які приготували йому інші. Адам - справжній ворог Шазама, відомого супергероя, з яким він бореться вже багато років. Але чи вдасться йому здійснити задумане, передавши свої сили стародавньому чаклунові, чи він знову зазнає поразки від прихильників Ліги справедливості?', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfp2K6vG-Rfisfmsv7NN1-g-MAOtp_TNHVdB38NZALd9TASxZ0', 125),
(36, 'Вартові галактики: Святковий спецвипуск', 'Різдво - це чудова пора, щоб добре відпочити, отримати подарунки від улюблених друзів і просто повеселитися. Однак так буває, якщо ти звичайнісінька людина. У цій же історії мова піде про легендарних Вартових галактики, які насправді готові на все, заради того, щоб пуститися берега. Ми всі жахливо скучили за Зоряним Лордом, Драксом, Ракетою, Богомолом і Грутом. На цей раз компанія знову збирається разом. Це абсолютно новий, оригінальний спеціальний різдвяний епізод, який занурить глядача у неймовірну атмосферу веселощів.', 'https://uaserials.pro/posters/6885.jpg', 40),
(37, 'Втеча з Шоушенка', 'В основу культової драми «Втеча з Шоушенка» лягла повість Стівена Кінга «Ріта Хейворт з Шоушенка». Дії фільму розгортаються в кінці 50-х років минулого століття. Молодого фінансиста Енді Дюфрейна за подвійне вбивство засуджують до довічного ув\'язнення, але він категорично заперечує свою причетність до злочину. Свій термін Енді відправляється відбувати в одну з найвідоміших в\'язниць - Шоушенк, з якої ще нікому не вдавалося втекти.', 'https://upload.wikimedia.org/wikipedia/uk/8/87/%D0%92%D1%82%D0%B5%D1%87%D0%B0_%D0%B7_%D0%A8%D0%BE%D1%83%D1%88%D0%B5%D0%BD%D0%BA%D0%B0.jpg', 142);

-- --------------------------------------------------------

--
-- Структура таблиці `films_genres`
--

CREATE TABLE IF NOT EXISTS `films_genres` (
  `film_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `films_genres`
--

INSERT INTO `films_genres` (`film_id`, `genre_id`) VALUES
(2, 1),
(2, 7),
(2, 8),
(2, 9),
(4, 4),
(4, 5),
(4, 6),
(25, 10),
(25, 3),
(26, 7),
(32, 7),
(32, 8),
(32, 19),
(33, 9),
(33, 1),
(33, 7),
(33, 8),
(33, 19),
(34, 1),
(34, 17),
(34, 8),
(35, 5),
(35, 7),
(35, 19),
(36, 5),
(36, 1),
(36, 7),
(36, 19),
(37, 3),
(37, 4);

-- --------------------------------------------------------

--
-- Структура таблиці `free_seats`
--

CREATE TABLE IF NOT EXISTS `free_seats` (
  `session_seat_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `seat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `free_seats`
--

INSERT INTO `free_seats` (`session_seat_id`, `session_id`, `seat_id`) VALUES
(342, 24, 2),
(343, 24, 3),
(344, 24, 4),
(345, 24, 5),
(346, 24, 6),
(348, 24, 8),
(349, 24, 9),
(350, 24, 10),
(351, 24, 11),
(352, 24, 12),
(353, 24, 13),
(354, 24, 14),
(357, 24, 17),
(359, 24, 19),
(360, 24, 20),
(361, 24, 21),
(362, 24, 22),
(363, 24, 23),
(364, 24, 24),
(365, 24, 25),
(366, 24, 26),
(367, 24, 27),
(368, 24, 28),
(369, 24, 29),
(370, 24, 30),
(371, 25, 1),
(372, 25, 2),
(373, 25, 3),
(374, 25, 4),
(375, 25, 5),
(376, 25, 6),
(377, 25, 7),
(378, 25, 8),
(379, 25, 9),
(380, 25, 10),
(381, 25, 11),
(382, 25, 12),
(383, 25, 13),
(384, 25, 14),
(385, 25, 15),
(386, 25, 16),
(387, 25, 17),
(388, 25, 18),
(389, 25, 19),
(390, 25, 20),
(391, 25, 21),
(392, 25, 22),
(393, 25, 23),
(394, 25, 24),
(395, 25, 25),
(396, 25, 26),
(397, 25, 27),
(398, 25, 28),
(399, 25, 29),
(400, 25, 30),
(431, 27, 1),
(432, 27, 2),
(433, 27, 3),
(434, 27, 4),
(435, 27, 5),
(436, 27, 6),
(437, 27, 7),
(438, 27, 8),
(439, 27, 9),
(440, 27, 10),
(441, 27, 11),
(442, 27, 12),
(443, 27, 13),
(444, 27, 14),
(445, 27, 15),
(446, 27, 16),
(447, 27, 17),
(448, 27, 18),
(449, 27, 19),
(450, 27, 20),
(451, 27, 21),
(452, 27, 22),
(453, 27, 23),
(454, 27, 24),
(455, 27, 25),
(456, 27, 26),
(457, 27, 27),
(458, 27, 28),
(459, 27, 29),
(460, 27, 30),
(521, 30, 1),
(522, 30, 2),
(523, 30, 3),
(524, 30, 4),
(525, 30, 5),
(526, 30, 6),
(527, 30, 7),
(528, 30, 8),
(529, 30, 9),
(530, 30, 10),
(531, 30, 11),
(532, 30, 12),
(533, 30, 13),
(534, 30, 14),
(535, 30, 15),
(536, 30, 16),
(537, 30, 17),
(538, 30, 18),
(539, 30, 19),
(540, 30, 20),
(541, 30, 21),
(542, 30, 22),
(543, 30, 23),
(544, 30, 24),
(545, 30, 25),
(546, 30, 26),
(547, 30, 27),
(548, 30, 28),
(549, 30, 29),
(550, 30, 30),
(581, 32, 1),
(582, 32, 2),
(583, 32, 3),
(584, 32, 4),
(585, 32, 5),
(586, 32, 6),
(587, 32, 7),
(588, 32, 8),
(589, 32, 9),
(590, 32, 10),
(591, 32, 11),
(592, 32, 12),
(593, 32, 13),
(594, 32, 14),
(595, 32, 15),
(596, 32, 16),
(597, 32, 17),
(598, 32, 18),
(599, 32, 19),
(600, 32, 20),
(601, 32, 21),
(602, 32, 22),
(603, 32, 23),
(604, 32, 24),
(605, 32, 25),
(606, 32, 26),
(607, 32, 27),
(608, 32, 28),
(609, 32, 29),
(610, 32, 30),
(611, 33, 1),
(612, 33, 2),
(613, 33, 3),
(614, 33, 4),
(615, 33, 5),
(616, 33, 6),
(617, 33, 7),
(618, 33, 8),
(619, 33, 9),
(620, 33, 10),
(621, 33, 11),
(622, 33, 12),
(623, 33, 13),
(624, 33, 14),
(625, 33, 15),
(626, 33, 16),
(627, 33, 17),
(628, 33, 18),
(629, 33, 19),
(630, 33, 20),
(631, 33, 21),
(632, 33, 22),
(633, 33, 23),
(634, 33, 24),
(635, 33, 25),
(636, 33, 26),
(637, 33, 27),
(638, 33, 28),
(639, 33, 29),
(640, 33, 30),
(701, 36, 1),
(702, 36, 2),
(703, 36, 3),
(704, 36, 4),
(705, 36, 5),
(706, 36, 6),
(707, 36, 7),
(708, 36, 8),
(709, 36, 9),
(710, 36, 10),
(711, 36, 11),
(712, 36, 12),
(713, 36, 13),
(714, 36, 14),
(715, 36, 15),
(716, 36, 16),
(717, 36, 17),
(718, 36, 18),
(719, 36, 19),
(720, 36, 20),
(721, 36, 21),
(722, 36, 22),
(723, 36, 23),
(724, 36, 24),
(725, 36, 25),
(726, 36, 26),
(727, 36, 27),
(728, 36, 28),
(729, 36, 29),
(730, 36, 30),
(731, 37, 1),
(732, 37, 2),
(733, 37, 3),
(734, 37, 4),
(735, 37, 5),
(736, 37, 6),
(737, 37, 7),
(738, 37, 8),
(739, 37, 9),
(740, 37, 10),
(741, 37, 11),
(742, 37, 12),
(743, 37, 13),
(744, 37, 14),
(745, 37, 15),
(746, 37, 16),
(747, 37, 17),
(748, 37, 18),
(749, 37, 19),
(750, 37, 20),
(751, 37, 21),
(752, 37, 22),
(753, 37, 23),
(754, 37, 24),
(755, 37, 25),
(756, 37, 26),
(757, 37, 27),
(758, 37, 28),
(759, 37, 29),
(760, 37, 30),
(761, 38, 1),
(762, 38, 2),
(763, 38, 3),
(764, 38, 4),
(765, 38, 5),
(766, 38, 6),
(767, 38, 7),
(768, 38, 8),
(769, 38, 9),
(770, 38, 10),
(771, 38, 11),
(772, 38, 12),
(773, 38, 13),
(774, 38, 14),
(775, 38, 15),
(776, 38, 16),
(777, 38, 17),
(778, 38, 18),
(779, 38, 19),
(780, 38, 20),
(781, 38, 21),
(782, 38, 22),
(783, 38, 23),
(784, 38, 24),
(785, 38, 25),
(786, 38, 26),
(787, 38, 27),
(788, 38, 28),
(789, 38, 29),
(790, 38, 30),
(791, 39, 1),
(792, 39, 2),
(793, 39, 3),
(794, 39, 4),
(795, 39, 5),
(796, 39, 6),
(797, 39, 7),
(798, 39, 8),
(799, 39, 9),
(800, 39, 10),
(801, 39, 11),
(802, 39, 12),
(803, 39, 13),
(804, 39, 14),
(805, 39, 15),
(806, 39, 16),
(807, 39, 17),
(808, 39, 18),
(809, 39, 19),
(810, 39, 20),
(811, 39, 21),
(812, 39, 22),
(813, 39, 23),
(814, 39, 24),
(815, 39, 25),
(816, 39, 26),
(817, 39, 27),
(818, 39, 28),
(819, 39, 29),
(820, 39, 30),
(821, 40, 1),
(822, 40, 2),
(823, 40, 3),
(824, 40, 4),
(825, 40, 5),
(826, 40, 6),
(827, 40, 7),
(828, 40, 8),
(829, 40, 9),
(830, 40, 10),
(831, 40, 11),
(832, 40, 12),
(833, 40, 13),
(834, 40, 14),
(835, 40, 15),
(836, 40, 16),
(837, 40, 17),
(838, 40, 18),
(839, 40, 19),
(840, 40, 20),
(841, 40, 21),
(842, 40, 22),
(843, 40, 23),
(844, 40, 24),
(845, 40, 25),
(846, 40, 26),
(847, 40, 27),
(848, 40, 28),
(849, 40, 29),
(850, 40, 30),
(851, 41, 1),
(852, 41, 2),
(853, 41, 3),
(854, 41, 4),
(855, 41, 5),
(856, 41, 6),
(857, 41, 7),
(858, 41, 8),
(859, 41, 9),
(860, 41, 10),
(861, 41, 11),
(862, 41, 12),
(863, 41, 13),
(864, 41, 14),
(865, 41, 15),
(866, 41, 16),
(867, 41, 17),
(868, 41, 18),
(869, 41, 19),
(870, 41, 20),
(871, 41, 21),
(872, 41, 22),
(873, 41, 23),
(874, 41, 24),
(875, 41, 25),
(876, 41, 26),
(877, 41, 27),
(878, 41, 28),
(879, 41, 29),
(880, 41, 30);

-- --------------------------------------------------------

--
-- Структура таблиці `genres`
--

CREATE TABLE IF NOT EXISTS `genres` (
  `genre_id` int(11) NOT NULL,
  `genre_name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `genres`
--

INSERT INTO `genres` (`genre_id`, `genre_name`) VALUES
(15, 'Історична драма'),
(9, 'Анімація'),
(10, 'Біографічний'),
(6, 'Бойовик'),
(12, 'Вестерн'),
(13, 'Детектив'),
(14, 'Дитяче'),
(11, 'Документальне кіно'),
(3, 'Драма'),
(5, 'Екшн'),
(20, 'Жахи'),
(1, 'Комедія'),
(4, 'Кримінал'),
(16, 'Мелодрама'),
(17, 'Музичний фільм'),
(7, 'Пригоди'),
(2, 'Романтика'),
(8, 'Сімейний'),
(18, 'Трилер'),
(19, 'Фентезі');

-- --------------------------------------------------------

--
-- Структура таблиці `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(2, 'admin'),
(1, 'user');

-- --------------------------------------------------------

--
-- Структура таблиці `seats`
--

CREATE TABLE IF NOT EXISTS `seats` (
  `seat_id` int(11) NOT NULL,
  `row_number` int(11) NOT NULL,
  `place_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `seats`
--

INSERT INTO `seats` (`seat_id`, `row_number`, `place_number`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 2, 1),
(12, 2, 2),
(13, 2, 3),
(14, 2, 4),
(15, 2, 5),
(16, 2, 6),
(17, 2, 7),
(18, 2, 8),
(19, 2, 9),
(20, 2, 10),
(21, 3, 1),
(22, 3, 2),
(23, 3, 3),
(24, 3, 4),
(25, 3, 5),
(26, 3, 6),
(27, 3, 7),
(28, 3, 8),
(29, 3, 9),
(30, 3, 10);

-- --------------------------------------------------------

--
-- Структура таблиці `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` int(11) NOT NULL,
  `film_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `ticket_price` decimal(10,2) NOT NULL,
  `free_seats` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `sessions`
--

INSERT INTO `sessions` (`session_id`, `film_id`, `date`, `time`, `ticket_price`, `free_seats`) VALUES
(24, 26, '2023-03-16', '12:00:00', '340.00', 25),
(25, 25, '2023-02-27', '12:00:00', '220.00', 30),
(27, 4, '2023-02-19', '19:00:00', '370.00', 30),
(30, 2, '2023-02-15', '16:15:00', '200.00', 30),
(32, 25, '2023-02-27', '18:00:00', '250.00', 30),
(33, 26, '2023-02-23', '12:11:00', '290.00', 30),
(36, 33, '2023-03-03', '09:00:00', '160.00', 30),
(37, 37, '2023-03-04', '09:30:00', '220.00', 30),
(38, 36, '2023-02-17', '17:50:00', '70.00', 30),
(39, 35, '2023-02-14', '20:00:00', '340.00', 30),
(40, 34, '2023-03-03', '19:00:00', '120.00', 30),
(41, 33, '2023-02-20', '12:00:00', '130.00', 30);

-- --------------------------------------------------------

--
-- Структура таблиці `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `ticket_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `seat_id` int(11) NOT NULL,
  `ticket_price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `tickets`
--

INSERT INTO `tickets` (`ticket_id`, `session_id`, `user_id`, `seat_id`, `ticket_price`) VALUES
(58, 24, 59, 18, '340.00');

-- --------------------------------------------------------

--
-- Структура таблиці `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT 1,
  `first_name` varchar(45) NOT NULL,
  `second_name` varchar(45) NOT NULL,
  `email` varchar(320) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone_number` varchar(13) DEFAULT NULL,
  `notification` tinyint(4) NOT NULL DEFAULT 1,
  `salt` varchar(100) NOT NULL,
  `verification_code` varchar(64),
  `enabled` BIT(1) NOT NULL DEFAULT FALSE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `users`
--

INSERT INTO `users` (`user_id`, `role_id`, `first_name`, `second_name`, `email`, `password`, `phone_number`, `notification`, `salt`) VALUES
(58, 2, 'Artem', 'Mahey', 'artemmagej@gmail.com', '+ycIC1Z7hgSavjx4Hn8aj81b4CbjQfX9Y9q0ApgUDB8=', '', 1, 'gITqPMzO7a8H4WjPC96G9lMv0E7T88'),
(59, 1, 'Artemm', 'Mahey', 'artemmagej1@gmail.com', '6QPhIO7EgfvmlQPJtuwn6/4NDDdYxTZ+Ql+aqSp6UMw=', '380986745006', 1, 'yO0L2UNlK5uCIZzH0FGhOod356WOLD');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`film_id`);

--
-- Індекси таблиці `films_genres`
--
ALTER TABLE `films_genres`
  ADD KEY `film_id_genre_idx` (`film_id`),
  ADD KEY `genre_id_film_idx` (`genre_id`);

--
-- Індекси таблиці `free_seats`
--
ALTER TABLE `free_seats`
  ADD PRIMARY KEY (`session_seat_id`),
  ADD KEY `seat_session_id_idx` (`session_id`),
  ADD KEY `free_seat_id_idx` (`seat_id`);

--
-- Індекси таблиці `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`genre_id`),
  ADD UNIQUE KEY `genre_name_UNIQUE` (`genre_name`);

--
-- Індекси таблиці `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `role_name_UNIQUE` (`role_name`);

--
-- Індекси таблиці `seats`
--
ALTER TABLE `seats`
  ADD PRIMARY KEY (`seat_id`);

--
-- Індекси таблиці `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `film_id_session_idx` (`film_id`);

--
-- Індекси таблиці `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ticket_id`),
  ADD KEY `session_id_ticket_idx` (`session_id`),
  ADD KEY `user_id_ticket_idx` (`user_id`),
  ADD KEY `seat_id_ticket_idx` (`seat_id`);

--
-- Індекси таблиці `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `user_role_id_idx` (`role_id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `films`
--
ALTER TABLE `films`
  MODIFY `film_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT для таблиці `free_seats`
--
ALTER TABLE `free_seats`
  MODIFY `session_seat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=881;

--
-- AUTO_INCREMENT для таблиці `genres`
--
ALTER TABLE `genres`
  MODIFY `genre_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблиці `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблиці `seats`
--
ALTER TABLE `seats`
  MODIFY `seat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблиці `sessions`
--
ALTER TABLE `sessions`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT для таблиці `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT для таблиці `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- Обмеження зовнішнього ключа збережених таблиць
--

--
-- Обмеження зовнішнього ключа таблиці `films_genres`
--
ALTER TABLE `films_genres`
  ADD CONSTRAINT `film_id_genre` FOREIGN KEY (`film_id`) REFERENCES `films` (`film_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `genre_id_film` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`genre_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `free_seats`
--
ALTER TABLE `free_seats`
  ADD CONSTRAINT `free_seat_id` FOREIGN KEY (`seat_id`) REFERENCES `seats` (`seat_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `seat_session_id` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `film_id_session` FOREIGN KEY (`film_id`) REFERENCES `films` (`film_id`) ON DELETE CASCADE;

--
-- Обмеження зовнішнього ключа таблиці `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `seat_id_ticket` FOREIGN KEY (`seat_id`) REFERENCES `seats` (`seat_id`),
  ADD CONSTRAINT `session_id_ticket` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_id_ticket` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Обмеження зовнішнього ключа таблиці `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `user_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
